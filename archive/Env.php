<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Config
 *
 * @author rl
 */
abstract class Env {

    //определение переменных для базы данных
    static $DB_SERVER;
    static $DB_USER;
    static $DB_PASS;
    static $DB;
    /**
     * @var PDO 
     */
    static $DBH;
    static $URL_INFO;
    static $REQUEST_URL;
    static $SCHEME;
    static $HOST;
    static $PATH;
    static $APPL_CONTEXT;
    static $HTTP_APPL_ROOT;
    static $DOCUMENT_APPL_ROOT;
    static $CLASSES_ROOT;
    static $SCHEMAS_ROOT;
    static $HTTP_SCHEMAS_ROOT;
    static $WADLS_ROOT;
    static $REQUEST_METHOD;
    static $parserXmlSchema;
    
    static function create() {
        //Откроем соединение
        try {
            # MySQL через PDO_MYSQL  
            Env::$DBH = new PDO("mysql:host=" . Env::$DB_SERVER . ";dbname=" . Env::$DB . "", Env::$DB_USER, Env::$DB_PASS);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
        /** Test
        echo "<pre>";
        Env::$DBH = new PDO("mysql:host=localhost;dbname=nerv", "root", "");
        $STH = Env::$DBH->prepare("SELECT * FROM users");
        $STH->execute();

        print_r($STH->fetchAll());
        */
        QueryBuilder::$DBH = Env::$DBH;
        
        Env::$URL_INFO = Utils::parseUrl();
        Env::$REQUEST_URL = Env::$URL_INFO["request_url"];
        Env::$SCHEME = Env::$URL_INFO["scheme"];
        Env::$HOST = Env::$URL_INFO["host"];
        Env::$PATH = Env::$URL_INFO["path"];
        Env::$REQUEST_METHOD = $_SERVER["REQUEST_METHOD"];

        Env::$HTTP_APPL_ROOT = Env::$SCHEME . "://" . Env::$HOST . Env::$APPL_CONTEXT;
        Env::$DOCUMENT_APPL_ROOT = $_SERVER["DOCUMENT_ROOT"] . Env::$APPL_CONTEXT;

        // Настроим автозагрузку классов
        Env::$CLASSES_ROOT = Env::$DOCUMENT_APPL_ROOT . "/classes";

        function __autoload($className) {
            $classesSubFolders = array(
                "I...Controller" => "/icontrollers",
                "Controller" => "/controllers",
                "Requests" => "/request",
                "Response" => "/responses",
                "Enum" => "/enums",
                "Mapper" => "/mappers",
                "Service" => "/services",
                "Validator" => "/validators",
                "Uri" => "/uris"
            );
            $classPath = Env::$CLASSES_ROOT;
            $interfacesSubFolders = array(
                "Controller" => "/icontrollers"
            );
            //найдем папку для класса. Если папка не найдена то класс должен лежать в директории types
            $subFolder = null;
            if (preg_match("/^I.+Controller$/", $className)) {
                $className = str_replace("I..", "", $className);
                foreach ($interfacesSubFolders as $subClass => $curSubFolder) {
                    if (preg_match("/" .$subClass . "$/", $className)) {
                        $subFolder = $curSubFolder;
                        break;
                    }
                }
            } else {
                foreach ($classesSubFolders as $subClass => $curSubFolder) {
                    if (preg_match("/" .$subClass . "$/", $className)) {
                        $subFolder = $curSubFolder;
                        break;
                    }
                }    
            }
            if ($subFolder === null) {
                $subFolder = "/types";
            }
            $classPath .= $subFolder;
            $classFullPath .= $classPath . "/" . $className . ".php";
            if (!file_exists($classFullPath)) {
                throw new Exception("Нет такого файла " . $classFullPath);
            }
            include $classFullPath;
        }
        
        Env::$SCHEMAS_ROOT = Env::$DOCUMENT_APPL_ROOT . "/schemas";
        Env::$HTTP_SCHEMAS_ROOT = "http://calmsen.com";
        Env::$WADLS_ROOT = Env::$DOCUMENT_APPL_ROOT . "/wadls";
        
        // Создадим классы и валидации форм
        Env::$parserXmlSchema = new ParserXmlSchema(Env::$SCHEMAS_ROOT, Env::$CLASSES_ROOT);
        Env::$parserXmlSchema->transformXsdToClass(Env::$SCHEMAS_ROOT . "/types/UserType.xsd");
        // Создадим интерфейсы контроллеров
        $pw = new ParserWadl(Env::$SCHEMAS_ROOT, Env::$CLASSES_ROOT, Env::$parserXmlSchema);
        $pw->transformWadlToClass(Env::$WADLS_ROOT . "/UserWadl.xml");
        // Определим контроллер и метод для вызова 
        if (count(Env::$URL_INFO["slugs"]) > 0) {
            $slug_rows = QueryBuilder::table("slugs")
                    ->where("slug", Operands::IN, "?", Env::$URL_INFO["slugs"])->select();
            
            $slugTypes = array();
            foreach ($slug_rows as $slug_row) {
                $slugTypes[$slug_row["slug"]] = $slug_row["type"];
            }
            new Router($slugTypes, NodeTypesEnum, Env::$URL_INFO["slugs"], Env::$APPL_CONTEXT);
        }
    }

    static function getParameter($parameter, $conv_spec_sym = 1) {

        $value = null;

        if (isset($_GET[$parameter])) {
            $value = $_GET[$parameter];
        } elseif (isset($_POST[$parameter])) {
            $value = $_POST[$parameter];
        }
        if (is_string($value)) {
            $value = trim($value);
            if ($conv_spec_sym == 1) {
                $value = stripslashes($value);
                $value = htmlspecialchars($value);
            }
        }

        if (is_array($value)) {
            foreach ($value as $key => $val) {
                $val = trim($val);
                if ($conv_spec_sym == 1) {
                    $val = stripslashes($val);
                    $val = htmlspecialchars($val);
                }
                $value[$key] = $val;
            }
        }
        return $value;
    }
    
}
?>