$(function(){
    /**
     * Добавляем модификатор(класс, описываемый по БЭМу). 
     * Если модификатор добавляется к элементу, то добавим класс и проверим чтоб у блока был требуемый модификатор
     * Если модификатор добавляется к блоку, то добавим классы для блока и всех его элементов, если это требуется
     * @param {string} modif
     * @returns {jQuery}
     */
    $.fn.addModif = function(modif) {
        return this.each(function(){
            if (this.classList.contains(modif)) {
                return;
            }
            
            var classParts = modif.split("__");
            var block = classParts[0];
            var blockParts = block.split("_");
            var blockName = blockParts[0];
            var blockModifType = blockParts[1];
            var blockModifValue = blockParts[2];
            var element = classParts[1];
            if (element !== undefined) {
                var elementParts = element.split("_");
                var elementName = elementParts[0];
                var elementModifType = elementParts[1];
                var elementModifValue = elementParts[2];
                var elementFullName = blockName + "__" + elementName;
                if (elementModifType !== undefined) {
                    if (this.dataset.bemBlock === undefined) {
                        
                    }
                        
                }
                for (var i in this.classList) {
                }
            }
        });
    };
    $("body").addModif("sdg")
});