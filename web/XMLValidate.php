<?php
header ('Content-type: text/html; charset=utf-8');
libxml_use_internal_errors(true);
$xml = new DOMDocument();
$xml->load('./xmls/Test.xml');
if ($xml->schemaValidate('./schemas/Test.xsd')) {
	echo "OK";
} else {
	$errors = libxml_get_errors();
	foreach($errors as $error) {
		echo "level: $error->level ";
		echo "code: $error->code ";
		echo "file: $error->file ";
		echo "line: $error->line ";
		echo "column: $error->column ";
		echo "message: $error->message <br />";
	}
	
}
//echo phpinfo();
?>
