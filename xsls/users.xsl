<xsl:stylesheet version = '1.0' 
     xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
	 
<xsl:import href="books.xsl"/>

<xsl:output method="html"/>	

<xsl:template match="/" priority="9"> 
     <xsl:apply-templates select="root/users"/>
</xsl:template>

<xsl:template match="users/columns/column">
    <th><xsl:value-of select="name" /></th>
</xsl:template>

<xsl:template match="user" mode="tableRow">
    <tr>
        <td><xsl:value-of select="concat(firstName, ' ', lastName)" /></td>
        <td><xsl:apply-templates select="books" mode="toString"/></td>
    </tr>
</xsl:template>

<xsl:template match="users[count(user)=0]" mode="tableRows">
    <tr>
        <td colspan="2">Нет ниодной записи</td>
    </tr>
</xsl:template>

<xsl:template match="users" mode="tableRows">
    <xsl:apply-templates select="user" mode="tableRow"/>
</xsl:template>

<xsl:template match="users" mode="table">
    <table border="1">
        <caption class="header"><xsl:value-of select="header" /></caption>
        <thead>
            <tr>
                <xsl:apply-templates select="columns/column"/>
            </tr>
        </thead>
        <tbody>
            <xsl:apply-templates select="." mode="tableRows"/>
        </tbody>
    </table>
</xsl:template>

<xsl:template match="users">
<div id="usersHolder">
	<h1 class="header"><xsl:value-of select="header" /></h1>
	<ul class="list">
		<xsl:apply-templates select="user" />
	</ul>
</div>
</xsl:template>

<xsl:template match="user">
<li>
	<xsl:value-of select="concat(firstName, ' ', lastName)" />
	<h4 class="header">Книги</h4>
	<xsl:apply-templates select="books" mode="toString" />
</li>
</xsl:template>

</xsl:stylesheet>