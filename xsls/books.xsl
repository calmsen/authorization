<xsl:stylesheet version = '1.0' 
     xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>

<xsl:output method="html"/>	

<xsl:template match="books"> 
<div id="booksHolder">
	<ul class="list">
		<xsl:apply-templates select="book" />
	</ul>
</div>
</xsl:template>

<xsl:template match="book">
<li>
	<xsl:value-of select="." />
</li>
</xsl:template>

<xsl:template match="books" mode="toString">
    <xsl:apply-templates select="book" mode="asStringPart" />
</xsl:template>

<xsl:template match="book" mode="asStringPart">
    <xsl:value-of select="." />
    <xsl:apply-templates select="." mode="comma" />
</xsl:template>

<xsl:template match="book[position()!=last()]" mode="comma">
    <xsl:text>,</xsl:text>
</xsl:template>

<xsl:template match="book[position()=last()]" mode="comma">
    <xsl:text></xsl:text>
</xsl:template>

</xsl:stylesheet>