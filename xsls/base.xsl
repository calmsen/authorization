<xsl:stylesheet version = '1.0' 
     xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>

<xsl:import href="users.xsl"/>

<xsl:template match="/" priority="10"> 
     <xsl:apply-templates select="root"/>
</xsl:template>

 <xsl:template match="root"> 
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>
			Первый шаблон - <xsl:value-of select="title" />
		</title>
		<style>
		.header {
			font-size: 14px;
			font-weight: bold;
		}
		.list {
			list-style: none;
			margin: 0px;
			padding: 0px;
		}
		</style>
	</head>
	<body>
		<xsl:apply-templates select="users" mode="table"/>
	</body>
</html>
</xsl:template>

<xsl:template match="*[position()!=last()]" mode="comma">
    <xsl:text>,</xsl:text>
</xsl:template>

<xsl:template match="*[position()=last()]" mode="comma">
    <xsl:text></xsl:text>
</xsl:template>

<xsl:template match="*" mode="toString">
    <xsl:apply-templates select="*" mode="asPartString" />
</xsl:template>

<xsl:template match="*" mode="asPartString">
    <xsl:value-of select="." />
    <xsl:apply-templates select="." mode="comma" />
</xsl:template>

</xsl:stylesheet>