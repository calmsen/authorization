<xsl:stylesheet version = '1.0' 
     xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>

<xsl:output method="xml"/>
	
<xsl:template match="xsl:stylesheet">
    <xsl:copy>
       <xsl:copy-of select="@*"/>
       <xsl:apply-templates />
    </xsl:copy>
</xsl:template>

<xsl:template match="xsl:import"  priority="10">
   <xsl:copy-of select="document(concat('http://world-page.tmp/test/xsls/', @href))/xsl:stylesheet/*" />
</xsl:template>

<xsl:template match="xsl:stylesheet/*" priority="9">
   <xsl:copy-of select="." />
</xsl:template>
</xsl:stylesheet>