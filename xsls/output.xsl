<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="html"/><xsl:template match="books"> 
<div id="bookssHolder">
	<ul class="list">
		<xsl:apply-templates select="book"/>
	</ul>
</div>
</xsl:template><xsl:template match="book">
<li>
	<xsl:value-of select="."/>
</li>
</xsl:template><xsl:output method="html"/><xsl:template match="/" priority="9"> 
     <xsl:apply-templates select="root/users"/>
</xsl:template><xsl:template match="users">
<div id="usersHolder">
	<h1 class="header"><xsl:value-of select="header"/></h1>
	<ul class="list">
		<xsl:apply-templates select="user"/>
	</ul>
</div>
</xsl:template><xsl:template match="user">
<li>
	<xsl:value-of select="concat(firstName, ' ', lastName)"/>
	<h4 class="header">&#x41A;&#x43D;&#x438;&#x433;&#x438;</h4>
	<xsl:apply-templates select="books"/>
</li>
</xsl:template>

<xsl:template match="/" priority="10"> 
     <xsl:apply-templates select="root"/>
</xsl:template>

 <xsl:template match="root"> 
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<title>
			&#x41F;&#x435;&#x440;&#x432;&#x44B;&#x439; &#x448;&#x430;&#x431;&#x43B;&#x43E;&#x43D; - <xsl:value-of select="title"/>
		</title>
		<style>
		.header {
			font-size: 14px;
			font-weight: bold;
		}
		.list {
			list-style: none;
			margin: 0px;
			padding: 0px;
		}
		</style>
	</head>
	<body>
		<xsl:apply-templates select="users"/>
	</body>
</html>
</xsl:template>

</xsl:stylesheet>
