<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UsersMapper
 *
 * @author rl
 */
class UsersMapper {

    const TABLE = "users";
    const ENTITY = "User";

    /**
     * @param int $id идентификатор` пользователя
     * @return User объект экземпляра пользователь
     */
    static function getUserById($id) {
        $user = QueryBuilder::table(UsersMapper::TABLE)
                ->where(UserProperty::ID, Operands::EQ, ":" . UserProperty::ID, $id)
                ->one();
        if ($user !== null) {
            return Utils::transformArrayToObject($user, UsersMapper::ENTITY);
        } else {
            return null;
        }
    }
    /**
     * 
     * @param int[] $ids идентификатор пользователя
     * @return User[] список объектов экземпляра пользователь
     */
    static function getUsersByIds($ids = array()) {
        $users = QueryBuilder::table(UsersMapper::TABLE)
                ->where(UserProperty::ID, Operands::EQ, ":" . UserProperty::ID, $id)
                ->get();
        $userObjs = array(); 
        foreach ($users as $user) {
            $userObjs[] = Utils::transformArrayToObject($user, UsersMapper::ENTITY);
        }
        return $userObjs;
    }
    /**
     * @param string $email email пользователя
     * @return User объект экземпляра пользователь
     */
    static function getUserByEmail($email) {
        $user = QueryBuilder::table(UsersMapper::TABLE)
                ->where(UserProperty::EMAIL, Operands::EQ, ":" . UserProperty::EMAIL, $email)
                ->one();
        if ($user !== null) {
            return Utils::transformArrayToObject($user, UsersMapper::ENTITY);
        } else {
            return null;
        }
    }
    /**
     * @param string $nick ник пользователя
     * @return User объект экземпляра пользователь
     */
    static function getUserByNick($nick) {
        $user = QueryBuilder::table(UsersMapper::TABLE)
                ->where(UserProperty::NICK, Operands::EQ, ":" . UserProperty::NICK, $nick)
                ->one();
        if ($user !== null) {
            return Utils::transformArrayToObject($user, UsersMapper::ENTITY);
        } else {
            return null;
        }
    }
    /**
     * @param string[] $fields атрибуты пользователя
     * @return User объект экземпляра пользователь
     */
    static function getUserByFields($fields = array()) {
        if (count($fields) == 0) {
            return null;
        }
        $query = QueryBuilder::table(UsersMapper::TABLE);
        foreach ($fields as $fieldName => $fieldValue) {
            $query->where($fieldName, OPERANDS::EQ, $fieldValue);
        }
        $user = $query->one();
        if ($user !== null) {
            return Utils::transformArrayToObject($user, UsersMapper::ENTITY);
        } else {
            return null;
        }
    }
    /**
     * @param string[] $fields атрибуты пользователя
     * @return User[] список объектов экземпляра пользователь
     */
    static function getUsersByFields($fields = array()) {
        if (count($fields) == 0) {
            throw new Exception("Массив не должен быть пустым.");
        }
        $query = QueryBuilder::table(UsersMapper::TABLE);
        foreach ($fields as $fieldName => $fieldValue) {
            $query->where($fieldName, OPERANDS::EQ, $fieldValue);
        }
        $users = $query->get();
        $userObjs = array(); 
        foreach ($users as $user) {
            $userObjs[] = Utils::transformArrayToObject($user, UsersMapper::ENTITY);
        }
        return $userObjs;
    }
    /**
     * @param function $fn лямбда функция
     * @return User объект экземпляра пользователь
     */
    static function getUserByQuery($fn) {
        $query = QueryBuilder::table(UsersMapper::TABLE);
        $fn($query);
        $user = $query->one();
        if ($user !== null) {
            return Utils::transformArrayToObject($user, UsersMapper::ENTITY);
        } else {
            return null;
        }
    }
    /**
     * @param function $fn лямбда функция
     * @return User[] список объектов экземпляра пользователь
     */
    static function getUsersByQuery($fn) {
        $query = QueryBuilder::table(UsersMapper::TABLE);
        $fn($query);
        $users = $query->get();
        $userObjs = array(); 
        foreach ($users as $user) {
            $userObjs[] = Utils::transformArrayToObject($user, UsersMapper::ENTITY);
        }
        return $userObjs;
    }
    /**
     * @param User объект экземпляра пользователь
     */
    static function saveUser($user) {
        $data = Utils::transformObjectToArray($user, Utils::getPrivateAndProtectedPropertiesNames("UserModel"));
        $query = QueryBuilder::table(UsersMapper::TABLE);
        if ($user->getId() == 0) {
            $query->insert($data);
            $user->setId($query->getInsertId());
        } else {
            $query->update($data);
        }
    }
    /**
     * @param Userp[] список объектов экземпляра пользователь
     */
    static function saveUsers($users = array()) {
        foreach ($users as $user) {
            $this->saveUser($user);
        }
    }
    /**
     * @param User $user объект экземпляра пользователь
     */
    static function setStateForUser($user) {
        self::setStateForUsers(array($user));
    }
    /**
     * @param Userp[] $users список объектов экземпляра пользователь
     * @param boolean $retained метка для инвертности массива
     * @return void
     * @throws Exception
     */
    static function setStateForUsers($users = array(), $retained = false) {
       if (count($users) == 0) {
           return;
       }
       $userIds = null;
       if (is_scalar($users[0])) {
           $userIds = $users;
       } else {
           $userIds = array();
           foreach ($users as $user) {
               $userIds[] = $user->getId();
           }
       }
       foreach ($usersIds as $usersId) {
           if (!is_int($usersId)) {
               throw new Exception("Массив должен содержать объекты или числа.");
           }
       }
       $data = array(UserProperty::STATE => StateEnum::REMOVED);
       if (!$retained) {
           QueryBuilder::table(UsersMapper::TABLE)->where(UserProperty::ID, Operands::IN, $userIds)->update();
       } else {
           QueryBuilder::table(UsersMapper::TABLE)->where(UserProperty::ID, Operands::NOTIN, $userIds)->update();
       }
       
    }
}
UsersMapper::saveUser();
?>
