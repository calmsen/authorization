<?php                    
/**                    
 * Данный класс содержит статические функции для проверки переменных на валидность                    
 *                    
 * @author Ruslan Rakhmankulov                    
 */                    
class AccountTypeValidator {                    

	static function validateAccount($obj) {                    
		try {            
			if ($obj->getId() !== null) {                
				BaseTypeValidator::validateId($obj->getId());                
			}            
		} catch (Exception $ex) {            
			ValidateResult::$errors["id"] = $ex->getMessage();            
		}
		try {            
			if ($obj->getLogin() !== null) {                
				BaseTypeValidator::validateLogin($obj->getLogin());                
			}            
		} catch (Exception $ex) {            
			ValidateResult::$errors["login"] = $ex->getMessage();            
		}
		try {            
			if ($obj->getPassword() !== null) {                
				BaseTypeValidator::validatePassword($obj->getPassword());                
			}            
		} catch (Exception $ex) {            
			ValidateResult::$errors["password"] = $ex->getMessage();            
		}
		try {            
			if ($obj->getUser() !== null) {                
				UserTypeValidator::validateUser($obj->getUser());                
			}            
		} catch (Exception $ex) {            
			ValidateResult::$errors["user"] = $ex->getMessage();            
		}
		$finded = false;
		if ($obj->getEl1() !== null) {                    
			$finded = true;                    
			try {            
				Validator::validateString($obj->getEl1());            
			} catch (Exception $ex) {            
				ValidateResult::$errors["el1"] = $ex->getMessage();            
			}                    
		}
		else if ($obj->getEl3() !== null) {                    
			$finded = true;                    
			try {            
				Validator::validateString($obj->getEl3());            
			} catch (Exception $ex) {            
				ValidateResult::$errors["el3"] = $ex->getMessage();            
			}
			try {            
				if ($obj->getEl4() !== null) {                
					Validator::validateString($obj->getEl4());                
				}            
			} catch (Exception $ex) {            
				ValidateResult::$errors["el4"] = $ex->getMessage();            
			}                    
		}
		else if ($obj->getEl2() !== null) {                    
			$finded = true;                    
			try {            
				Validator::validateString($obj->getEl2());            
			} catch (Exception $ex) {            
				ValidateResult::$errors["el2"] = $ex->getMessage();            
			}                    
		}
		else if ($obj->getEl5() !== null) {                    
			$finded = true;                    
			try {            
				Validator::validateString($obj->getEl5());            
			} catch (Exception $ex) {            
				ValidateResult::$errors["el5"] = $ex->getMessage();            
			}
			try {            
				Validator::validateString($obj->getEl3());            
			} catch (Exception $ex) {            
				ValidateResult::$errors["el3"] = $ex->getMessage();            
			}
			try {            
				if ($obj->getEl4() !== null) {                
					Validator::validateString($obj->getEl4());                
				}            
			} catch (Exception $ex) {            
				ValidateResult::$errors["el4"] = $ex->getMessage();            
			}                    
		}
		if (!$finded) {                
			throw new Exception("Укажите что нибудь из следующего: el1 | el3 | el2 | el5");                
		}                    
	}

	static function validateAccounts($list) {                    
		foreach($list as $item) {            
			AccountTypeValidator::validateAccount($item);            
		}                    
	}

	static function validateAddedAccount($obj) {                    
		try {            
			BaseTypeValidator::validateId($obj->getId());            
		} catch (Exception $ex) {            
			ValidateResult::$errors["id"] = $ex->getMessage();            
		}
		try {            
			BaseTypeValidator::validateLogin($obj->getLogin());            
		} catch (Exception $ex) {            
			ValidateResult::$errors["login"] = $ex->getMessage();            
		}
		try {            
			BaseTypeValidator::validatePassword($obj->getPassword());            
		} catch (Exception $ex) {            
			ValidateResult::$errors["password"] = $ex->getMessage();            
		}                    
	}

	static function validateAddedAccounts($list) {                    
		if (count($list) < 1) {            
			throw new Exception("Массив с типом AddedAccounts не должен состоять меньше чем из 1 элементов.");            
		}
		if (count($list) > 1) {            
			throw new Exception("Массив с типом AddedAccounts не должен состоять больше чем из 1 элементов.");            
		}
		foreach($list as $item) {            
			AccountTypeValidator::validateAddedAccount($item);            
		}                    
	}                    
}                    
?>