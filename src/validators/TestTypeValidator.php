<?php                    
/**                    
 * Данный класс содержит статические функции для проверки переменных на валидность                    
 *                    
 * @author Ruslan Rakhmankulov                    
 */                    
class TestTypeValidator extends Validator {                    

	static function validateTest($obj) {                    
		// Проверим в кэше                
		if (UserTypeValidator::$caches["testCache"] === null) {                
			UserTypeValidator::$caches["testCache"] = [];                
		}                
		if (in_array($obj->getId(), UserTypeValidator::$caches["testCache"])) {                
			return;                
		}                
		UserTypeValidator::$caches["testCache"][] = $obj->getId();                
		//
		try {            
			if ($obj->getId() !== null) {                
				BaseTypeValidator::validateId($obj->getId());                
			}            
		} catch (\Exception $ex) {            
			ValidateResult::$errors["id"] = $ex->getMessage();            
		}
		try {            
			if ($obj->getEmail() !== null) {                
				BaseTypeValidator::validateEmail($obj->getEmail());                
			}            
		} catch (\Exception $ex) {            
			ValidateResult::$errors["email"] = $ex->getMessage();            
		}                    
	}                    
}                    
?>