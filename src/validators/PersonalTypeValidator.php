<?php                    
/**                    
 * Данный класс содержит статические функции для проверки переменных на валидность                    
 *                    
 * @author Ruslan Rakhmankulov                    
 */                    
class PersonalTypeValidator extends Validator {                    

	static function validatePersonal($obj) {                    
		// Проверим в кэше                
		if (UserTypeValidator::$caches["personalCache"] === null) {                
			UserTypeValidator::$caches["personalCache"] = [];                
		}                
		if (in_array($obj->getId(), UserTypeValidator::$caches["personalCache"])) {                
			return;                
		}                
		UserTypeValidator::$caches["personalCache"][] = $obj->getId();                
		//
		try {            
			if ($obj->getId() !== null) {                
				BaseTypeValidator::validateId($obj->getId());                
			}            
		} catch (\Exception $ex) {            
			ValidateResult::$errors["id"] = $ex->getMessage();            
		}
		try {            
			if ($obj->getEmail() !== null) {                
				BaseTypeValidator::validateEmail($obj->getEmail());                
			}            
		} catch (\Exception $ex) {            
			ValidateResult::$errors["email"] = $ex->getMessage();            
		}
		try {            
			if ($obj->getPhone() !== null) {                
				BaseTypeValidator::validatePhone($obj->getPhone());                
			}            
		} catch (\Exception $ex) {            
			ValidateResult::$errors["phone"] = $ex->getMessage();            
		}                    
	}                    
}                    
?>