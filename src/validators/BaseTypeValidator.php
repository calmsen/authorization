<?php                    
/**                    
 * Данный класс содержит статические функции для проверки переменных на валидность                    
 *                    
 * @author Ruslan Rakhmankulov                    
 */                    
class BaseTypeValidator extends Validator {                    

	static function validateId($num) {                    
		if (is_int($num)) {                
			throw new \Exception( "Число с типом Id должно содержать целое число." );                
		}
		if ($num < 0) {                
			throw new \Exception( "Число c типом Id не может быть меньше 0." );                 
		}                    
	}

	static function validateName($str) {                    
		if (mb_strlen($str) < 1) {                    
			throw new \Exception( "Строка с типом Name не должна состоять меньше чем из 1 символа." );                     
		}
		if (mb_strlen($str) > 32) {                    
			throw new \Exception( "Строка с типом Name не должна состоять больше чем из 32 символов." );                     
		}
		if (!preg_match(".*", $str)) {                    
			throw new \Exception( "Строка с типом Name должна совпадать по шаблону .*." );                     
		}                    
	}

	static function validateAlias($str) {                    
		if (mb_strlen($str) < 1) {                    
			throw new \Exception( "Строка с типом Alias не должна состоять меньше чем из 1 символа." );                     
		}
		if (mb_strlen($str) > 32) {                    
			throw new \Exception( "Строка с типом Alias не должна состоять больше чем из 32 символов." );                     
		}
		if (!preg_match(".*", $str)) {                    
			throw new \Exception( "Строка с типом Alias должна совпадать по шаблону .*." );                     
		}                    
	}

	static function validateEmail($str) {                    
		if (mb_strlen($str) < 7) {                    
			throw new \Exception( "Строка с типом Email не должна состоять меньше чем из 7 символов." );                     
		}
		if (mb_strlen($str) > 32) {                    
			throw new \Exception( "Строка с типом Email не должна состоять больше чем из 32 символов." );                     
		}
		if (!preg_match(".*", $str)) {                    
			throw new \Exception( "Строка с типом Email должна совпадать по шаблону .*." );                     
		}                    
	}

	static function validatePassword($str) {                    
		if (mb_strlen($str) < 7) {                    
			throw new \Exception( "Строка с типом Password не должна состоять меньше чем из 7 символов." );                     
		}
		if (mb_strlen($str) > 16) {                    
			throw new \Exception( "Строка с типом Password не должна состоять больше чем из 16 символов." );                     
		}
		if (!preg_match(".*", $str)) {                    
			throw new \Exception( "Строка с типом Password должна совпадать по шаблону .*." );                     
		}                    
	}

	static function validateWebsite($str) {                    
		if (mb_strlen($str) < 7) {                    
			throw new \Exception( "Строка с типом Website не должна состоять меньше чем из 7 символов." );                     
		}
		if (mb_strlen($str) > 64) {                    
			throw new \Exception( "Строка с типом Website не должна состоять больше чем из 64 символов." );                     
		}
		if (!preg_match(".*", $str)) {                    
			throw new \Exception( "Строка с типом Website должна совпадать по шаблону .*." );                     
		}                    
	}

	static function validateDescription($str) {                    
		if (mb_strlen($str) < 1) {                    
			throw new \Exception( "Строка с типом Description не должна состоять меньше чем из 1 символа." );                     
		}
		if (mb_strlen($str) > 8192) {                    
			throw new \Exception( "Строка с типом Description не должна состоять больше чем из 8192 символов." );                     
		}                    
	}

	static function validatePhone($num) {                    
		if (is_int($num)) {                
			throw new \Exception( "Число с типом Phone должно содержать целое число." );                
		}
		if (strlen($num) > 11) {                    
			throw new \Exception( "Число c типом Phone не должно состоять больше чем из 11 цифр." );                     
		}                    
	}

	static function validateSex($str) {                    
		if (!preg_match("^(undefined|male|female)$",$str)) {                    
			throw new \Exception( "Строка с типом Sex должна быть одним из значений: undefined,male,female." );                     
		}                    
	}

	static function validateState($str) {                    
		if (!preg_match("^(registrated|removed|active)$",$str)) {                    
			throw new \Exception( "Строка с типом State должна быть одним из значений: registrated,removed,active." );                     
		}                    
	}

	static function validateDrivingLicence($str) {                    
		if (!preg_match("^(A|B|C|D|E)$",$str)) {                    
			throw new \Exception( "Строка с типом DrivingLicence должна быть одним из значений: A,B,C,D,E." );                     
		}                    
	}

	static function validateDrivingLicences($list) {                    
		foreach($list as $item) {            
			BaseTypeValidator::validateDrivingLicence($item);            
		}                    
	}

	static function validateDate($date) {                    
		$dateAsString = $date->format("Y-m-d");                    
		if (!preg_match(".*", $dateAsString)) {                    
			throw new \Exception( "Date/Time с типом Date должна совпадать по шаблону .*." );                     
		}                    
	}

	static function validateTime($time) {                    
		$timeAsString = $time->format("H:i:s");                    
		if (!preg_match(".*", $timeAsString)) {                    
			throw new \Exception( "Date/Time с типом Time должна совпадать по шаблону .*." );                     
		}                    
	}

	static function validateDateTime($dateTime) {                    
		$dateTimeAsString = $dateTime->format("Y-m-d H:i:s");                    
		if (!preg_match(".*", $dateTimeAsString)) {                    
			throw new \Exception( "Date/Time с типом DateTime должна совпадать по шаблону .*." );                     
		}                    
	}

	static function validateLogin($str) {                    
		if (mb_strlen($str) < 1) {                    
			throw new \Exception( "Строка с типом Login не должна состоять меньше чем из 1 символа." );                     
		}
		if (!preg_match(".*", $str)) {                    
			throw new \Exception( "Строка с типом Login должна совпадать по шаблону .*." );                     
		}                    
	}

	static function validateNodeTypes($str) {                    
		if (!preg_match("^(super|user|community)$",$str)) {                    
			throw new \Exception( "Строка с типом NodeTypes должна быть одним из значений: super,user,community." );                     
		}                    
	}

	static function validateColorWord($str) {                    
		if (!preg_match("^(white|yellow|blue|green|brown|red|orange|pink|grey|black)$",$str)) {                    
			throw new \Exception( "Строка с типом ColorWord должна быть одним из значений: white,yellow,blue,green,brown,red,orange,pink,grey,black." );                     
		}                    
	}

	static function validateColorCode($str) {                    
		if (!preg_match("#[0-9a-f]{6}", $str)) {                    
			throw new \Exception( "Строка с типом ColorCode должна совпадать по шаблону #[0-9a-f]{6}." );                     
		}                    
	}

	static function validateColor($item) {                    
		$isValidated = true;                
		try {                
			BaseTypeValidator::validateColorWord($item);                
		} catch (\Exception $ex) {                
			$isValidated = false;                
		}                
		if ($isValidated) {                
			return;                
		}
		$isValidated = true;                
		try {                
			BaseTypeValidator::validateColorCode($item);                
		} catch (\Exception $ex) {                
			$isValidated = false;                
		}                
		if ($isValidated) {                
			return;                
		}
		throw new \Exception("Свойство Color должно соответствовать одному из типу: ColorWord|ColorCode.");                    
	}

	static function validateColors($list) {                    
		foreach($list as $item) {            
			BaseTypeValidator::validateColor($item);            
		}                    
	}

	static function validateFiveFavoriteColors($list) {                    
		if (count($list) != 5) {                    
			throw new \Exception( "Список с типом FiveFavoriteColors не должен состоять больше или меньше чем из 5 элементов." );                     
		}
		foreach($list as $item) {            
			BaseTypeValidator::validateColor($item);            
		}                    
	}

	static function validateRole($str) {                    
		if (!preg_match("^(guest|user|owner|admin|member)$",$str)) {                    
			throw new \Exception( "Строка с типом Role должна быть одним из значений: guest,user,owner,admin,member." );                     
		}                    
	}

	static function validateRoles($list) {                    
		foreach($list as $item) {            
			BaseTypeValidator::validateRole($item);            
		}                    
	}

	static function validateHash($str) {                    
		if (mb_strlen($str) != 32) {                    
			throw new \Exception( "Строка с типом Hash не должна состоять больше или меньше чем из 32 символов." );                     
		}                    
	}

	static function validateCommunityType($str) {                    
		if (!preg_match("^(open|lock)$",$str)) {                    
			throw new \Exception( "Строка с типом CommunityType должна быть одним из значений: open,lock." );                     
		}                    
	}

	static function validateListOfLong($list) {                    
		foreach($list as $item) {            
			XsdTypeValidator::validateLong($item);            
		}                    
	}

	static function validateMapForStringAndString($map) {                    
		foreach($map as $key => $value) {            
			BaseTypeValidator::validateEntryForStringAndString($key, $value);            
		}                    
	}

	static function validateEntryForStringAndString($key, $value) {                    
		try {                    
			XsdTypeValidator::validateString($key);                    
		} catch (\Exception $ex) {                    
			ValidateResult::$errors["key"] = $ex->getMessage();                    
		}
		try {                    
			XsdTypeValidator::validateString($value);                    
		} catch (\Exception $ex) {                    
			ValidateResult::$errors["value"] = $ex->getMessage();                    
		}                    
	}

	static function validateMapForStringAndLong($map) {                    
		foreach($map as $key => $value) {            
			BaseTypeValidator::validateEntryForStringAndLong($key, $value);            
		}                    
	}

	static function validateEntryForStringAndLong($key, $value) {                    
		try {                    
			XsdTypeValidator::validateString($key);                    
		} catch (\Exception $ex) {                    
			ValidateResult::$errors["key"] = $ex->getMessage();                    
		}
		try {                    
			XsdTypeValidator::validateLong($value);                    
		} catch (\Exception $ex) {                    
			ValidateResult::$errors["value"] = $ex->getMessage();                    
		}                    
	}

	static function validateMapForStringAndListOfLong($map) {                    
		foreach($map as $key => $value) {            
			BaseTypeValidator::validateEntryForStringAndListOfLong($key, $value);            
		}                    
	}

	static function validateEntryForStringAndListOfLong($key, $value) {                    
		try {                    
			XsdTypeValidator::validateString($key);                    
		} catch (\Exception $ex) {                    
			ValidateResult::$errors["key"] = $ex->getMessage();                    
		}
		try {                    
			BaseTypeValidator::validateListOfLong($value);                    
		} catch (\Exception $ex) {                    
			ValidateResult::$errors["value"] = $ex->getMessage();                    
		}                    
	}                    
}                    
?>