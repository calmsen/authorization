<?php                    
/**                    
 * Данный класс содержит статические функции для проверки переменных на валидность                    
 *                    
 * @author Ruslan Rakhmankulov                    
 */                    
class ImageTypeValidator {                    

	static function validateImage($obj) {                    
		try {            
			if ($obj->getId() !== null) {                
				BaseTypeValidator::validateId($obj->getId());                
			}            
		} catch (Exception $ex) {            
			ValidateResult::$errors["id"] = $ex->getMessage();            
		}
		try {            
			if ($obj->getName() !== null) {                
				BaseTypeValidator::validateAlias($obj->getName());                
			}            
		} catch (Exception $ex) {            
			ValidateResult::$errors["name"] = $ex->getMessage();            
		}
		try {            
			if ($obj->getTitle() !== null) {                
				BaseTypeValidator::validateName($obj->getTitle());                
			}            
		} catch (Exception $ex) {            
			ValidateResult::$errors["title"] = $ex->getMessage();            
		}                    
	}

	static function validateImages($list) {                    
		foreach($list as $item) {            
			ImageTypeValidator::validateImage($item);            
		}                    
	}                    
}                    
?>