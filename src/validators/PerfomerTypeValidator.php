<?php                    
/**                    
 * Данный класс содержит статические функции для проверки переменных на валидность                    
 *                    
 * @author Ruslan Rakhmankulov                    
 */                    
class PerfomerTypeValidator {                    

	static function validatePerfomer($obj) {                    
		Validator::validateUser($obj);
		try {            
			if ($obj->getSecretary() !== null) {                
				UserTypeValidator::validateUser($obj->getSecretary());                
			}            
		} catch (Exception $ex) {            
			ValidateResult::$errors["secretary"] = $ex->getMessage();            
		}                    
	}

	static function validateDetailedPerfomer($obj) {                    
		Validator::validateDetailedUser($obj);
		try {            
			if ($obj->get() !== null) {                
				UserTypeValidator::validateUser($obj->get());                
			}            
		} catch (Exception $ex) {            
			ValidateResult::$errors[""] = $ex->getMessage();            
		}                    
	}                    
}                    
?>