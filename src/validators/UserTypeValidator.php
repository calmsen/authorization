<?php                    
/**                    
 * Данный класс содержит статические функции для проверки переменных на валидность                    
 *                    
 * @author Ruslan Rakhmankulov                    
 */                    
class UserTypeValidator extends Validator {                    

	static function validateUser($obj) {                    
		// Проверим в кэше                
		if (UserTypeValidator::$caches["userCache"] === null) {                
			UserTypeValidator::$caches["userCache"] = [];                
		}                
		if (in_array($obj->getId(), UserTypeValidator::$caches["userCache"])) {                
			return;                
		}                
		UserTypeValidator::$caches["userCache"][] = $obj->getId();                
		//
		try {            
			if ($obj->getId() !== null) {                
				BaseTypeValidator::validateId($obj->getId());                
			}            
		} catch (\Exception $ex) {            
			ValidateResult::$errors["id"] = "Идентификатор пользователя не может быть меньше 0.";            
		}
		try {            
			if ($obj->getNick() !== null) {                
				BaseTypeValidator::validateAlias($obj->getNick());                
			}            
		} catch (\Exception $ex) {            
			ValidateResult::$errors["nick"] = "Ник пользователя может содержать только латинские буквы, цифры и символ тире.";            
		}
		try {            
			if ($obj->getFirstName() !== null) {                
				BaseTypeValidator::validateName($obj->getFirstName());                
			}            
		} catch (\Exception $ex) {            
			ValidateResult::$errors["firstName"] = $ex->getMessage();            
		}
		try {            
			if ($obj->getMiddleName() !== null) {                
				BaseTypeValidator::validateName($obj->getMiddleName());                
			}            
		} catch (\Exception $ex) {            
			ValidateResult::$errors["middleName"] = $ex->getMessage();            
		}
		try {            
			if ($obj->getLastName() !== null) {                
				BaseTypeValidator::validateName($obj->getLastName());                
			}            
		} catch (\Exception $ex) {            
			ValidateResult::$errors["lastName"] = $ex->getMessage();            
		}
		try {            
			if ($obj->getFullName() !== null) {                
				BaseTypeValidator::validateName($obj->getFullName());                
			}            
		} catch (\Exception $ex) {            
			ValidateResult::$errors["fullName"] = $ex->getMessage();            
		}
		try {            
			if ($obj->getDescription() !== null) {                
				BaseTypeValidator::validateDescription($obj->getDescription());                
			}            
		} catch (\Exception $ex) {            
			ValidateResult::$errors["description"] = $ex->getMessage();            
		}
		try {            
			if ($obj->getPersonal() !== null) {                
				PersonalTypeValidator::validatePersonal($obj->getPersonal());                
			}            
		} catch (\Exception $ex) {            
			ValidateResult::$errors["personal"] = $ex->getMessage();            
		}
		try {            
			if ($obj->getAccounts() !== null) {                
				AccountTypeValidator::validateAccounts($obj->getAccounts());
				// проверим на уникальность элементов в массиве                
				foreach ($obj->getAccounts() as $item1) {                
					$finded = 0;                
					foreach ($obj->getAccounts() as $item2) {                
						if ($item1->getAccount()->getId() == $item2->getAccount()->getId() && $item1->getAccount()->getLogin() == $item2->getAccount()->getLogin()) {                
							$finded++;                
						}                
					}                
					if ($finded > 1) {                
						throw new \Exception("Массив содержит не уникальные элементы.");                
					}                
				}                                
			}            
		} catch (\Exception $ex) {            
			ValidateResult::$errors["accounts"] = $ex->getMessage();            
		}
		try {            
			if ($obj->getImages() !== null) {                
				ImageTypeValidator::validateImages($obj->getImages());                
			}            
		} catch (\Exception $ex) {            
			ValidateResult::$errors["images"] = $ex->getMessage();            
		}
		try {            
			if ($obj->getWebsite() !== null) {                
				BaseTypeValidator::validateWebsite($obj->getWebsite());                
			}            
		} catch (\Exception $ex) {            
			ValidateResult::$errors["website"] = $ex->getMessage();            
		}
		try {            
			if ($obj->getBirthday() !== null) {                
				XsdTypeValidator::validateDate($obj->getBirthday());                
			}            
		} catch (\Exception $ex) {            
			ValidateResult::$errors["birthday"] = $ex->getMessage();            
		}
		try {            
			if ($obj->getSex() !== null) {                
				BaseTypeValidator::validateSex($obj->getSex());                
			}            
		} catch (\Exception $ex) {            
			ValidateResult::$errors["sex"] = $ex->getMessage();            
		}
		try {            
			if ($obj->getDrivingLicences() !== null) {                
				BaseTypeValidator::validateDrivingLicences($obj->getDrivingLicences());                
			}            
		} catch (\Exception $ex) {            
			ValidateResult::$errors["drivingLicences"] = $ex->getMessage();            
		}
		try {            
			if ($obj->getFiveFavoriteColors() !== null) {                
				BaseTypeValidator::validateFiveFavoriteColors($obj->getFiveFavoriteColors());                
			}            
		} catch (\Exception $ex) {            
			ValidateResult::$errors["fiveFavoriteColors"] = $ex->getMessage();            
		}
		try {            
			if ($obj->getCreateDate() !== null) {                
				XsdTypeValidator::validateDate($obj->getCreateDate());                
			}            
		} catch (\Exception $ex) {            
			ValidateResult::$errors["createDate"] = $ex->getMessage();            
		}
		try {            
			if ($obj->getLastOnlineDate() !== null) {                
				XsdTypeValidator::validateDate($obj->getLastOnlineDate());                
			}            
		} catch (\Exception $ex) {            
			ValidateResult::$errors["lastOnlineDate"] = $ex->getMessage();            
		}
		try {            
			if ($obj->getModerateDate() !== null) {                
				XsdTypeValidator::validateDate($obj->getModerateDate());                
			}            
		} catch (\Exception $ex) {            
			ValidateResult::$errors["moderateDate"] = $ex->getMessage();            
		}
		try {            
			if ($obj->getState() !== null) {                
				BaseTypeValidator::validateState($obj->getState());                
			}            
		} catch (\Exception $ex) {            
			ValidateResult::$errors["state"] = $ex->getMessage();            
		}
		try {            
			if ($obj->getColor() !== null) {                
				BaseTypeValidator::validateColor($obj->getColor());                
			}            
		} catch (\Exception $ex) {            
			ValidateResult::$errors["color"] = $ex->getMessage();            
		}
		try {            
			if ($obj->getDirector() !== null) {                
				DirectorTypeValidator::validateDirector($obj->getDirector());                
			}            
		} catch (\Exception $ex) {            
			ValidateResult::$errors["director"] = $ex->getMessage();            
		}
		try {            
			if ($obj->getFriend() !== null) {                
				if($obj->getFriend() instanceof Director) {                    
					DirectorTypeValidator::validateDirector($obj->getFriend());                    
				} else {                
					UserTypeValidator::validateUser($obj->getFriend());                
				}                
			}            
		} catch (\Exception $ex) {            
			ValidateResult::$errors["friend"] = $ex->getMessage();            
		}
		try {            
			if ($obj->getKinsman() !== null) {                
				if($obj->getKinsman() instanceof Director) {                    
					DirectorTypeValidator::validateDirector($obj->getKinsman());                    
				} else {                
					UserTypeValidator::validateUser($obj->getKinsman());                
				}                
			}            
		} catch (\Exception $ex) {            
			ValidateResult::$errors["kinsman"] = $ex->getMessage();            
		}                    
	}

	static function validateUsers($list) {                    
		foreach($list as $item) {            
			UserTypeValidator::validateUser($item);            
		}                    
	}

	static function validateAddedUser($obj) {                    
		// Проверим в кэше                
		if (UserTypeValidator::$caches["addedUserCache"] === null) {                
			UserTypeValidator::$caches["addedUserCache"] = [];                
		}                
		if (in_array($obj->getId(), UserTypeValidator::$caches["addedUserCache"])) {                
			return;                
		}                
		UserTypeValidator::$caches["addedUserCache"][] = $obj->getId();                
		//
		try {            
			BaseTypeValidator::validateId($obj->getId());            
		} catch (\Exception $ex) {            
			ValidateResult::$errors["id"] = "Идентификатор пользователя не может быть меньше 0.";            
		}
		try {            
			AccountTypeValidator::validateAddedAccounts($obj->getAccounts());            
		} catch (\Exception $ex) {            
			ValidateResult::$errors["accounts"] = $ex->getMessage();            
		}                    
	}

	static function validateDetailedUser($obj) {                    
		// Проверим в кэше                
		if (UserTypeValidator::$caches["detailedUserCache"] === null) {                
			UserTypeValidator::$caches["detailedUserCache"] = [];                
		}                
		if (in_array($obj->getId(), UserTypeValidator::$caches["detailedUserCache"])) {                
			return;                
		}                
		UserTypeValidator::$caches["detailedUserCache"][] = $obj->getId();                
		//
		try {            
			BaseTypeValidator::validateId($obj->getId());            
		} catch (\Exception $ex) {            
			ValidateResult::$errors["id"] = "Идентификатор пользователя не может быть меньше 0.";            
		}
		try {            
			BaseTypeValidator::validateAlias($obj->getNick());            
		} catch (\Exception $ex) {            
			ValidateResult::$errors["nick"] = "Ник пользователя может содержать только латинские буквы, цифры и символ тире.";            
		}
		try {            
			if ($obj->getFirstName() !== null) {                
				BaseTypeValidator::validateName($obj->getFirstName());                
			}            
		} catch (\Exception $ex) {            
			ValidateResult::$errors["firstName"] = $ex->getMessage();            
		}
		try {            
			if ($obj->getMiddleName() !== null) {                
				BaseTypeValidator::validateName($obj->getMiddleName());                
			}            
		} catch (\Exception $ex) {            
			ValidateResult::$errors["middleName"] = $ex->getMessage();            
		}
		try {            
			if ($obj->getLastName() !== null) {                
				BaseTypeValidator::validateName($obj->getLastName());                
			}            
		} catch (\Exception $ex) {            
			ValidateResult::$errors["lastName"] = $ex->getMessage();            
		}
		try {            
			if ($obj->getWebsite() !== null) {                
				BaseTypeValidator::validateWebsite($obj->getWebsite());                
			}            
		} catch (\Exception $ex) {            
			ValidateResult::$errors["website"] = $ex->getMessage();            
		}
		try {            
			if ($obj->getBirthday() !== null) {                
				XsdTypeValidator::validateDate($obj->getBirthday());                
			}            
		} catch (\Exception $ex) {            
			ValidateResult::$errors["birthday"] = $ex->getMessage();            
		}
		try {            
			BaseTypeValidator::validateSex($obj->getSex());            
		} catch (\Exception $ex) {            
			ValidateResult::$errors["sex"] = $ex->getMessage();            
		}
		try {            
			XsdTypeValidator::validateDate($obj->getCreateDate());            
		} catch (\Exception $ex) {            
			ValidateResult::$errors["createDate"] = $ex->getMessage();            
		}
		try {            
			XsdTypeValidator::validateDate($obj->getModerateDate());            
		} catch (\Exception $ex) {            
			ValidateResult::$errors["moderateDate"] = $ex->getMessage();            
		}
		try {            
			BaseTypeValidator::validateState($obj->getState());            
		} catch (\Exception $ex) {            
			ValidateResult::$errors["state"] = $ex->getMessage();            
		}                    
	}

	static function validateSimplifiedUser($obj) {                    
		// Проверим в кэше                
		if (UserTypeValidator::$caches["simplifiedUserCache"] === null) {                
			UserTypeValidator::$caches["simplifiedUserCache"] = [];                
		}                
		if (in_array($obj->getId(), UserTypeValidator::$caches["simplifiedUserCache"])) {                
			return;                
		}                
		UserTypeValidator::$caches["simplifiedUserCache"][] = $obj->getId();                
		//
		try {            
			BaseTypeValidator::validateId($obj->getId());            
		} catch (\Exception $ex) {            
			ValidateResult::$errors["id"] = "Идентификатор пользователя не может быть меньше 0.";            
		}
		try {            
			BaseTypeValidator::validateAlias($obj->getNick());            
		} catch (\Exception $ex) {            
			ValidateResult::$errors["nick"] = "Ник пользователя может содержать только латинские буквы, цифры и символ тире.";            
		}
		try {            
			XsdTypeValidator::validateDate($obj->getLastOnlineDate());            
		} catch (\Exception $ex) {            
			ValidateResult::$errors["lastOnlineDate"] = $ex->getMessage();            
		}
		try {            
			BaseTypeValidator::validateState($obj->getState());            
		} catch (\Exception $ex) {            
			ValidateResult::$errors["state"] = $ex->getMessage();            
		}                    
	}                    
}                    
?>