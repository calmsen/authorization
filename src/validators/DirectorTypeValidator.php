<?php                    
/**                    
 * Данный класс содержит статические функции для проверки переменных на валидность                    
 *                    
 * @author Ruslan Rakhmankulov                    
 */                    
class DirectorTypeValidator {                    

	static function validateDirector($obj) {                    
		Validator::validateUser($obj);
		try {            
			if ($obj->getSecretary() !== null) {                
				UserTypeValidator::validateUser($obj->getSecretary());                
			}            
		} catch (Exception $ex) {            
			ValidateResult::$errors["secretary"] = $ex->getMessage();            
		}                    
	}                    
}                    
?>