<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
use Calmsen\Component\QueryBuilder\QueryBuilder;
use Calmsen\Component\Parser\ParserXmlSchema;
use Calmsen\Component\Parser\ParserWadl;
use Calmsen\Component\Utils\Utils;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
/**
 * Description of Env
 *
 * @author rl
 */
class Env {
    /** Определение переменных для базы данных */
    static $DB_SERVER;
    static $DB_USER;
    static $DB_PASS;
    static $DB;
    /** Путь до приложения */
    static $APPL_CONTEXT;
    
    static $DOCUMENT_APPL_ROOT;
    static $CLASSES_ROOT;
    static $SCHEMAS_ROOT;
    static $WADLS_ROOT;
    
    static $HTTP_APPL_ROOT;
    static $HTTP_SCHEMAS_ROOT;
    
    static $DBH;
    static $URL_INFO;
    
    static function create() {     
        /** Определеим абсолютные пути */
        Env::$DOCUMENT_APPL_ROOT = $_SERVER["DOCUMENT_ROOT"] . Env::$APPL_CONTEXT;

        Env::$CLASSES_ROOT = Env::$DOCUMENT_APPL_ROOT . "/src";    
        Env::$SCHEMAS_ROOT = Env::$DOCUMENT_APPL_ROOT . "/web/schemas";
        Env::$WADLS_ROOT = Env::$DOCUMENT_APPL_ROOT . "/web/wadls";  
        
        /** Настройка загрузчика классов */
        require_once Env::$DOCUMENT_APPL_ROOT . "/app/bootstrap.php";    
        
        /** Откроем соединение */
        Env::$DBH = new PDO("mysql:host=" . Env::$DB_SERVER . ";dbname=" . Env::$DB . "", Env::$DB_USER, Env::$DB_PASS);
        /** Test
         * echo "<pre>";
         * Env::$DBH = new PDO("mysql:host=localhost;dbname=nerv", "root", "");
         * $STH = Env::$DBH->prepare("SELECT * FROM users");
         * $STH->execute();
         * 
         * print_r($STH->fetchAll());
        */
        QueryBuilder::$DBH = Env::$DBH;

        /** Определеим http пути */
        Env::$URL_INFO = Utils::parseUri();
        
        Env::$HTTP_APPL_ROOT = Env::$URL_INFO["scheme"] . "://" . Env::$URL_INFO["host"] . (Env::$URL_INFO["port"] != 80 && Env::$URL_INFO["port"] != 443 ? ":" . Env::$URL_INFO["port"] : "") . Env::$APPL_CONTEXT;
        Env::$HTTP_SCHEMAS_ROOT = "http://calmsen.com/schemas";
        
        /** Создадим классы и валидации форм */
        $pxs = new ParserXmlSchema(Env::$HTTP_SCHEMAS_ROOT, Env::$SCHEMAS_ROOT, Env::$CLASSES_ROOT);
        $pxs->transformXsdToClass(Env::$SCHEMAS_ROOT . "/requests/TestRequest.xsd");
//        /** Создадим интерфейсы контроллеров */
//        $pw = new ParserWadl(Env::$SCHEMAS_ROOT, Env::$CLASSES_ROOT, $pxs);
//        $pw->transformWadlToClass(Env::$WADLS_ROOT . "/UserWadl.xml");
    }
}

?>
