<?php                    
/**                    
 * Данный интерфейс описывает методы которые должны быть реадизованы в контроллере                    
 *                    
 * @author Ruslan Rakhmankulov                    
 */                    
interface IUserController {                                

	/**            
	 * @resource(path = "/nodes/main/login", method = "GET", headers = {} matrixs = {"1;roles=guest"});            
	 */            
	public function getLoginForm(GetLoginFormResponse $output);            

	/**            
	 * @resource(path = "/nodes/main/login", method = "PUT", headers = {} matrixs = {"1;roles=guest"});            
	 */            
	public function login(LoginRequest $input, LoginResponse $output);            

	/**            
	 * @resource(path = "/nodes/main/password/remind", method = "GET", headers = {} matrixs = {"1;roles=guest"});            
	 */            
	public function getRemindForm(GetRemindPasswordFormResponse $output);            

	/**            
	 * @resource(path = "/nodes/main/password/remind", method = "PUT", headers = {} matrixs = {"1;roles=guest"});            
	 */            
	public function remind(RemindPasswordRequest $input, RemindPasswordResponse $output);            

	/**            
	 * @resource(path = "/nodes/main/password/recovery", method = "GET", headers = {} matrixs = {"1;roles=guest"});            
	 */            
	public function getRecoveryForm(GetRecoveryPassworFormdRequest $input, GetRecoveryPasswordFormResponse $output);            

	/**            
	 * @resource(path = "/nodes/main/password/recovery", method = "PUT", headers = {} matrixs = {"1;roles=guest"});            
	 */            
	public function recovery(RecoveryPasswordRequest $input);            

	/**            
	 * @resource(path = "/nodes/main/registration", method = "GET", headers = {} matrixs = {"1;roles=guest"});            
	 */            
	public function getAddedUserForm(GetAddedUserResponse $output);            

	/**            
	 * @resource(path = "/nodes/main/registration", method = "POST", headers = {} matrixs = {"1;roles=guest"});            
	 */            
	public function addUser(AddUserRequest $input, AddUserResponse $output);            

	/**            
	 * @resource(path = "/nodes/main/logout", method = "PUT", headers = {} matrixs = {"1;roles=user"});            
	 */            
	public function logout();            

	/**            
	 * @resource(path = "/users/{userName}", method = "GET", headers = {} matrixs = {"1;roles=owner+guest"});            
	 */            
	public function getUser(UserResponse $output);            

	/**            
	 * @resource(path = "/users/{userName}/activate", method = "GET", headers = {} matrixs = {"1;roles=owner+guest"});            
	 */            
	public function activateUser(ActivateUserResponse $output);            

	/**            
	 * @resource(path = "/users/{userName}/edit", method = "GET", headers = {} matrixs = {"1;roles=owner"});            
	 */            
	public function getEditFormUser(GetEditUserFormResponse $output);            

	/**            
	 * @resource(path = "/users/{userName}/edit", method = "PUT", headers = {} matrixs = {"1;roles=owner"});            
	 */            
	public function updateUser(SaveUserRequest $input, SaveUserResponse $output);            

	/**            
	 * @resource(path = "/users/{userName}/edit", method = "DELETE", headers = {} matrixs = {"1;roles=owner"});            
	 */            
	public function deleteUser();                    
}                    
?>