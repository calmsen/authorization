<?php                        
/**                        
 * Данный класс описывает модель                        
 *                        
 * @author Ruslan Rakhmankulov                        
 */                        
class SaveUserRequest {                        

	/**                
	 * @var DetailedUser                
	 */                
	private $user;


	public function getUser($user) {                        
		return$this->user;                        
	}

	public function setUser($user) {                        
		$this->user = $user;                        
		return$this;                        
	}                        
}                        
?>