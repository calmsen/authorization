<?php                        
/**                        
 * Данный класс описывает модель                        
 *                        
 * @author Ruslan Rakhmankulov                        
 */                        
class LoginRequest {                        

	/**                
	 * @var DetailedAccount                
	 */                
	private $account;


	public function getAccount($account) {                        
		return$this->account;                        
	}

	public function setAccount($account) {                        
		$this->account = $account;                        
		return$this;                        
	}                        
}                        
?>