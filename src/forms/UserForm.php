<?php                        
/**                        
 * Данный класс описывает форму                        
 *                        
 * @author Ruslan Rakhmankulov                        
 */                        
class UserForm {                        

	/**                        
	 * @var FormField[] as map                        
	 */                        
	protected $fields;                        

	/**                        
	 * @return FormField[] as map                        
	 */                        
	public function getFields() {                        
		return $this->fields;                        
	}                        

	/**                        
	 * @param FormField[] as map $fields                        
	 * @return UserForm                        
	 */                        
	public function setFields($fields) {                        
		$this->fields = $fields;                        
		return $this;                        
	}                        

	function __constructor(){                
		$this->fields = array();                

		$this->fields["id"] = (new FormField())
			->setName("id")
			->setType("number");

		$this->fields["nick"] = (new FormField())
			->setName("nick")
			->setType("text")
			->setMaxlength("32")
			->setPattern(".*");

		$this->fields["firstName"] = (new FormField())
			->setName("firstName")
			->setType("text")
			->setMaxlength("32")
			->setPattern(".*");

		$this->fields["middleName"] = (new FormField())
			->setName("middleName")
			->setType("text")
			->setMaxlength("32")
			->setPattern(".*");

		$this->fields["lastName"] = (new FormField())
			->setName("lastName")
			->setType("text")
			->setMaxlength("32")
			->setPattern(".*");

		$this->fields["description"] = (new FormField())
			->setName("description")
			->setType("text")
			->setMaxlength("8192");

		$this->fields["personal"] = (new FormField())
			->setName("personal")
			->setType("number");

		$this->fields["accounts"] = (new FormField())
			->setName("accounts")
			->setType("text");

		$this->fields["images"] = (new FormField())
			->setName("images")
			->setType("text");

		$this->fields["website"] = (new FormField())
			->setName("website")
			->setType("text")
			->setMaxlength("64")
			->setPattern(".*");

		$this->fields["birthday"] = (new FormField())
			->setName("birthday")
			->setType("date");

		$this->fields["sex"] = (new FormField())
			->setName("sex")
			->setType("text")
			->setDefaultValue("undefined");

		$this->fields["drivingLicences"] = (new FormField())
			->setName("drivingLicences")
			->setType("text");

		$this->fields["fiveFavoriteColors"] = (new FormField())
			->setName("fiveFavoriteColors")
			->setType("text");

		$this->fields["createDate"] = (new FormField())
			->setName("createDate")
			->setType("date");

		$this->fields["lastOnlineDate"] = (new FormField())
			->setName("lastOnlineDate")
			->setType("date");

		$this->fields["moderateDate"] = (new FormField())
			->setName("moderateDate")
			->setType("date");

		$this->fields["state"] = (new FormField())
			->setName("state")
			->setType("text")
			->setDefaultValue("registrated");

		$this->fields["color"] = (new FormField())
			->setName("color")
			->setType("color");

		$this->fields["director"] = (new FormField())
			->setName("director")
			->setType("number");

		$this->fields["friend"] = (new FormField())
			->setName("friend")
			->setType("number");

		$this->fields["kinsman"] = (new FormField())
			->setName("kinsman")
			->setType("number");                
	}                        
}                        
?>