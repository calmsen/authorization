<?php                        
/**                        
 * Данный класс описывает форму                        
 *                        
 * @author Ruslan Rakhmankulov                        
 */                        
class DetailedPerfomerForm extends DetailedUserForm {                                                

	function __constructor(){                
		parent::DetailedUserForm();                

		$this->fields["secretary"] = (new FormField())
			->setName("secretary")
			->setType("number");                
	}                        
}                        
?>