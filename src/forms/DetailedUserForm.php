<?php                        
/**                        
 * Данный класс описывает форму                        
 *                        
 * @author Ruslan Rakhmankulov                        
 */                        
class DetailedUserForm {                        

	/**                        
	 * @var FormField[] as map                        
	 */                        
	protected $fields;                        

	/**                        
	 * @return FormField[] as map                        
	 */                        
	public function getFields() {                        
		return $this->fields;                        
	}                        

	/**                        
	 * @param FormField[] as map $fields                        
	 * @return DetailedUserForm                        
	 */                        
	public function setFields($fields) {                        
		$this->fields = $fields;                        
		return $this;                        
	}                        

	function __constructor(){                
		$this->fields = array();                

		$this->fields["id"] = (new FormField())
			->setName("id")
			->setType("number");

		$this->fields["nick"] = (new FormField())
			->setName("nick")
			->setType("text")
			->setMaxlength("32")
			->setPattern(".*");

		$this->fields["firstName"] = (new FormField())
			->setName("firstName")
			->setType("text")
			->setMaxlength("32")
			->setPattern(".*");

		$this->fields["middleName"] = (new FormField())
			->setName("middleName")
			->setType("text")
			->setMaxlength("32")
			->setPattern(".*");

		$this->fields["lastName"] = (new FormField())
			->setName("lastName")
			->setType("text")
			->setMaxlength("32")
			->setPattern(".*");

		$this->fields["website"] = (new FormField())
			->setName("website")
			->setType("text")
			->setMaxlength("64")
			->setPattern(".*");

		$this->fields["birthday"] = (new FormField())
			->setName("birthday")
			->setType("date");

		$this->fields["sex"] = (new FormField())
			->setName("sex")
			->setType("text")
			->setDefaultValue("undefined");

		$this->fields["createDate"] = (new FormField())
			->setName("createDate")
			->setType("date");

		$this->fields["moderateDate"] = (new FormField())
			->setName("moderateDate")
			->setType("date");

		$this->fields["state"] = (new FormField())
			->setName("state")
			->setType("text")
			->setDefaultValue("registrated");                
	}                        
}                        
?>