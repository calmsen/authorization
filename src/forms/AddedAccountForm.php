<?php                        
/**                        
 * Данный класс описывает форму                        
 *                        
 * @author Ruslan Rakhmankulov                        
 */                        
class AddedAccountForm {                        

	/**                        
	 * @var FormField[] as map                        
	 */                        
	private $fields;                        

	/**                        
	 * @return FormField[] as map                        
	 */                        
	public function getFields() {                        
		return $this->fields;                        
	}                        

	/**                        
	 * @param FormField[] as map $fields                        
	 * @return AddedAccountForm                        
	 */                        
	public function setFields($fields) {                        
		$this->fields = $fields;                        
		return $this;                        
	}                        

	function __constructor(){                
		$this->fields = array();                

		$this->fields["id"] = (new FormField())
			->setName("id")
			->setType("number");

		$this->fields["login"] = (new FormField())
			->setName("login")
			->setType("text")
			->setPattern(".*");

		$this->fields["password"] = (new FormField())
			->setName("password")
			->setType("text")
			->setMaxlength("16")
			->setPattern(".*");                
	}                        
}                        
?>