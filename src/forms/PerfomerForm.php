<?php                        
/**                        
 * Данный класс описывает форму                        
 *                        
 * @author Ruslan Rakhmankulov                        
 */                        
class PerfomerForm extends UserForm {                                                

	function __constructor(){                
		parent::UserForm();                

		$this->fields["secretary"] = (new FormField())
			->setName("secretary")
			->setType("number");                
	}                        
}                        
?>