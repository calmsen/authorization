<?php                        
/**                        
 * Данный класс описывает форму                        
 *                        
 * @author Ruslan Rakhmankulov                        
 */                        
class AccountForm {                        

	/**                        
	 * @var FormField[] as map                        
	 */                        
	private $fields;                        

	/**                        
	 * @return FormField[] as map                        
	 */                        
	public function getFields() {                        
		return $this->fields;                        
	}                        

	/**                        
	 * @param FormField[] as map $fields                        
	 * @return AccountForm                        
	 */                        
	public function setFields($fields) {                        
		$this->fields = $fields;                        
		return $this;                        
	}                        

	function __constructor(){                
		$this->fields = array();                

		$this->fields["id"] = (new FormField())
			->setName("id")
			->setType("number");

		$this->fields["login"] = (new FormField())
			->setName("login")
			->setType("text")
			->setPattern(".*");

		$this->fields["password"] = (new FormField())
			->setName("password")
			->setType("text")
			->setMaxlength("16")
			->setPattern(".*");

		$this->fields["user"] = (new FormField())
			->setName("user")
			->setType("number");

		$this->fields["el1"] = (new FormField())
			->setName("el1")
			->setType("text")
			->setRequired("1");

		$this->fields["el2"] = (new FormField())
			->setName("el2")
			->setType("text")
			->setRequired("1");

		$this->fields["el3"] = (new FormField())
			->setName("el3")
			->setType("text")
			->setRequired("1");

		$this->fields["el4"] = (new FormField())
			->setName("el4")
			->setType("text");

		$this->fields["el5"] = (new FormField())
			->setName("el5")
			->setType("text")
			->setRequired("1");                
	}                        
}                        
?>