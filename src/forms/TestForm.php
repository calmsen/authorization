<?php                        
/**                        
 * Данный класс описывает форму                        
 *                        
 * @author Ruslan Rakhmankulov                        
 */                        
class TestForm {                        

	/**                        
	 * @var FormField[] as map                        
	 */                        
	protected $fields;                        

	/**                        
	 * @return FormField[] as map                        
	 */                        
	public function getFields() {                        
		return $this->fields;                        
	}                        

	/**                        
	 * @param FormField[] as map $fields                        
	 * @return TestForm                        
	 */                        
	public function setFields($fields) {                        
		$this->fields = $fields;                        
		return $this;                        
	}                        

	function __constructor(){                
		$this->fields = array();                

		$this->fields["id"] = (new FormField())
			->setName("id")
			->setType("number");

		$this->fields["email"] = (new FormField())
			->setName("email")
			->setType("email")
			->setMaxlength("32")
			->setPattern(".*");                
	}                        
}                        
?>