<?php                        
/**                        
 * Данный класс описывает форму                        
 *                        
 * @author Ruslan Rakhmankulov                        
 */                        
class ImageForm {                        

	/**                        
	 * @var FormField[] as map                        
	 */                        
	private $fields;                        

	/**                        
	 * @return FormField[] as map                        
	 */                        
	public function getFields() {                        
		return $this->fields;                        
	}                        

	/**                        
	 * @param FormField[] as map $fields                        
	 * @return ImageForm                        
	 */                        
	public function setFields($fields) {                        
		$this->fields = $fields;                        
		return $this;                        
	}                        

	function __constructor(){                
		$this->fields = array();                

		$this->fields["id"] = (new FormField())
			->setName("id")
			->setType("number");

		$this->fields["name"] = (new FormField())
			->setName("name")
			->setType("text")
			->setMaxlength("32")
			->setPattern(".*");

		$this->fields["title"] = (new FormField())
			->setName("title")
			->setType("text")
			->setMaxlength("32")
			->setPattern(".*");                
	}                        
}                        
?>