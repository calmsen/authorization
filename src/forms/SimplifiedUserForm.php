<?php                        
/**                        
 * Данный класс описывает форму                        
 *                        
 * @author Ruslan Rakhmankulov                        
 */                        
class SimplifiedUserForm {                        

	/**                        
	 * @var FormField[] as map                        
	 */                        
	protected $fields;                        

	/**                        
	 * @return FormField[] as map                        
	 */                        
	public function getFields() {                        
		return $this->fields;                        
	}                        

	/**                        
	 * @param FormField[] as map $fields                        
	 * @return SimplifiedUserForm                        
	 */                        
	public function setFields($fields) {                        
		$this->fields = $fields;                        
		return $this;                        
	}                        

	function __constructor(){                
		$this->fields = array();                

		$this->fields["id"] = (new FormField())
			->setName("id")
			->setType("number");

		$this->fields["nick"] = (new FormField())
			->setName("nick")
			->setType("text")
			->setMaxlength("32")
			->setPattern(".*");

		$this->fields["lastOnlineDate"] = (new FormField())
			->setName("lastOnlineDate")
			->setType("date");

		$this->fields["state"] = (new FormField())
			->setName("state")
			->setType("text")
			->setDefaultValue("registrated");                
	}                        
}                        
?>