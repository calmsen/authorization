<?php                        
/**                        
 * Данный класс описывает форму                        
 *                        
 * @author Ruslan Rakhmankulov                        
 */                        
class DirectorForm extends UserForm {                                                

	function __constructor(){                
		parent::UserForm();                

		$this->fields["secretary"] = (new FormField())
			->setName("secretary")
			->setType("number");                
	}                        
}                        
?>