<?php                            
/**                            
 * Данный класс описывает модель                            
 *                            
 * @author Ruslan Rakhmankulov                            
 */                            
class UserProperty {                            
	const ID = "id";
	const NICK = "nick";
	const FIRST_NAME = "firstName";
	const MIDDLE_NAME = "middleName";
	const LAST_NAME = "lastName";
	const FULL_NAME = "fullName";
	const DESCRIPTION = "description";
	const PERSONAL = "personal";
	const ACCOUNTS = "accounts";
	const IMAGES = "images";
	const WEBSITE = "website";
	const BIRTHDAY = "birthday";
	const SEX = "sex";
	const DRIVING_LICENCES = "drivingLicences";
	const FIVE_FAVORITE_COLORS = "fiveFavoriteColors";
	const CREATE_DATE = "createDate";
	const LAST_ONLINE_DATE = "lastOnlineDate";
	const MODERATE_DATE = "moderateDate";
	const STATE = "state";
	const COLOR = "color";
	const DIRECTOR = "director";
	const FRIEND = "friend";
	const KINSMAN = "kinsman";                            
}                            
?>