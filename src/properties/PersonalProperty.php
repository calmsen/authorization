<?php                            
/**                            
 * Данный класс описывает модель                            
 *                            
 * @author Ruslan Rakhmankulov                            
 */                            
class PersonalProperty {                            
	const ID = "id";
	const EMAIL = "email";
	const PHONE = "phone";                            
}                            
?>