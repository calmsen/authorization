<?php                                
/**                                
 * Данный класс описывает перечисление                                
 *                                
 * @author Ruslan Rakhmankulov                                
 */                                
class RoleEnum {                                
	const GUEST = "guest";
	const USER = "user";
	const OWNER = "owner";
	const ADMIN = "admin";
	const MEMBER = "member";                                
}                                
?>