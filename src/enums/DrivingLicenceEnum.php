<?php                                
/**                                
 * Данный класс описывает перечисление                                
 *                                
 * @author Ruslan Rakhmankulov                                
 */                                
class DrivingLicenceEnum {                                
	const A = "A";
	const B = "B";
	const C = "C";
	const D = "D";
	const E = "E";                                
}                                
?>