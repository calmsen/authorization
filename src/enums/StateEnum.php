<?php                                
/**                                
 * Данный класс описывает перечисление                                
 *                                
 * @author Ruslan Rakhmankulov                                
 */                                
class StateEnum {                                
	const REGISTRATED = "registrated";
	const REMOVED = "removed";
	const ACTIVE = "active";                                
}                                
?>