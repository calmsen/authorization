<?php                                
/**                                
 * Данный класс описывает перечисление                                
 *                                
 * @author Ruslan Rakhmankulov                                
 */                                
class ColorWordEnum {                                
	const WHITE = "white";
	const YELLOW = "yellow";
	const BLUE = "blue";
	const GREEN = "green";
	const BROWN = "brown";
	const RED = "red";
	const ORANGE = "orange";
	const PINK = "pink";
	const GREY = "grey";
	const BLACK = "black";                                
}                                
?>