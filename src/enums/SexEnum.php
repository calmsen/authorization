<?php                                
/**                                
 * Данный класс описывает перечисление                                
 *                                
 * @author Ruslan Rakhmankulov                                
 */                                
class SexEnum {                                
	const UNDEFINED = "undefined";
	const MALE = "male";
	const FEMALE = "female";                                
}                                
?>