<?php                                
/**                                
 * Данный класс описывает перечисление                                
 *                                
 * @author Ruslan Rakhmankulov                                
 */                                
class NodeTypesEnum {                                
	const SUPER = "super";
	const USER = "user";
	const COMMUNITY = "community";                                
}                                
?>