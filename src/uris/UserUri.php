<?php                    
/**                    
 * Данный класс содержит константы с URI                    
 *                    
 * @author Ruslan Rakhmankulov                    
 */                    
class UserUri {                    
	const LOGIN = "/login";
	const PASSWORD_REMIND = "/password/remind";
	const PASSWORD_RECOVERY = "/password/recovery";
	const REGISTRATION = "/registration";
	const LOGOUT = "/logout";
	const USERS = "/users/{userName}";
	const USERS_ACTIVATE = "/users/{userName}/activate";
	const USERS_EDIT = "/users/{userName}/edit";                    
}                    
?>