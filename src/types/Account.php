<?php                        
/**                        
 * Данный класс описывает модель                        
 *                        
 * @author Ruslan Rakhmankulov                        
 */                        
class Account {                        

	/**                
	 * @var int                
	 */                
	private $id;

	/**                
	 * @var string                
	 */                
	private $login;

	/**                
	 * @var string                
	 */                
	private $password;

	/**                
	 * @var User                
	 */                
	private $user;

	/**                
	 * @var string                
	 */                
	private $el1;

	/**                
	 * @var string                
	 */                
	private $el2;

	/**                
	 * @var string                
	 */                
	private $el3;

	/**                
	 * @var string                
	 */                
	private $el4;

	/**                
	 * @var string                
	 */                
	private $el5;


	/**                
	 * @return int                
	 */                
	public function getId() {                
		return$this->id;                
	}

	/**                
	 * @param int $id                
	 * @return Account                
	 */                
	public function setId($id) {                
		$this->id = $id;                
		return$this;                
	}

	/**                
	 * @return string                
	 */                
	public function getLogin() {                
		return$this->login;                
	}

	/**                
	 * @param string $login                
	 * @return Account                
	 */                
	public function setLogin($login) {                
		$this->login = $login;                
		return$this;                
	}

	/**                
	 * @return string                
	 */                
	public function getPassword() {                
		return$this->password;                
	}

	/**                
	 * @param string $password                
	 * @return Account                
	 */                
	public function setPassword($password) {                
		$this->password = $password;                
		return$this;                
	}

	/**                
	 * @return User                
	 */                
	public function getUser() {                
		return$this->user;                
	}

	/**                
	 * @param User $user                
	 * @return Account                
	 */                
	public function setUser($user) {                
		$this->user = $user;                
		return$this;                
	}

	/**                
	 * @return string                
	 */                
	public function getEl1() {                
		return$this->el1;                
	}

	/**                
	 * @param string $el1                
	 * @return Account                
	 */                
	public function setEl1($el1) {                
		$this->el1 = $el1;                
		return$this;                
	}

	/**                
	 * @return string                
	 */                
	public function getEl2() {                
		return$this->el2;                
	}

	/**                
	 * @param string $el2                
	 * @return Account                
	 */                
	public function setEl2($el2) {                
		$this->el2 = $el2;                
		return$this;                
	}

	/**                
	 * @return string                
	 */                
	public function getEl3() {                
		return$this->el3;                
	}

	/**                
	 * @param string $el3                
	 * @return Account                
	 */                
	public function setEl3($el3) {                
		$this->el3 = $el3;                
		return$this;                
	}

	/**                
	 * @return string                
	 */                
	public function getEl4() {                
		return$this->el4;                
	}

	/**                
	 * @param string $el4                
	 * @return Account                
	 */                
	public function setEl4($el4) {                
		$this->el4 = $el4;                
		return$this;                
	}

	/**                
	 * @return string                
	 */                
	public function getEl5() {                
		return$this->el5;                
	}

	/**                
	 * @param string $el5                
	 * @return Account                
	 */                
	public function setEl5($el5) {                
		$this->el5 = $el5;                
		return$this;                
	}

	function __constructor(){                
		$this->id = 0;                
	}                        
}                        
?>