<?php                        
/**                        
 * Данный класс описывает модель                        
 *                        
 * @author Ruslan Rakhmankulov                        
 */                        
class DetailedUser {                        

	/**                
	 * @var int                
	 */                
	protected $id;

	/**                
	 * @var string                
	 */                
	protected $nick;

	/**                
	 * @var string                
	 */                
	protected $firstName;

	/**                
	 * @var string                
	 */                
	protected $middleName;

	/**                
	 * @var string                
	 */                
	protected $lastName;

	/**                
	 * @var string                
	 */                
	protected $website;

	/**                
	 * @var DateTime                
	 */                
	protected $birthday;

	/**                
	 * @var string                
	 */                
	protected $sex;

	/**                
	 * @var DateTime                
	 */                
	protected $createDate;

	/**                
	 * @var DateTime                
	 */                
	protected $moderateDate;

	/**                
	 * @var string                
	 */                
	protected $state;


	/**                
	 * @return int                
	 */                
	public function getId() {                
		return$this->id;                
	}

	/**                
	 * @param int $id                
	 * @return DetailedUser                
	 */                
	public function setId($id) {                
		$this->id = $id;                
		return$this;                
	}

	/**                
	 * @return string                
	 */                
	public function getNick() {                
		return$this->nick;                
	}

	/**                
	 * @param string $nick                
	 * @return DetailedUser                
	 */                
	public function setNick($nick) {                
		$this->nick = $nick;                
		return$this;                
	}

	/**                
	 * @return string                
	 */                
	public function getFirstName() {                
		return$this->firstName;                
	}

	/**                
	 * @param string $firstName                
	 * @return DetailedUser                
	 */                
	public function setFirstName($firstName) {                
		$this->firstName = $firstName;                
		return$this;                
	}

	/**                
	 * @return string                
	 */                
	public function getMiddleName() {                
		return$this->middleName;                
	}

	/**                
	 * @param string $middleName                
	 * @return DetailedUser                
	 */                
	public function setMiddleName($middleName) {                
		$this->middleName = $middleName;                
		return$this;                
	}

	/**                
	 * @return string                
	 */                
	public function getLastName() {                
		return$this->lastName;                
	}

	/**                
	 * @param string $lastName                
	 * @return DetailedUser                
	 */                
	public function setLastName($lastName) {                
		$this->lastName = $lastName;                
		return$this;                
	}

	/**                
	 * @return string                
	 */                
	public function getWebsite() {                
		return$this->website;                
	}

	/**                
	 * @param string $website                
	 * @return DetailedUser                
	 */                
	public function setWebsite($website) {                
		$this->website = $website;                
		return$this;                
	}

	/**                
	 * @return DateTime                
	 */                
	public function getBirthday() {                
		return$this->birthday;                
	}

	/**                
	 * @param DateTime $birthday                
	 * @return DetailedUser                
	 */                
	public function setBirthday($birthday) {                
		$this->birthday = $birthday;                
		return$this;                
	}

	/**                
	 * @return string                
	 */                
	public function getSex() {                
		return$this->sex;                
	}

	/**                
	 * @param string $sex                
	 * @return DetailedUser                
	 */                
	public function setSex($sex) {                
		$this->sex = $sex;                
		return$this;                
	}

	/**                
	 * @return DateTime                
	 */                
	public function getCreateDate() {                
		return$this->createDate;                
	}

	/**                
	 * @param DateTime $createDate                
	 * @return DetailedUser                
	 */                
	public function setCreateDate($createDate) {                
		$this->createDate = $createDate;                
		return$this;                
	}

	/**                
	 * @return DateTime                
	 */                
	public function getModerateDate() {                
		return$this->moderateDate;                
	}

	/**                
	 * @param DateTime $moderateDate                
	 * @return DetailedUser                
	 */                
	public function setModerateDate($moderateDate) {                
		$this->moderateDate = $moderateDate;                
		return$this;                
	}

	/**                
	 * @return string                
	 */                
	public function getState() {                
		return$this->state;                
	}

	/**                
	 * @param string $state                
	 * @return DetailedUser                
	 */                
	public function setState($state) {                
		$this->state = $state;                
		return$this;                
	}

	function __constructor(){                
		$this->id = 0;
		$this->sex = "undefined";
		$this->state = "registrated";                
	}                        
}                        
?>