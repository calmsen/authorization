<?php                        
/**                        
 * Данный класс описывает модель                        
 *                        
 * @author Ruslan Rakhmankulov                        
 */                        
class Personal {                        

	/**                
	 * @var int                
	 */                
	protected $id;

	/**                
	 * @var string                
	 */                
	protected $email;

	/**                
	 * @var int                
	 */                
	protected $phone;


	/**                
	 * @return int                
	 */                
	public function getId() {                
		return$this->id;                
	}

	/**                
	 * @param int $id                
	 * @return Personal                
	 */                
	public function setId($id) {                
		$this->id = $id;                
		return$this;                
	}

	/**                
	 * @return string                
	 */                
	public function getEmail() {                
		return$this->email;                
	}

	/**                
	 * @param string $email                
	 * @return Personal                
	 */                
	public function setEmail($email) {                
		$this->email = $email;                
		return$this;                
	}

	/**                
	 * @return int                
	 */                
	public function getPhone() {                
		return$this->phone;                
	}

	/**                
	 * @param int $phone                
	 * @return Personal                
	 */                
	public function setPhone($phone) {                
		$this->phone = $phone;                
		return$this;                
	}

	function __constructor(){                
		$this->id = 0;
		$this->phone = 0;                
	}                        
}                        
?>