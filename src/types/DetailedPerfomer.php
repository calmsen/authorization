<?php                        
/**                        
 * Данный класс описывает модель                        
 *                        
 * @author Ruslan Rakhmankulov                        
 */                        
class DetailedPerfomer extends DetailedUser {                        

	/**                
	 * @var User                
	 */                
	protected $secretary;


	/**                
	 * @return User                
	 */                
	public function getSecretary() {                
		return$this->secretary;                
	}

	/**                
	 * @param User $secretary                
	 * @return DetailedPerfomer                
	 */                
	public function setSecretary($secretary) {                
		$this->secretary = $secretary;                
		return$this;                
	}                        
}                        
?>