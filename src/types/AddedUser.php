<?php                        
/**                        
 * Данный класс описывает модель                        
 *                        
 * @author Ruslan Rakhmankulov                        
 */                        
class AddedUser {                        

	/**                
	 * @var int                
	 */                
	protected $id;

	/**                
	 * @var AddedAccount[]                
	 */                
	protected $accounts;


	/**                
	 * @return int                
	 */                
	public function getId() {                
		return$this->id;                
	}

	/**                
	 * @param int $id                
	 * @return AddedUser                
	 */                
	public function setId($id) {                
		$this->id = $id;                
		return$this;                
	}

	/**                
	 * @return AddedAccount[]                
	 */                
	public function getAccounts() {                
		return$this->accounts;                
	}

	/**                
	 * @param AddedAccount[] $accounts                
	 * @return AddedUser                
	 */                
	public function setAccounts($accounts) {                
		$this->accounts = $accounts;                
		return$this;                
	}

	function __constructor(){                
		$this->id = 0;
		$this->accounts = array();                
	}                        
}                        
?>