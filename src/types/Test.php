<?php                        
/**                        
 * Данный класс описывает модель                        
 *                        
 * @author Ruslan Rakhmankulov                        
 */                        
class Test {                        

	/**                
	 * @var int                
	 */                
	protected $id;

	/**                
	 * @var string                
	 */                
	protected $email;


	/**                
	 * @return int                
	 */                
	public function getId() {                
		return$this->id;                
	}

	/**                
	 * @param int $id                
	 * @return Test                
	 */                
	public function setId($id) {                
		$this->id = $id;                
		return$this;                
	}

	/**                
	 * @return string                
	 */                
	public function getEmail() {                
		return$this->email;                
	}

	/**                
	 * @param string $email                
	 * @return Test                
	 */                
	public function setEmail($email) {                
		$this->email = $email;                
		return$this;                
	}

	function __constructor(){                
		$this->id = 0;                
	}                        
}                        
?>