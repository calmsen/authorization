<?php                        
/**                        
 * Данный класс описывает модель                        
 *                        
 * @author Ruslan Rakhmankulov                        
 */                        
class AddedAccount {                        

	/**                
	 * @var int                
	 */                
	private $id;

	/**                
	 * @var string                
	 */                
	private $login;

	/**                
	 * @var string                
	 */                
	private $password;


	/**                
	 * @return int                
	 */                
	public function getId() {                
		return$this->id;                
	}

	/**                
	 * @param int $id                
	 * @return AddedAccount                
	 */                
	public function setId($id) {                
		$this->id = $id;                
		return$this;                
	}

	/**                
	 * @return string                
	 */                
	public function getLogin() {                
		return$this->login;                
	}

	/**                
	 * @param string $login                
	 * @return AddedAccount                
	 */                
	public function setLogin($login) {                
		$this->login = $login;                
		return$this;                
	}

	/**                
	 * @return string                
	 */                
	public function getPassword() {                
		return$this->password;                
	}

	/**                
	 * @param string $password                
	 * @return AddedAccount                
	 */                
	public function setPassword($password) {                
		$this->password = $password;                
		return$this;                
	}

	function __constructor(){                
		$this->id = 0;                
	}                        
}                        
?>