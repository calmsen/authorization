<?php                        
/**                        
 * Данный класс описывает модель                        
 *                        
 * @author Ruslan Rakhmankulov                        
 */                        
class Perfomer extends User {                        

	/**                
	 * @var User                
	 */                
	protected $secretary;


	/**                
	 * @return User                
	 */                
	public function getSecretary() {                
		return$this->secretary;                
	}

	/**                
	 * @param User $secretary                
	 * @return Perfomer                
	 */                
	public function setSecretary($secretary) {                
		$this->secretary = $secretary;                
		return$this;                
	}                        
}                        
?>