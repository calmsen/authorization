<?php                        
/**                        
 * Данный класс описывает модель                        
 *                        
 * @author Ruslan Rakhmankulov                        
 */                        
class Director extends User {                        

	/**                
	 * @var User                
	 */                
	protected $secretary;


	/**                
	 * @return User                
	 */                
	public function getSecretary() {                
		return$this->secretary;                
	}

	/**                
	 * @param User $secretary                
	 * @return Director                
	 */                
	public function setSecretary($secretary) {                
		$this->secretary = $secretary;                
		return$this;                
	}                        
}                        
?>