<?php                        
/**                        
 * Данный класс описывает модель                        
 *                        
 * @author Ruslan Rakhmankulov                        
 */                        
class Image {                        

	/**                
	 * @var int                
	 */                
	private $id;

	/**                
	 * @var string                
	 */                
	private $name;

	/**                
	 * @var string                
	 */                
	private $title;


	/**                
	 * @return int                
	 */                
	public function getId() {                
		return$this->id;                
	}

	/**                
	 * @param int $id                
	 * @return Image                
	 */                
	public function setId($id) {                
		$this->id = $id;                
		return$this;                
	}

	/**                
	 * @return string                
	 */                
	public function getName() {                
		return$this->name;                
	}

	/**                
	 * @param string $name                
	 * @return Image                
	 */                
	public function setName($name) {                
		$this->name = $name;                
		return$this;                
	}

	/**                
	 * @return string                
	 */                
	public function getTitle() {                
		return$this->title;                
	}

	/**                
	 * @param string $title                
	 * @return Image                
	 */                
	public function setTitle($title) {                
		$this->title = $title;                
		return$this;                
	}

	function __constructor(){                
		$this->id = 0;                
	}                        
}                        
?>