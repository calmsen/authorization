<?php                        
/**                        
 * Данный класс описывает модель                        
 *                        
 * @author Ruslan Rakhmankulov                        
 */                        
class SimplifiedUser {                        

	/**                
	 * @var int                
	 */                
	protected $id;

	/**                
	 * @var string                
	 */                
	protected $nick;

	/**                
	 * @var DateTime                
	 */                
	protected $lastOnlineDate;

	/**                
	 * @var string                
	 */                
	protected $state;


	/**                
	 * @return int                
	 */                
	public function getId() {                
		return$this->id;                
	}

	/**                
	 * @param int $id                
	 * @return SimplifiedUser                
	 */                
	public function setId($id) {                
		$this->id = $id;                
		return$this;                
	}

	/**                
	 * @return string                
	 */                
	public function getNick() {                
		return$this->nick;                
	}

	/**                
	 * @param string $nick                
	 * @return SimplifiedUser                
	 */                
	public function setNick($nick) {                
		$this->nick = $nick;                
		return$this;                
	}

	/**                
	 * @return DateTime                
	 */                
	public function getLastOnlineDate() {                
		return$this->lastOnlineDate;                
	}

	/**                
	 * @param DateTime $lastOnlineDate                
	 * @return SimplifiedUser                
	 */                
	public function setLastOnlineDate($lastOnlineDate) {                
		$this->lastOnlineDate = $lastOnlineDate;                
		return$this;                
	}

	/**                
	 * @return string                
	 */                
	public function getState() {                
		return$this->state;                
	}

	/**                
	 * @param string $state                
	 * @return SimplifiedUser                
	 */                
	public function setState($state) {                
		$this->state = $state;                
		return$this;                
	}

	function __constructor(){                
		$this->id = 0;
		$this->state = "registrated";                
	}                        
}                        
?>