<?php                        
/**                        
 * Данный класс описывает модель                        
 *                        
 * @author Ruslan Rakhmankulov                        
 */                        
class User {                        

	/**                
	 * @var int                
	 */                
	protected $id;

	/**                
	 * @var string                
	 */                
	protected $nick;

	/**                
	 * @var string                
	 */                
	protected $firstName;

	/**                
	 * @var string                
	 */                
	protected $middleName;

	/**                
	 * @var string                
	 */                
	protected $lastName;

	/**                
	 * @var string                
	 */                
	protected $fullName;

	/**                
	 * @var string                
	 */                
	protected $description;

	/**                
	 * @var Personal                
	 */                
	protected $personal;

	/**                
	 * @var Account[]                
	 */                
	protected $accounts;

	/**                
	 * @var Image[]                
	 */                
	protected $images;

	/**                
	 * @var string                
	 */                
	protected $website;

	/**                
	 * @var DateTime                
	 */                
	protected $birthday;

	/**                
	 * @var string                
	 */                
	protected $sex;

	/**                
	 * @var string[]                
	 */                
	protected $drivingLicences;

	/**                
	 * @var string[]                
	 */                
	protected $fiveFavoriteColors;

	/**                
	 * @var DateTime                
	 */                
	protected $createDate;

	/**                
	 * @var DateTime                
	 */                
	protected $lastOnlineDate;

	/**                
	 * @var DateTime                
	 */                
	protected $moderateDate;

	/**                
	 * @var string                
	 */                
	protected $state;

	/**                
	 * @var string                
	 */                
	protected $color;

	/**                
	 * @var Director                
	 */                
	protected $director;

	/**                
	 * @var User                
	 */                
	protected $friend;

	/**                
	 * @var User                
	 */                
	protected $kinsman;


	/**                
	 * @return int                
	 */                
	public function getId() {                
		return$this->id;                
	}

	/**                
	 * @param int $id                
	 * @return User                
	 */                
	public function setId($id) {                
		$this->id = $id;                
		return$this;                
	}

	/**                
	 * @return string                
	 */                
	public function getNick() {                
		return$this->nick;                
	}

	/**                
	 * @param string $nick                
	 * @return User                
	 */                
	public function setNick($nick) {                
		$this->nick = $nick;                
		return$this;                
	}

	/**                
	 * @return string                
	 */                
	public function getFirstName() {                
		return$this->firstName;                
	}

	/**                
	 * @param string $firstName                
	 * @return User                
	 */                
	public function setFirstName($firstName) {                
		$this->firstName = $firstName;                
		return$this;                
	}

	/**                
	 * @return string                
	 */                
	public function getMiddleName() {                
		return$this->middleName;                
	}

	/**                
	 * @param string $middleName                
	 * @return User                
	 */                
	public function setMiddleName($middleName) {                
		$this->middleName = $middleName;                
		return$this;                
	}

	/**                
	 * @return string                
	 */                
	public function getLastName() {                
		return$this->lastName;                
	}

	/**                
	 * @param string $lastName                
	 * @return User                
	 */                
	public function setLastName($lastName) {                
		$this->lastName = $lastName;                
		return$this;                
	}

	/**                
	 * @return string                
	 */                
	public function getFullName() {                
		return$this->fullName;                
	}

	/**                
	 * @param string $fullName                
	 * @return User                
	 */                
	public function setFullName($fullName) {                
		$this->fullName = $fullName;                
		return$this;                
	}

	/**                
	 * @return string                
	 */                
	public function getDescription() {                
		return$this->description;                
	}

	/**                
	 * @param string $description                
	 * @return User                
	 */                
	public function setDescription($description) {                
		$this->description = $description;                
		return$this;                
	}

	/**                
	 * @return Personal                
	 */                
	public function getPersonal() {                
		return$this->personal;                
	}

	/**                
	 * @param Personal $personal                
	 * @return User                
	 */                
	public function setPersonal($personal) {                
		$this->personal = $personal;                
		return$this;                
	}

	/**                
	 * @return Account[]                
	 */                
	public function getAccounts() {                
		return$this->accounts;                
	}

	/**                
	 * @param Account[] $accounts                
	 * @return User                
	 */                
	public function setAccounts($accounts) {                
		$this->accounts = $accounts;                
		return$this;                
	}

	/**                
	 * @return Image[]                
	 */                
	public function getImages() {                
		return$this->images;                
	}

	/**                
	 * @param Image[] $images                
	 * @return User                
	 */                
	public function setImages($images) {                
		$this->images = $images;                
		return$this;                
	}

	/**                
	 * @return string                
	 */                
	public function getWebsite() {                
		return$this->website;                
	}

	/**                
	 * @param string $website                
	 * @return User                
	 */                
	public function setWebsite($website) {                
		$this->website = $website;                
		return$this;                
	}

	/**                
	 * @return DateTime                
	 */                
	public function getBirthday() {                
		return$this->birthday;                
	}

	/**                
	 * @param DateTime $birthday                
	 * @return User                
	 */                
	public function setBirthday($birthday) {                
		$this->birthday = $birthday;                
		return$this;                
	}

	/**                
	 * @return string                
	 */                
	public function getSex() {                
		return$this->sex;                
	}

	/**                
	 * @param string $sex                
	 * @return User                
	 */                
	public function setSex($sex) {                
		$this->sex = $sex;                
		return$this;                
	}

	/**                
	 * @return string[]                
	 */                
	public function getDrivingLicences() {                
		return$this->drivingLicences;                
	}

	/**                
	 * @param string[] $drivingLicences                
	 * @return User                
	 */                
	public function setDrivingLicences($drivingLicences) {                
		$this->drivingLicences = $drivingLicences;                
		return$this;                
	}

	/**                
	 * @return string[]                
	 */                
	public function getFiveFavoriteColors() {                
		return$this->fiveFavoriteColors;                
	}

	/**                
	 * @param string[] $fiveFavoriteColors                
	 * @return User                
	 */                
	public function setFiveFavoriteColors($fiveFavoriteColors) {                
		$this->fiveFavoriteColors = $fiveFavoriteColors;                
		return$this;                
	}

	/**                
	 * @return DateTime                
	 */                
	public function getCreateDate() {                
		return$this->createDate;                
	}

	/**                
	 * @param DateTime $createDate                
	 * @return User                
	 */                
	public function setCreateDate($createDate) {                
		$this->createDate = $createDate;                
		return$this;                
	}

	/**                
	 * @return DateTime                
	 */                
	public function getLastOnlineDate() {                
		return$this->lastOnlineDate;                
	}

	/**                
	 * @param DateTime $lastOnlineDate                
	 * @return User                
	 */                
	public function setLastOnlineDate($lastOnlineDate) {                
		$this->lastOnlineDate = $lastOnlineDate;                
		return$this;                
	}

	/**                
	 * @return DateTime                
	 */                
	public function getModerateDate() {                
		return$this->moderateDate;                
	}

	/**                
	 * @param DateTime $moderateDate                
	 * @return User                
	 */                
	public function setModerateDate($moderateDate) {                
		$this->moderateDate = $moderateDate;                
		return$this;                
	}

	/**                
	 * @return string                
	 */                
	public function getState() {                
		return$this->state;                
	}

	/**                
	 * @param string $state                
	 * @return User                
	 */                
	public function setState($state) {                
		$this->state = $state;                
		return$this;                
	}

	/**                
	 * @return string                
	 */                
	public function getColor() {                
		return$this->color;                
	}

	/**                
	 * @param string $color                
	 * @return User                
	 */                
	public function setColor($color) {                
		$this->color = $color;                
		return$this;                
	}

	/**                
	 * @return Director                
	 */                
	public function getDirector() {                
		return$this->director;                
	}

	/**                
	 * @param Director $director                
	 * @return User                
	 */                
	public function setDirector($director) {                
		$this->director = $director;                
		return$this;                
	}

	/**                
	 * @return User                
	 */                
	public function getFriend() {                
		return$this->friend;                
	}

	/**                
	 * @param User $friend                
	 * @return User                
	 */                
	public function setFriend($friend) {                
		$this->friend = $friend;                
		return$this;                
	}

	/**                
	 * @return User                
	 */                
	public function getKinsman() {                
		return$this->kinsman;                
	}

	/**                
	 * @param User $kinsman                
	 * @return User                
	 */                
	public function setKinsman($kinsman) {                
		$this->kinsman = $kinsman;                
		return$this;                
	}

	function __constructor(){                
		$this->id = 0;
		$this->accounts = array();
		$this->images = array();
		$this->sex = "undefined";
		$this->drivingLicences = array();
		$this->fiveFavoriteColors = array();
		$this->state = "registrated";                
	}                        
}                        
?>