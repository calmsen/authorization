<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * @author Ruslan Rakhmankulov
 */
class UserController implements IUserController{

    public function getAddedUserForm(){}
    
    public function addUser(AddUserRequest $input, AddUserResponse $output) {
        if (isset($_SESSION["user"])) {
            throw new Exception("Bad Request");
        }
        $form = new AddUserForm();
        if (UserService::getUserByEmail($form->getEmail()) !== null) {
            throw new Exception("Bad Request");
        }
        $user = new User();
        $user->setEmail($form->getEmail());
        $user->setPassword(md5($form->getPassword()));
        $user->setState(State::REGISTRATED);
        UserService::saveUser($user);
        $hash = md5($form->getEmail() . $form->getPassword() . "вы будете взвешены!");

        $subject = "Подтверждение регистрации";
        $message = "Здравствуйте! Спасибо за регистрацию на сайте " . Env::$HTTP_APPL_ROOT . "\n
        Ваш логин: " . $user->getEmail() . "\n
        Ваш пароль: " . $form->getPassword() . "\n
        Перейдите по ссылке, чтобы активировать ваш аккаунт:\n
        " . Env::$HTTP_APPL_ROOT . sprintf(URLS::$ACTIVATE_USER_URL, $user->getId()) . "/activate?email=" . $email . "&hash=" . $hash . "\n
        С уважением, Администрация " . Env::$HOST . ".";
        $mail = mail($email, $subject, $message, "Content-type:text/plane; Charset=UTF-8\r\n"); //отправляем сообщение

        if (!$mail) {
            /**
             * TODO: сделать Rollback
             */
            return;
        }
        $model = new AddUserModel();
        $model->setUserId($userId);
        return $model;
    }

    public function getEditUserForm(){}
    
    public function saveUser() {
        if (!isset($_SESSION["user"])) {
            throw new Exception("Bad Request");
        }
        //найдем id пользователя
        sscanf(Env::$PATH, URLS::$USER_URL, $userId);
        $form = new SaveUserForm();
        if ($userId === null || $userId != $_SESSION["user"]["id"]) {
            throw new Exception("Bad Request");
        }
        $user = new User();
        $user->setId($userId);
        if ($form->getFirstName() !== null) {
            $user->setFirstName($form->getFirstName());
        }
        if ($form->getMiddleName() !== null) {
            $user->setMiddleName($form->getMiddleName());
        }
        if ($form->getLastName() !== null) {
            $user->setLastName($form->getLastName());
        }
        UserService::saveUser($user);
        $model = new SaveUserModel();
        return $model;
    }
    
    public function deleteUser() {        
        if (!isset($_SESSION["user"])) {
            throw new Exception("Bad Request");
        }
        $form = new DeleteUserForm();
        //найдем id пользователя
        sscanf(Env::$PATH, URLS::$USER_URL, $userId);
        if ($userId === null || $userId != $_SESSION["user"]["id"]) {
            throw new Exception("Bad Request");
        }
        $user = UserService::getUserById($userId);
        $user->setState(State::REMOVED);
        UserService::saveUser($user);
        $model = new DeleteUserModel();
        return $model;
    }

    public function activateUser() {
        $form = new ActivateUserForm();
        if ($form->getEmail() == "" || $form->getHash() == "") {
            throw new Exception("Bad Request");
        }
        $user = UserService::getUserByEmail($form->getEmail());
        //проверим актуальность даты
        if (strtotime($user->getCreateDate() . "+1 days") < strtotime($time)) {
            /**
             * TODO: удалим пользователя
             */
            throw new Exception("Bad Request");
        }

        $user->setState(State::ACTIVE);
        UserService::saveUser($user);
        $model = new ActivateUserModel();
        return $model;
    }

    public function getLoginForm(){}
    
    public function login() {
        if (!isset($_SESSION["user"])) {
            throw new Exception("Bad Request");
        }
        $form = new LoginForm();
        //Составим запрос для выбора пользователя
        $user = UserService::getUserByQuery(function($query) {
                    $query->where(UserProperty::EMAIL, Operands::EQ, ":" . UserProperty::EMAIL, $form->getEmail());
                    $query->where(UserProperty::PASSWORD, Operands::EQ, ":" . UserProperty::PASSWORD, md5($form->getPassword()));
                });
        if ($user === null) {
            throw new Exception("Bad Request");
        }
        $model = new LoginModel();
        return $model;
    }

    public function logout(){}
    
    public function getRemindPasswordForm(){}
    
    public function remindPassword(){}
    
    public function getRecoveryPasswordForm(){}
    
    public function recoveryPassword(){}

    public function getEditFormUser() {
        
    }

    public function getRecoveryForm() {
        
    }

    public function getRemindForm() {
        
    }

    public function recovery() {
        
    }

    public function remind() {
        
    }
    
}

?>
