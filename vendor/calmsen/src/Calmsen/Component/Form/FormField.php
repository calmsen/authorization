<?php
namespace Calmsen\Component\Form;
/**
 * Description of FormField
 *
 * @author rl
 */
class FormField {

    private $name;
    private $type;
    private $required;
    private $min;
    private $max;
    private $maxlength;
    private $pattern;
    private $defaultValue;
    
    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    public function getType() {
        return $this->type;
    }

    public function setType($type) {
        $this->type = $type;
        return $this;
    }

    public function getRequired() {
        return $this->required;
    }

    public function setRequired($required) {
        $this->required = $required;
        return $this;
    }

    public function getMin() {
        return $this->min;
    }

    public function setMin($min) {
        $this->min = $min;
        return $this;
    }

    public function getMax() {
        return $this->max;
    }

    public function setMax($max) {
        $this->max = $max;
        return $this;
    }

    public function getMaxlength() {
        return $this->maxlength;
    }

    public function setMaxlength($maxlength) {
        $this->maxlength = $maxlength;
        return $this;
    }

    public function getPattern() {
        return $this->pattern;
    }

    public function setPattern($pattern) {
        $this->pattern = $pattern;
        return $this;
    }

    public function getDefaultValue() {
        return $this->defaultValue;
    }

    public function setDefaultValue($defaultValue) {
        $this->defaultValue = $defaultValue;
        return $this;
    }

    function __construct() {
        $this->required = false;
        $this->min = 0;
        $this->max = 0;
        $this->maxlength = 0;
    }

}

?>
