<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Calmsen\Component\Router;
/**
 * URI должен быть оформлен по шаблону^
 * /{culture}-{city}/{nodeType}/{nodeName}/{list}/{sections}/{(yyyy/mm/dd|yyyy/mm|yyyy)}/{item}/{view}/page{page}
 *
 * @author rl
 */
class Router {
    private $slugTypes;
    private $nodeTypesEnum;
    private $_slugs;
    private $context;
    static $uri = array();
    static $nodeType;
    static $nodeName;
    static $slugs = array();
    static $archive;
    static $sections = array();
    static $page = 1;
    
    function __construct($slugTypes, $nodeTypesEnum, $slugs = array(), $context) {
        $this->slugTypes = $slugTypes;
        $this->nodeTypesEnum = $nodeTypesEnum;
        $this->_slugs = $slugs;
        $this->context = preg_replace("/^\/|\/$/", "", $context);
        // Узнаем контролер и метод
        $this->getMethodByUri();
    }
    
    private function parseUri(){
        if (count($this->_slugs) == 0) {
            return array(
                array("super", "mother")
            );
        }
        if ($this->_slugs[0] == $this->context) {
            array_splice($this->_slugs, 0, 1);
        }
        
        if (count($this->_slugs) == 0) {
            return array(
                array("super", "mother")
            );
        }
        if (count($this->_slugs) < 2 || !defined($this->nodeTypesEnum . "::" . strtoupper($this->_slugs[0]))) {
            $this->_slugs = array_merge(array("super", "mother"), $this->_slugs);
            $this->slugTypes["super"] = "system";
            $this->slugTypes["mother"] = "node";
        }
        $nodeType = $this->_slugs[0];
        $nodeName = $this->_slugs[1];
        $slugs = array();// все slug-и кроме разделов и архива
        $archive = "";
        $isArchiveSet = false; // если есть данные об архиве, то они должны быть следовать друг за другом
        $sections = array(); // slug-и, которые представляют разделы
        $isSectionSet = false; // если есть разделы, то они должны быть следовать друг за другом
        $page = 1;
        foreach ($this->_slugs as $key => $slug) {
            // Найдем данные об архиве
            if ($key >= 2 || $key <= 4) {
                if ($archive == "") {
                    $isArchiveSet = true;
                }
                if ($isArchiveSet && is_numeric($slug)) {
                    if ($archive != "") {
                        $archive .= ".";
                    }
                    $archive .= $slug;
                    continue;
                }
            }
            $isArchiveSet = false;
            // Найдем разделы
            if ($this->slugTypes[$slug] == "section") {
                if (count($sections) == 0) {
                    $isSectionSet = true;
                }
                if (!$isSectionSet) {
                    throw new Exception("Разделы должны следовать друг за другом.");
                }
                $sections[] = $slug;
                continue;
            }
            $isSectionSet = false;
            // Найдем текущую страницу
            if ($this->_slugs[$key + 1] === null && preg_match("/^page\\d+$/", $this->_slugs[$key])) {
                $page = intval(substr($this->_slugs[$key], 4));
                $page = $page > 0 ? $page : 1;
                continue;
            }
            if ($this->slugTypes[$slug] === null) {
                throw new Exception("Не известный slug - " . $slug . ".");
            }
            $slugs[] = $slug;
        }

        return array($nodeType, $nodeName, $slugs, $archive, $sections, $page);
        
    }
    
    private function parseDoc($doc) {
        preg_match("/@resource\((.*)\)/", $doc, $match);
        $paramsStr = explode(",", preg_replace("/\\s|\"/", "", $match[1]));
        $params = array();
        foreach ($paramsStr as $str) {
            $kv = explode("=", $str);
            $params[$kv[0]] = $kv[1];
        }
        return $params;
    }
    
    private function parseMethod($method) {
        $doc = $method->getDocComment();
        $docParams = $this->parseDoc($doc);
        if ($docParams["method"] != $_SERVER["REQUEST_METHOD"]) {
            throw new Exception("Метод доступен по запросу " . $docParams["method"] . ".");
        }
        // Приведем URI к виду для шаблона
        
        $path = preg_replace("/^\/|\/$|{archive}|{sections}|{page}/", "", $docParams["path"]);
        $slugs = explode("/", $path);
        if ($slugs[0] != "{type}"
                && (count($slugs) < 2 || !defined($this->nodeTypesEnum . "::" . strtoupper($slugs[0])))) {
            $slugs = array_merge(array("super", "mother"), $slugs);
        }
        $re = "/^\/" . implode("\/", $slugs) . "$/";
        $re = preg_replace("/\{.*\}/U", "[^\/]+", $re);
        
        // Проверим uri 
        if (!preg_match($re, self::$uri)) {
            throw new Exception("Не совпал URI по шаблону " . $re . ".");
        }
        $methodName = $method->getName();
        $methodParams = array_map(function($p){
            return array("paramName" => $p->getName(), "className" => $p->getClass()->getName());
        }, $method->getParameters());
        return array("slugs" => $slugs, "methodName" => $methodName, "methodParams" => $methodParams);
    }
    
    private function parseClass($className) {
        $class = new ReflectionClass($className);
        $methods = $class->getMethods();
        $methodsData = array();
        foreach ($methods as $method) {
            $methodData = null;
            try {
                $methodData = $this->parseMethod($method);
            } catch (Exception $ex) {
            }
            // Если параметры не равны null, то значит мы нашли метод
            if ($methodData !== null) {
                $methodsData[] = $methodData;
            }
        }
        if (count($methodsData) === 0) {
            throw new Exception("Не удалось найти метод, обрабатывающий данный url.");
        }
        if (count($methodsData) == 1) {
            $methodData = $methodsData[0];
        } else {
            $methodData = array_reduce($methodsData, function($m1, $m2){
                $l1 = count($m1["slugs"]);
                $l2 = count($m2["slugs"]);
                for ($i = 0; $i < $l1; $i++) {
                    $s1 = $m1["slugs"][$i];
                    for ($j = 0; $j < $l2; $j++) {
                        $s2 = $m2["slugs"][$i];
                        if ($s1 != $s2) {
                            if ($s2 == "[^\/]+") {
                                return $m2;
                            } else {
                                return $m1;
                            }
                        }
                    }   
                }
            });
        }
            
        return array("className" => $className, "slugs" => $methodData["slugs"], "methodName" => $methodData["methodName"], "methodParams" => $methodData["methodParams"]);
    }
    
    private function getMethodByUri() {
        // Распарсим uri
        $uriInfo = $this->parseUri();
        self::$nodeType = $uriInfo[0];
        self::$nodeName = $uriInfo[1];
        self::$slugs = $uriInfo[2] !== null ? $uriInfo[2] : array();
        self::$archive = $uriInfo[3] !== null ? $uriInfo[3] : "";
        self::$sections = $uriInfo[4] !== null ? $uriInfo[4] : array();
        self::$page = $uriInfo[5] !== null ? $uriInfo[5] : 1;
        
        self::$uri = "/" . implode("/", self::$slugs);
        // Узнаем метод
        $classesData = array();
        foreach (scandir("classes/icontrollers") as $fileName) {
            if ($fileName == "." || $fileName == "..") {
                continue;
            }
            $className = str_replace(".php", "", $fileName);
            $classData = null;
            try {
                $classData = $this->parseClass($className);
            } catch (Exception $ex) {
            }
            if ($classData !== null) {
                $classesData[] = $classData;
            }
        }
        if (count($classesData) == 0) {
            throw new Exception("Не удалось найти метод, обрабатывающий данный url.");
        }
        if (count($classesData) == 1) {
            $classeData = $classesData[0];
        } else {
            $classeData = array_reduce($classesData, function($c1, $c2){
                $l1 = count($c1["slugs"]);
                $l2 = count($c2["slugs"]);
                for ($i = 0; $i < $l1; $i++) {
                    $s1 = $c1["slugs"][$i];
                    for ($j = 0; $j < $l2; $j++) {
                        $s2 = $c2["slugs"][$i];
                        if ($s1 != $s2) {
                            if ($s2 == "[^\/]+") {
                                return $c2;
                            } else {
                                return $c1;
                            }
                        }
                    }   
                }
            });
        }
        // Параметры из 
        $uriParams = array();
        foreach($classeData["slugs"] as $key => $slug) {
            if (strpos($slug, "{") !== false) {
                $uriParams[substr($slug, 1, strlen($slug) - 1)] = self::$slugs[$key];
            }
        }
        $classeData["uriParams"] = $uriParams;
        echo "<pre>";
        print_r($classeData);
    }
}

?>
