<?php
namespace Calmsen\Component\Validator;
/**
 * Description of ValidateResult
 *
 * @author Ruslan Rakhmankulov
 */
class ValidateResult {
    /**
     * @var Map<string,string>
     */
    static $errors = array();
    /**
     * @var boolean 
     */
    static $hasErrors = false;

}

?>
