<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Calmsen\Component\QueryBuilder;
/**
 * Description of Query
 *
 * @author rl
 */
class QueryBuilder {
    static $DBH;
    private $preview = false;
    private $dbh;
    private $sth;
    private $error = false;
    private $query;
    private $select;
    private $from;
    /** $join содержит элементы join, соединенные элементами on. Для последнего элемента join элементы on находятся в $on */
    private $join;
    private $on;
    private $where;
    private $groupBy;
    private $having;
    private $orderBy;
    private $limit;
    private $selectBindParams = array();
    private $joinBindParams = array();
    private $whereBindParams = array();
    private $havingBindParams = array();
    private $outerAlias;
    private $sub = false;
    private $insertId;

    static function table($table, $alias = null) {
        return (new QueryBuilder($table, $alias))->setDbh(QueryBuilder::$DBH);
    }
    
    static function sub($table = null, $alias = null) {
        return (new QueryBuilder($table, $alias))->setSub(true);
    }
    
    static function preview($table = null, $alias = null) {
        return (new QueryBuilder($table, $alias))->setPreview(true);
    }

    function __construct($table = null, $alias = null) {
        $this->from($table, $alias);
    }
    
    public function getPreview() {
        return $this->preview;
    }

    public function setPreview($preview) {
        $this->preview = $preview;
        return $this;
    }

    public function getSub() {
        return $this->sub;
    }

    public function setSub($sub) {
        $this->sub = $sub;
        return $this;
    }
    public function getDbh() {
        return $this->dbh;
    }

    public function setDbh($dbh) {
        $this->dbh = $dbh;
        return $this;
    }

    public function from($table, $alias = null) {
        if ($table !== null) {
            $this->from = $this->selectSubQueryOrTable($table, $alias);
            $this->outerAlias = ($alias !== null ? $alias : $table);             
        }        
        return $this;
    }
    
    private function addBindParam(&$bindParams, $field2, $param, $quote = true) {
        if ($param === null) {
            return false;
        }
        $match = null;
        if (strpos($field2, "?") !== false || strpos($field2, ":") === 0 || preg_match("/.*(:[a-zA-Z0-9]+).*/", $field2, $match)) {
            if ($match != null) {
                $field2 = $match[1];
            }
            $bindParams[] = array("field" => $field2,"param" => $param, "quote" => $quote);
            return true;
        }
        return false;
    }
    
    private function addSelectBindParam($field2, $param, $quote = true) {
        return $this->addBindParam($this->selectBindParams, $field2, $param, $quote);
    }
    /**
     * 
     * @param string $field1 название поле. Может быть массивом.
     * @param string $operand шаблон выражения
     * @param string $field2 значение или плейсхолдер. Может быть массивом.
     * @param string $param значение, если $field2 передан как плейсхолдер. Может быть массивом. Так же 
     * если $param массив, а $field2 не массив, то $field2 преобразуется в массив автоматически.
     * @param boolean $quote указывает нужны ли ковычки. По умолчанию нужны.
     * @return string готовое выражение
     */
    private function uCondition($type, $field1, $operand, $field2, $param = null, $quote){
        if (is_object($field1)) {
            return "(" . $field1->{"get" . ucfirst($type)}() . ")";
        }
        $bindType = $type;
        // переопределим bindType для типа on
        if ($bindType == "on") {
            $bindType = "join";
        }
        
        $field2Str = null; // записываем сюда конечный результат
        
        // создадим массивы из $field2 и $param, если они таковыми не являются
        $field2Arr = null;
        $paramArr = null;
        
        if (is_array($field2)) {
            $field2Arr = $field2;
        } else {
            $field2Arr = [$field2];
        }
        
        if ($param !== null) {
            if (is_array($param)) {
                $paramArr = $param;
                // переопределим $field2Arr, если это требуется
                if ($field2 == "?" && count($param) > 1) {
                    $field2Arr = array_fill(0, count($param), '?');
                }
            } else {
                $paramArr = [$param];
            }
            
            if (count($field2Arr) != count($paramArr)) {
                throw new Exception("Не правильно переданы параметры \$field2 и \$param.");
            }
        }            
        
            
        // обработаем текущее условие
        $fields = [];
        foreach ($field2Arr as $key => $curField2) {
            if (is_object($curField2)) {
                // добавим привязанные параметры в наш объект
                array_push($this->whereBindParams, $curField2->getBindParams());
                
                $curField2 = $curField2->getQuery();
                
                if (strpos($operand, "(%s)") === false) {
                    $curField2 = "(" . $curField2 . ")";
                }
                $fields[] = $curField2;
            } else if ($paramArr !== null && !$this->addBindParam($this->{$bindType . "BindParams"}, $curField2, $paramArr[$key], $quote)) {
                if ($quote) {
                    $fields[] = "'" . $curField2 . "'";
                } else {
                    $fields[] = $curField2;
                }
            } else {
                $fields[] = $curField2;
            }
        }
        $field2Str = implode(",", $fields);
        
        return sprintf($operand, $field1, $field2Str);
    }
    
    private function onCondition($field1, $operand, $field2, $param = null, $quote){
        return $this->uCondition("on", $field1, $operand, $field2, $param, $quote);
    }
    
    private function whereCondition($field1, $operand, $field2, $param = null, $quote){
        return $this->uCondition("where", $field1, $operand, $field2, $param, $quote);
    }
    
    private function havingCondition($field1, $operand, $field2, $param = null, $quote){
        return $this->uCondition("having", $field1, $operand, $field2, $param, $quote);
    }

    private function uJoin($type, $table, $alias = null, $field1 = null, $operand = null, $field2 = null, $param = null, $quote = false) {
        // добавим от предыдущего join - а
        if ($this->join !== null) {
            $this->join .= " ON " . $this->getOn();
            // очистим для текущего join - а
            $this->on = null;
        }
        // если $table является объектом то вернем подзапрос иначе просто название таблицы
        $table = $this->joinSubQueryOrTable($table, $alias);
        // добавим к основному join - у
        $this->join .= $type . $table;
        // если переданы дополнительные поля, то вызовем условие on
        if ($field1 !== null) {
            $this->on($field1, $operand, $field2, $param, $quote);        
        }
        return $this;
    }

    public function join($table, $alias = null, $field1 = null, $operand = null, $field2 = null, $param = null, $quote = false) {
        return $this->uJoin(" JOIN ", $table, $alias, $field1, $operand, $field2, $param, $quote);
    }

    public function innerJoin($table, $alias = null, $field1 = null, $operand = null, $field2 = null, $param = null, $quote = false) {
        return join($table, $alias, $field1, $operand, $field2, $param, $quote);
    }

    public function crossJoin($table, $alias = null, $field1 = null, $operand = null, $field2 = null, $param = null, $quote = false) {
        return join($table, $alias, $field1, $operand, $field2, $param, $quote);
    }

    public function leftJoin($table, $alias = null, $field1 = null, $operand = null, $field2 = null, $param = null, $quote = false) {
        return $this->uJoin(" LEFT JOIN ", $table, $alias = null, $field1, $operand, $field2, $param, $quote);
    }

    public function leftOuterJoin($table, $alias = null, $field1 = null, $operand = null, $field2 = null, $param = null, $quote = false) {
        return leftJoin($table, $alias, $field1, $operand, $field2, $param, $quote);
    }

    public function rightJoin($table, $alias = null, $field1 = null, $operand = null, $field2 = null, $param = null, $quote = false) {
        return $this->uJoin(" RIGHT JOIN ", $table, $alias = null, $field1, $operand, $field2, $param, $quote);
    }

    public function rightOuterJoin($table, $alias = null, $field1 = null, $operand = null, $field2 = null, $param = null, $quote = false) {
        return rightJoin($table, $alias, $field1, $operand, $field2, $param, $quote);
    }

    public function outerJoin($table, $alias = null, $field1 = null, $operand = null, $field2 = null, $param = null, $quote = false) {
        return $this->uJoin(" OUTER JOIN ", $table, $alias = null, $field1, $operand, $field2, $param, $quote);
    }
    
    public function uOn($type, $field1, $operand = null, $field2 = null, $param = null, $quote = true) {
        $this->on .= $type . $this->onCondition($field1, $operand, $field2, $param, $quote);
        return $this;
    }
    
    public function on($field1, $operand = null, $field2 = null, $param = null, $quote = true) {
        return $this->uOn(($this->on !== null ? " AND " : ""), $field1, $operand, $field2, $param, $quote);
    }

    public function andOn($field1, $operand = null, $field2 = null, $param = null, $quote = true) {
        return $this->uOn(" AND ", $field1, $operand, $field2, $param, $quote);
    }

    public function orOn($field1, $operand = null, $field2 = null, $param = null, $quote = true) {
        return $this->uOn(" OR ", $field1, $operand, $field2, $param, $quote);
    }
    
    private function uWhere($type, $field1, $operand = null, $field2 = null, $param = null, $quote = true) {     
        $this->where .= $type . $this->whereCondition($field1, $operand, $field2, $param, $quote);
        return $this;
    }
    
    public function where($field1, $operand = null, $field2 = null, $param = null, $quote = true) {
        return $this->uWhere(($this->where !== null ? " AND " : ""), $field1, $operand, $field2, $param, $quote);
    }

    public function andWhere($field1, $operand = null, $field2 = null, $param = null, $quote = true) {
        return $this->uWhere(" AND ", $field1, $operand, $field2, $param, $quote);
    }

    public function orWhere($field1, $operand = null, $field2 = null, $param = null, $quote = true) {
        return $this->uWhere(" OR ", $field1, $operand, $field2, $param, $quote);
    }
    
    private function uHaving($type, $field1, $operand = null, $field2 = null, $param = null, $quote = true) {        
        $this->having .= $type . $this->havingCondition($field1, $operand, $field2, $param, $quote);
        return $this;
    }
    
    public function having($field1, $operand = null, $field2 = null, $param = null, $quote = true) {
        return $this->uHaving(($this->having !== null ? " AND " : ""), $field1, $operand, $field2, $param, $quote);
    }
    
    public function andHaving($field1, $operand = null, $field2 = null, $param = null, $quote = true) {
        return $this->uHaving(" AND ", $field1, $operand, $field2, $param, $quote);
    }

    public function orHaving($field1, $operand = null, $field2 = null, $param = null, $quote = true) {
        return $this->uHaving(" OR ", $field1, $operand, $field2, $param, $quote);
    }

    public function groupBy($groupBy) {
        if ($this->groupBy !== null) {
            $this->groupBy .= ", ";
        }
        $this->groupBy .= $groupBy;
        return $this;
    }

    public function orderBy($sort, $order) {
        if ($this->orderBy !== null) {
            $this->orderBy .= ", ";
        }
        $this->orderBy .= $sort . " " . $order;
        return $this;
    }

    public function limit($start, $count) {
        $this->limit = $start . ", " . $count;
        return $this;
    }
    
    public function getOn() {
        return $this->on;
    }
    
    public function getWhere() {
        return $this->where;
    }
    
    public function getHaving() {
        return $this->having;
    }
    
    private function buildQuery() {
         $this->query = "";
        if ($this->select != "") {
            $this->query .= "SELECT " . $this->select;
        } else {
            $this->query .= "SELECT *";
        }
        if ($this->from != "") {
            $this->query .= " FROM " . $this->from;
        }
        if ($this->join != "") {
            $this->query .= " " . $this->join;
        }        
        if ($this->on != "") {
            $this->query .= " ON " . $this->on;
        }
        if ($this->where != "") {
            $this->query .= " WHERE " . $this->where;
        }
        if ($this->groupBy != "") {
            $this->query .= " GROUP BY " . $this->groupBy;
        }
        if ($this->having != "") {
            $this->query .= " HAVING " . $this->having;
        }
        if ($this->orderBy != "") {
            $this->query .= " ORDER BY " . $this->orderBy;
        }
        if ($this->limit != "") {
            $this->query .= " LIMIT " . $this->limit;
        }
    }
    
    public function getQuery() {
        if ($this->query !== null) {
            return $this->query;
        }
        $this->buildQuery();
        
        return $this->query;
    }
    
    public function getBindParams() {
        return array_merge($this->selectBindParams, $this->joinBindParams, $this->whereBindParams, $this->havingBindParams);
    }
    
    private function uSubQueryOrTable($type, $table, $alias) {
        if (is_object($table)) {
            if ($alias !== null) {
                $table->setOuterAlias($alias);
            }                
            $table = $this->uSubQuery($type, $table);
        } else {
            $table .= ($alias !== null ? " " . $alias : "");
        }
        return $table;
    }
    
    private function joinSubQueryOrTable($table, $alias) {
        return $this->uSubQueryOrTable("join", $table, $alias);
    }
    
    private function selectSubQueryOrTable($table, $alias) {
        return $this->uSubQueryOrTable("select", $table, $alias);
    }
    
    private function uSubQuery($type, $qb) {
        // добавим привязанные параметры в наш объект
        array_push($this->{$type . "BindParams"}, $qb->getBindParams());
        
        $subQuery = "(" . $qb->getQuery() . ") as " . $qb->getOuterAlias();
        return $subQuery;
    }
    
    private function selectSubQuery($qb) {
        return $this->uSubQuery("select", $qb);
    }
    
    private function execute() {
        $this->sth = $this->dbh->prepare($this->query);
        $counter = 0;
        foreach ($this->getBindParams() as $bindParam) {
            $this->sth->bindParam(($bindParam["field"] == "?" ? ++$counter : $bindParam["field"])
                    , $bindParam["param"]
                    , ($bindParam["quote"] ? PDO::PARAM_STR : 
                        (is_int($bindParam["param"]) ? PDO::PARAM_INT : 
                            (is_bool($bindParam["param"]) ? PDO::PARAM_BOOL : PDO::PARAM_STR))));
        }
        $this->sth->execute();
        
        $this->error = $this->sth->errorCode() == 0;
        return $this->error;
    }
    /**
     * Метод селект. 
     * @param string $select выражение содержит одиночные выражения, соединенные запятыми. Может быть массивом выражений. Массив может содержать массивы, в которых должны содержаться $select $param $quote для одиночного выражения (н-р: IF(:field < 2,'yes','no') as response).
     * @param string $param значение параметра 
     * @param boolean $quote нужно ли экранировать. По умолчанию нужно.
     * @return \QueryBuilder
     */
    public function select($select = null, $param = null, $quote = true) {
        if ($select !== null) {
            $selectStr = null; // сюда записываем конечный результат
            // создадим массив из $select, если он таковым не является
            $selectArr = null;
            if (is_string($select)) {
                if ($param !== null) {
                    $selectArr = [array($select, $param, $quote)];
                } else {
                    $selectArr = [$select];
                }
            } else if (is_object($select)) {
                $selectArr = [$select];
            } else {
                $selectArr = $select;
            }
            // обработаем текущий селект
            $selectStr = implode(", ", array_map(function ($curSelect) {
                                $curSelectStr = null;
                                if (is_object($curSelect)) {
                                    // сформируем подзапрос 
                                    $curSelectStr = $this->selectSubQuery($curSelect);
                                } else if (is_array($curSelect)) {
                                    $this->addSelectBindParam($curSelect[0], $curSelect[1], $curSelect[2]);
                                    $curSelectStr = $curSelect[0];
                                } else {
                                    $curSelectStr = $curSelect;
                                }
                                return $curSelectStr;
                            }, $selectArr));

            $this->select = $selectStr;
        }
        
        $this->buildQuery();
        // если подзапрос то вернем объект
        if ($this->sub) {
            return $this;
        }
        // если в режиме отладки, то не выполняем запрос
        if ($this->preview) {
            return [];
        }
        // выполнение запроса
        if ($this->execute()) {
            return $this->sth->fetchAll(PDO::FETCH_ASSOC);
        } else {
            return [];
        }
    }

    public function one($select = null, $param = null, $quote = true) {
        $rows = $this->select($select, $param, $quote);
        if (count($rows) >= 1) {
            return $rows[0];
        } else {
            return null;
        }
    }
    
    public function min($select) {
        $rows = $this->select("MIN(" . $select . ") as minValue");
        if (count($rows) >= 1) {
            return floatval($rows[0]["minValue"]);
        } else {
            return 0;
        }
    }
    
    public function max($select) {
        $rows = $this->select("MAX(" . $select . ") as maxValue");
        if (count($rows) >= 1) {
            return floatval($rows[0]["maxValue"]);
        } else {
            return 0;
        }
    }
    
    public function avg($select) {
        $rows = $this->select("AVG(" . $select . ") as avgValue");
        if (count($rows) >= 1) {
            return floatval($rows[0]["avgValue"]);
        } else {
            return 0;
        }
    }
    
    public function sum($select) {
        $rows = $this->select("SUM(" . $select . ") as sumValue");
        if (count($rows) >= 1) {
            return floatval($rows[0]["sumValue"]);
        } else {
            return 0;
        }
    }
    
    public function count($select) {
        $rows = $this->select("COUNT(" . $select . ") as countValue");
        if (count($rows) >= 1) {
            return floatval($rows[0]["countValue"]);
        } else {
            return 0;
        }
    }
    
    public function getInsertId() {
        if ($this->insertId !== null) {
            return $this->insertId;
        }
        if ($this->preview) {
            return rand(1, 1000000);
        }
        if ($this->sub) {
            throw new Exception("Данная операция не может быть выполнена.");
        }
        return $this->dbh->lastInsertId();;
    }
    
    private function pickDataBindParams(&$data) {
        foreach ($data as $key => $value) {
            $bindData = [];
            if (is_array($value)) {
                if (count($value) < 3) {
                    $bindData[0] = ":" . $key;
                    $bindData[1] = $value[0];
                    $bindData[2] = $value[1];
                } else {
                    $bindData = $value;
                }
            } else {
                $bindData[0] = ":" . $key;
                $bindData[1] = $value;
                $bindData[2] = false;
            }
            $this->addSelectBindParam($bindData[0], $bindData[1], $bindData[2]);
            $data[$key] = $bindData[0];
        }
        return $bindParams;
    }
    
    public function insert($data) {
        // обработаем привязанные параметры
        $this->pickDataBindParams($data);
        
        $this->query = "INSERT INTO " . $this->from . " (" . implode(", ", array_keys($data)) . ") values (" . implode(", ", array_values($data)) . ")";
        // если подзапрос то вернем объект
        if ($this->sub) {
            return $this;
        }
        // если в реиме отладки, то не выполняем запрос
        if ($this->preview) {
            return false;
        }
        // Выполним запрос
        return $this->execute();
    }

    public function update($data) {
        // обработаем привязанные параметры
        $this->pickDataBindParams($data);
        
        $this->query = "UPDATE " . $this->from . " SET " . implode(", ", array_map(function ($key, $value) {
                                    return $key . " = " . $value;
                                }, array_keys($data), array_values($data)));
        if ($this->where != "") {
            $this->query .= " WHERE " . $this->where;
        }
        // если подзапрос то вернем объект
        if ($this->sub) {
            return $this;
        }
        // если в режиме отладки, то не выполняем запрос
        if ($this->preview) {
            return false;
        }
        // Выполним запрос
        return $this->execute();
    }

    public function delete() {
        $this->query = "DELETE FROM " . $this->from;
        if ($this->where != "") {
            $this->query .= " WHERE " . $this->where;
        }
        // если подзапрос то вернем объект
        if ($this->sub) {
            return $this;
        }
        // если в режиме отладки, то не выполняем запрос
        if ($this->preview) {
            return false;
        }
        // Выполним запрос
        return $this->execute();
    }
    
    public function setOuterAlias($outerAlias) {
        $this->outerAlias = $outerAlias;
        return $this;
    }
    
    public function getOuterAlias() {
        return $this->outerAlias;
    }
}
/**
 * Тесты
 */
//echo "<pre>";
//echo "Инициализация QueryBuilder для выполнения запроса:";
//echo "<br/>";
//QueryBuilder::table("users");
//echo "<br/>";
//echo "Инициализация QueryBuilder для составления подзапроса:";
//echo "<br/>";
//QueryBuilder::preview("users");
//echo "<br/>";
//echo "Инициализация QueryBuilder для просмотра запроса (Будем использовать такой способ для демонстрации возможностей QueryBuilder, в рабочем проекте будет использоваться table ):";
//echo "<br/>";
//QueryBuilder::preview("users");
//echo "Выберем записи из таблицы users: ";
//echo "<br/>";
//echo QueryBuilder::preview("users")->getQuery();
//echo "<br/>";
//echo "Выберем записи из таблицы users с одним фильтром: ";
//echo "<br/>";
//echo QueryBuilder::preview("users")->where("id", Operands::EQ, 124)->getQuery();
//echo "<br/>";
//echo "Выберем записи из таблицы users с одним фильтром без экранирования: ";
//echo "<br/>";
//echo QueryBuilder::preview("users")->where("id", Operands::EQ, 124, null, false)->getQuery();
//echo "<br/>";
//echo "Выберем записи из таблицы users с одним фильтром с плейхолдером: ";
//$qb = QueryBuilder::preview("users")->where("id", Operands::EQ, "?", 124, false);
//echo "<br/>";
//echo $qb->getQuery();
//echo "<br/>";
//echo "Плейсхолдеры:";
//print_r($qb->getBindParams());
//echo "<br/>";
//echo "Выберем записи из таблицы users с одним фильтром с именованным плейсхолдером: ";
//$qb = QueryBuilder::preview("users")->where("nick", Operands::EQ, ":nick", "luk");
//echo "<br/>";
//echo $qb->getQuery();
//echo "<br/>";
//echo "Плейсхолдеры:";
//print_r($qb->getBindParams());
//echo "<br/>";
//echo "Выберем записи из таблицы users с фильтрами: ";
//echo "<br/>";
//echo QueryBuilder::preview("users")
//        ->where("id", Operands::EQ, 124, null, false)
//        ->andWhere("nick", Operands::EQ, "luk")// аналогично ->where("nick", Operands::EQ, "luk")
//        ->getQuery();
//echo "<br/>";
//echo "Выберем записи из таблицы users с фильтрамм c оператором or: ";
//echo "<br/>";
//echo QueryBuilder::preview("users")
//        ->where("id", Operands::EQ, 124, null, false)
//        ->orWhere("nick", Operands::EQ, "luk")
//        ->getQuery();
//echo "<br/>";
//echo "Выберем записи из таблицы users с вложенными фильтрамм: ";
//echo "<br/>";
//echo QueryBuilder::preview("users")
//        ->where("id", Operands::EQ, 124, null, false)
//        ->andWhere(QueryBuilder::sub()
//                    ->where("nick", Operands::EQ, "luk")
//                    ->orWhere("nick", Operands::EQ, "obi"))
//        ->getQuery();
//echo "<br/>";
//echo "Выберем записи из таблицы users с фильтром IN с плейсхолдерами: ";
//echo "<br/>";
//$qb = QueryBuilder::preview("users")
//        ->where("id", Operands::IN, "?", array(124 , 345, 464), false);
//echo $qb->getQuery();
//echo "<br/>";
//echo "Плейсхолдеры:";
//print_r($qb->getBindParams());
//echo "<br/>";
//echo "Выберем записи из таблицы users с фильтром IN с разными плейсхолдерами и без: ";
//echo "<br/>";
//$qb = QueryBuilder::preview("users")
//        ->where("id", Operands::IN, array(":user1" , 345, "?"), array(124 , null, 464), false);
//echo $qb->getQuery();
//echo "<br/>";
//echo "Плейсхолдеры:";
//print_r($qb->getBindParams());
//echo "<br/>";
//echo "Выберем записи из таблицы posts с подзапросом: ";
//echo "<br/>";
//$qb = QueryBuilder::preview("posts")
//        ->where("userId", Operands::EQ, 
//                QueryBuilder::sub("users")
//                    ->where("id", Operands::EQ, 124, null, false));
//$qb->select("id");
//echo $qb->getQuery();
//echo "<br/>";
//echo "Выберем записи из таблицы posts с подзапросом: ";
//echo "<br/>";
//$qb = QueryBuilder::preview("posts");
//$qb->select(QueryBuilder::sub("users")
//                    ->select("id")
//                    ->where("id", Operands::EQ, 124, null, false)
//                    ->setOuterAlias("userId"));
//echo $qb->getQuery();
//echo "<br/>";
//echo "Выберем записи из таблицы posts с подзапросом с плейсхолдерами: ";
//echo "<br/>";
//$qb = QueryBuilder::preview("posts")
//        ->where("id", Operands::EQ, ":postId", 777, false);
//$qb->select(QueryBuilder::sub("users")
//                    ->select("id")
//                    ->where("id", Operands::EQ, ":userId", 124, false)
//                    ->setOuterAlias("userId"));
//echo $qb->getQuery();
//echo "<br/>";
//echo "Плейсхолдеры:";
//print_r($qb->getBindParams());
//echo "<br/>";
//echo "Выберем записи из таблицы posts с подзапросом и полями: ";
//echo "<br/>";
//$qb = QueryBuilder::preview("posts");
//$qb->select(["id", "title", 
//        QueryBuilder::sub("users")
//                    ->select("id")
//                    ->where("id", Operands::EQ, 124, null, false)
//                    ->setOuterAlias("userId")]);
//echo $qb->getQuery();
//echo "<br/>";
//echo "Выберем записи из таблицы users с выражением: ";
//echo "<br/>";
//$qb = QueryBuilder::preview("users");
//$qb->select("IF(:param < 2,'yes','no') as response", 1, false);
//echo $qb->getQuery();
//echo "<br/>";
//echo "Плейсхолдеры:";
//print_r($qb->getBindParams());
//echo "<br/>";
//echo "Выберем записи из таблицы users с выражением и полями: ";
//echo "<br/>";
//$qb = QueryBuilder::preview("users");
//$qb->select(["id", "nick", array("IF(:param < 2,'yes','no') as response", 1, false)]);
//echo $qb->getQuery();
//echo "<br/>";
//echo "Плейсхолдеры:";
//print_r($qb->getBindParams());
//echo "<br/>";
//echo "Выберем записи из таблицы posts с объединением: ";
//echo "<br/>";
//$qb = QueryBuilder::preview("posts", "p")
//        ->join("users", "u", "p.userId", Operands::EQ, "u.id")
//        ->on("p.userId", Operands::EQ, 345, null, false);
//$qb->select("p.id, p.title, u.id");
//echo $qb->getQuery();
//echo "<br/>";
//echo "Выберем записи из таблицы posts с объединением c сложным фильтром: ";
//echo "<br/>";
//$qb = QueryBuilder::preview("posts", "p")
//        ->join("users", "u", QueryBuilder::sub()
//            ->on("p.userId", Operands::EQ, "u.id")
//            ->orOn("p.userId", Operands::EQ, 124, null, false))
//        ->on("p.userId", Operands::EQ, 345);
//$qb->select("p.id, p.title, u.id");
//echo $qb->getQuery();
//echo "<br/>";
//echo "Выберем записи из таблицы posts с объединением c сложным фильтром: ";
//echo "<br/>";
//$qb = QueryBuilder::preview("posts", "p")
//        ->join("users", "u")
//        ->on(QueryBuilder::sub()
//            ->on("p.userId", Operands::EQ, "u.id")
//            ->orOn("p.userId", Operands::EQ, 124, null, false))
//        ->on("p.userId", Operands::EQ, 345);
//$qb->select("p.id, p.title, u.id");
//echo $qb->getQuery();
//echo "<br/>";
//echo "Выберем записи из таблицы posts с объединением c сложным фильтром: ";
//echo "<br/>";
//$qb = QueryBuilder::preview("posts", "p")
//        ->join(QueryBuilder::sub("users"), "u")
//        ->on("p.userId", Operands::EQ, 345);
//$qb->select("p.id, p.title, u.id");
//echo $qb->getQuery();
//echo "<br/>";
//echo "Выберем записи из таблицы posts с объединением c сложным фильтром: ";
//echo "<br/>";
//$qb = QueryBuilder::preview(QueryBuilder::sub("posts"), "p")
//        ->join(QueryBuilder::sub("users"), "u")
//            ->on("p.userId", Operands::EQ, 345)
//        ->where("p.userId", Operands::EQ, 345)
//        ->groupBy("p.userId")
//        ->having("p.userId", Operands::EQ, 345)
//        ->orHaving(QueryBuilder::sub()
//                ->having("p.userId", Operands::EQ, "u.id")
//                ->andHaving("p.userId", Operands::EQ, 124, null, false))
//        ->orderBy("p.userId", "ASC")
//        ->limit(10, 30);
//$qb->select("p.id, p.title, u.id");
//echo $qb->getQuery();
//echo "<br/>";
//echo "Выберем count из таблицы posts: ";
//echo "<br/>";
//$qb = QueryBuilder::preview("posts", "p");// аналогично min max avg sum
//$qb->count("p.id");
//echo $qb->getQuery();
//echo "<br/>";
//echo "Добавим запись в таблице posts: ";
//echo "<br/>";
//$pos = 242;
//$qb = QueryBuilder::preview("posts");
//$qb->insert(array(
//            "title" => "заголовок"// по умолчанию будет [":title", "заголовок", true]
//            , "annotation" => ["?", "аннотация", true]
//            , "description" => [":description", "описание", true]
//            , "moderateDate" => ["now()", false]
//            , "position" => ["position + :position", $pos, false]
//        ));
//echo $qb->getQuery();
//$postId = $qb->getInsertId();
//echo "<br/>";
//echo "Плейсхолдеры:";
//print_r($qb->getBindParams());
//echo "<br/>";
//echo "postId = " . $postId;
//echo "<br/>";
//echo "Обновим запись в таблице posts: ";
//echo "<br/>";
//$qb = QueryBuilder::preview("posts")
//        ->where("id", Operands::EQ, $postId);
//$qb->update(array(
//            "title" => "заголовок"// по умолчанию будет [":title", "заголовок", true]
//            , "annotation" => ["?", "аннотация", true]
//            , "description" => [":description", "описание", true]
//            , "moderateDate" => ["now()", false]
//        ));
//echo $qb->getQuery();
//echo "<br/>";
//echo "Плейсхолдеры:";
//print_r($qb->getBindParams());
?>