<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Calmsen\Component\QueryBuilder;
/**
 * Description of Operands
 *
 * @author rl
 */
class Operands {
    const EQ = "%s = %s";
    const NOTEQ = "%s != %s";
    const LT = "%s < %s";
    const GT = "%s > %s";
    const LTEQ = "%s <= %s";
    const GTEQ = "%s >= %s";
    const IN = "%s IN (%s)";
    const NOTIN = "%s NOT IN (%s)";
    const ISNULL = "%s IS NULL %s";
    const ISNOTNULL = "%s IS NOT NULL %s";
    const LIKE = "%s LIKE %%s";
}

?>
