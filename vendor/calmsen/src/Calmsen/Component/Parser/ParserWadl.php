<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Calmsen\Component\Parser;
/**
 * Description of ParserWadl
 *
 * @author rl
 */
class ParserWadl {
    private $wadlsRoot;
    private $classesRoot;
    /**
     *
     * @var ParserXmlSchema 
     */
    private $parserXmlSchema;
    private $wadls = array();
    private $schemas = [];
    
    function __construct($wadlsRoot, $classesRoot, $parserXmlSchema) {
        $this->wadlsRoot = $wadlsRoot;
        $this->classesRoot = $classesRoot;
        $this->parserXmlSchema = $parserXmlSchema;
    }
    
    private function getWadl($wadlFullPath) {
        if ($this->wadls[$wadlFullPath] === null) {
            if (!file_exists($wadlFullPath)) {
                throw new \Exception("Нет файла " . $wadlFullPath);
            }
            $wadl = new \SimpleXMLElement($wadlFullPath, NULL, TRUE);
            
            $this->wadls[$wadlFullPath] = $wadl;
        }
        return $this->wadls[$wadlFullPath];
    }
    
    private function getWadlFullPath($wadl) {
        $wadlFullPath = null;
        if (count($this->wadls) == 0) {
            throw new \Exception("Массив \$this->wadls не должен быть пустым.");
        }
        foreach ($this->wadls as $p => $w) {
            if ($w == $wadl) {
                $wadlFullPath = $p;
                break;
            }
        }
        
        if ($wadlFullPath === null) {
            throw new \Exception("Путь к схеме не найден.");
        }
        return $wadlFullPath;
    }

    private function getAbsWadlBasePath($wadl) {
        $wadlFullPath = $this->getWadlFullPath($wadl);
        
        $parsedUrlData = Utils::parseUrl($wadlFullPath);
        $absWadlBasePath = $parsedUrlData["dirname"];
        return $absWadlBasePath;
    }
    
    private function getInterfaceNameFromWadlFullPath($wadlFullPath) {
        $pathParts = explode("/", $wadlFullPath);
        
        $wadlFileName = $pathParts[count($pathParts) - 1];
        return "I" . str_replace(array("Wadl", ".xml"), array("Controller", ""), $wadlFileName);
    }
    
    private function getClassNameFromWadlFullPath($wadlFullPath) {
        $pathParts = explode("/", $wadlFullPath);
        
        $wadlFileName = $pathParts[count($pathParts) - 1];
        return str_replace(array("Wadl", ".xml"), array("Uri", ""), $wadlFileName);
    }
    /**
     * 
     * @param \SimpleXMLElement $wadl
     * @param type $ns
     * @return type
     * @throws \Exception
     */
    private function getNamespaceByNS($wadl, $ns) {
        $namespaces = $wadl->getDocNamespaces();
        $namespace = $namespaces[$ns];
        if($namespace === null) {
            throw new \Exception("Атрибут xmlns:" . $ns . " не определен в элементе application.");
        }
        return $namespace;
    }
    
    private function isFindedNamespaceInWadl($wadl, $namespace) {
        $namespaces = $wadl->getDocNamespaces();
        $finded = false;
        foreach ($namespaces as $ns => $curNamespace) {
            if ($curNamespace == $namespace) {
                $finded = true;
                break;
            }
        }
        return $finded;
    }
    
    private function parseElementAttr($type, $wadl) {
        $ns = substr($type, 0, strpos($type, ":"));
        if ($ns == null) {
            throw new \Exception("Не указан ns у текста в атрибуте.");
        }
        $type = substr($type, strpos($type, ":") + 1);

        $namespace = $this->getNamespaceByNS($wadl, $ns);
        
        return array(
            "type" => $type
            , "namespace" => $namespace
        );
    }
    
    private function parseTypeAttr($type, $wadl) {
        $ns = substr($type, 0, strpos($type, ":"));
        if ($ns == null) {
            throw new \Exception("Не указан ns у текста в атрибуте.");
        }
        $type = substr($type, strpos($type, ":") + 1);

        $namespace = $this->getNamespaceByNS($wadl, $ns);
        
        return array(
            "type" => $type
            , "namespace" => $namespace
        );
    }
    
    private function parseRepresentationElement($element, $wadl) {
        $mediaTypeAttr = $element["mediaType"];
        if ($mediaTypeAttr === null) {
            throw new \Exception("Элемент representation должен содержать атрибут mediaType.");
        }
        if ($mediaTypeAttr == "application/xml") {
            $elementAttr = $element["element"];
            if ($mediaTypeAttr === null) {
                throw new \Exception("Элемент representation должен содержать атрибут element.");
            }
            $elementAttrData = $this->parseElementAttr($elementAttr, $wadl);
            // Провери есть ли такой элемент в схемах
            $finded = false;
            foreach ($this->schemas as $schema) {
                if ($schema["targetNamespace"] == $elementAttrData['namespace']) {
                    try {
                        $el = $this->parserXmlSchema->getElementByName($elementAttrData["type"], $schema);
                        if ($el->getName() != "complexType") {
                            throw new \Exception("Элемент должен быть типа complexType.");
                        }
                        $finded = true;
                        break;
                    } catch (\Exception $ex) {
                    }
                }
            }
            if (!$finded) {
                throw new \Exception("Элемент с атрибутом name равным " . $elementAttrData["type"] . " не найден.");
            }
            return array(
                "dataType" => $elementAttrData["type"]
            );
        } else {
            throw new \Exception("Атрибут со значением " . $mediaTypeAttr .  " не обрабатывается.");
        }
    }
    
    private function parseRequestElement($requestEl, $wadl) {
        $output = array();
        $els = $requestEl->xpath("wadl:*");
        $first = true;
        foreach ($els as $el) {
            if (!$first) {
                throw new \Exception("Второй элемент " . $el->getName() . " не обрабатывается!");
            }
            if ($el->getName() == "representation") {
                $representationElementData = $this->parseRepresentationElement($el, $wadl);                
                $output["requestDataType"] = $representationElementData["dataType"];
            } else {
                throw new \Exception("Элемент " . $el->getName() . " не обрабатывается!");
            }
            $first = false;
        }
        return $output;
    }
    
    private function parseResponseElement($responseEl, $wadl) {
        $output = array();
        $statusAttr = $responseEl["status"];
        if ($statusAttr === null) {
            throw new \Exception("Элемент response должен содержать атрибут status.");
        }
        if ($statusAttr == "200") {
            $output["status"] = $status;
        } else {
            throw new \Exception("Атрибут status равный " . $statusAttr . " не обрабатывается.");
        }
        $els = $responseEl->xpath("wadl:*");
        $first = true;
        foreach ($els as $el) {
            if (!$first) {
                throw new \Exception("Второй элемент " . $el->getName() . " не обрабатывается!");
            }
            if ($el->getName() == "representation") {
                $representationElementData = $this->parseRepresentationElement($el, $wadl);                
                $output["responseDataType"] = $representationElementData["dataType"];
                break;
            } else {
                throw new \Exception("Элемент " . $el->getName() . " не обрабатывается.");
            }
            $first = false;
        }
        return $output;
    }
    
    private function parseMethodElement($methodEl, $wadl) {
        $output = array();
        $nameAttr = $methodEl["name"];
        if ($nameAttr === null) {
            throw new \Exception("Элемент method должен содержать атрибут name.");
        }
        $output["type"] = $nameAttr . "";
        $idAttr = $methodEl["id"];
        if ($idAttr === null) {
            throw new \Exception("Элемент method должен содержать атрибут id.");
        }
        $output["name"] = $idAttr . "";
        $els = $methodEl->xpath("wadl:*");
        foreach($els as $el) {
            if ($el->getName() == "request") {
                $requestElementData = $this->parseRequestElement($el, $wadl);
                $output["requestDataType"] = $requestElementData["requestDataType"];
            } else if ($el->getName() == "response") {
                $responseElementData = $this->parseResponseElement($el, $wadl);
                $output["responseDataType"] = $responseElementData["responseDataType"];
            } else {
                throw new \Exception("Элемент " . $el->getName() . " не обрабатывается в методе " . $output["name"] . ".");
            }
        }
        return $output;
    }
    
    private function parseParamElement($paramEl, $wadl) {
        $output = array();
        $nameAttr = $paramEl["name"];
        if ($nameAttr === null) {
            throw new \Exception("Элемент param должен содержать атрибут name.");
        }
        $output["name"] = $nameAttr . "";
        $styleAttr = $paramEl["style"];
        if ($styleAttr === null) {
            throw new \Exception("Элемент param с именем " . $output["name"] . " должен содержать атрибут style.");
        }
        $output["style"] = $styleAttr . "";
        if ($output["style"] == "query") {
            throw new \Exception("Определите параметры с атрибутом style равные query внутри representation.");
        }
        $fixedAttr = $paramEl["fixed"];
        if ($fixedAttr !== null) {
            $output["fixed"] = $fixedAttr . "";
        }
        $defaultAttr = $paramEl["default"];
        if ($defaultAttr !== null) {
            $output["default"] = $defaultAttr . "";
        }
        if ($output["fixed"] !== null && $output["default"] !== null) {
            throw new \Exception("Элемент param должен содержать или атрибут fixed, или атрибут default.");
        }
        if (($output["style"] == "header" || $output["style"] == "matrix") && $output["fixed"] === null) {
            throw new \Exception("Параметр с атрибутом style равный header или matrix должен содержать атрибут fixed.");
        }
        $typeAttr = $paramEl["type"];
        if ($typeAttr === null) {
            throw new \Exception("Элемент param должен содержать атрибут type.");
        }
        $typeData = $this->parseTypeAttr($typeAttr, $wadl);// Провери есть ли такой элемент в схемах
        $output["type"] = $typeData["type"];
        $finded = false;
        foreach ($this->schemas as $schema) {
            if ($schema["targetNamespace"] == $typeData['namespace']) {
                try {
                    $el = $this->parserXmlSchema->getElementByName($typeData["type"], $schema);
                    if ($el->getName() != "simpleType") {
                        throw new \Exception("Элемент должен быть типа simpleType.");
                    }
                    $finded = true;
                    break;
                } catch (\Exception $ex) {
                }
            }
        }
        if (!$finded) {
            throw new \Exception("Элемент с атрибутом name равным " . $output["name"] . " не найден.");
        }
        return $output;
        
    }
    
    private function parserResourceElement($resourceEl, $wadl) {
        $output = array();
        $pathAttr = $resourceEl["path"];
        if ($pathAttr === null) {
            throw new \Exception("Элемент resource должен содержать атрибут path.");
        }
        $output["path"] = $pathAttr . "";
        $paramEls = $resourceEl->xpath("wadl:param");
        $paramsData = array();
        foreach ($paramEls as $paramEl) {
            $paramsData[] = $this->parseParamElement($paramEl, $wadl);
        }
        $output["params"] = $paramsData;
        if (preg_match_all("/\{(.*)\}/U", $output["path"], $matches)) {
            foreach ($matches[1] as $templateName) {
                $finded = false;
                foreach ($output["params"] as $param) {
                    if ($param["style"] = "template" && $templateName == $param["name"]) {
                        $finded = true;
                    }
                }
                if (!$finded) {
                    throw new \Exception("Для ресурса с path равным " . $output["path"] . " не определен параметр с атрибутом name равным " . $templateName . " и атрибутом style равным template.");
                }
            }
                
        }
        $resourceElsData = array();
        $resourceEls = $resourceEl->xpath("wadl:resource");
        foreach ($resourceEls as $resourceEl2) {
            $resourceElsData[] = $this->parserResourceElement($resourceEl2, $wadl);
        }
        $output["resources"] = $resourceElsData;
        $methodEls = $resourceEl->xpath("wadl:method");
        $methodsData = array();
        foreach ($methodEls as $methodEl) {
            $methodsData[] = $this->parseMethodElement($methodEl, $wadl);
        }
        $output["methods"] = $methodsData;
        return $output;
    }
    
    private function createClassWithConstsFromWadl($className, $resourceElsData = array()) {
        
        $classWrap = "<?php
                    \n/**
                    \n * Данный класс содержит константы с URI
                    \n *
                    \n * @author Ruslan Rakhmankulov
                    \n */
                    \nclass %s {
                    %s
                    \n}
                    \n?>";
        $classProperties = "";
        $property = "\n\tconst %s = \"/%s\";";
        $resourceElsDataUniq = [];
        foreach ($resourceElsData as $resourceElData) {
            $finded = false;
            foreach ($resourceElsDataUniq as $resourceElDataUniq) {
                if ($resourceElData["fullPath"] == $resourceElDataUniq["fullPath"]) {
                    $finded = true;
                }
            }
            if (!$finded) {
                $resourceElsDataUniq[] = $resourceElData;
            }
        }
        $classProperties = implode("", array_map(function($resourceElsData)use($property) {
            $propertyName = preg_replace("/nodes\/main\//", "", $resourceElsData["fullPath"]);
            $propertyName = preg_replace("/\/\{.*\}/", "", $propertyName);
            $propertyName = preg_replace("/^\/|\/$/", "", $propertyName);
            $propertyName = preg_replace("/\//", "_", $propertyName);
            $propertyName = strtoupper($propertyName);
            $propertyValue = preg_replace("/nodes\/main\//", "", $resourceElsData["fullPath"]);
            $propertyValue = preg_replace("/^\/|\/$/", "", $propertyValue);
                            return sprintf($property, $propertyName, $propertyValue);
                        }, $resourceElsDataUniq));
        $class = preg_replace("/\\r\\n/", "", sprintf($classWrap, $className, $classProperties));
        $classFullPath = $this->classesRoot . "/uris/" . $className . ".php";
        file_put_contents($classFullPath, $class);
    }
    
    private function createInterfaceFromWadl($interfaceName, $resourceElsData = array()) {
        
        $interfaceWrap = "<?php
                    \n/**
                    \n * Данный интерфейс описывает методы которые должны быть реадизованы в контроллере
                    \n *
                    \n * @author Ruslan Rakhmankulov
                    \n */
                    \ninterface %s {
                    %s
                    \n}
                    \n?>";
        $interfaceMethods = array();
        $methodWrap = "
            \n\n\t/**
            \n\t * @resource(path = \"%s\", method = \"%s\", headers = {%s} matrixs = {%s});
            \n\t */
            \n\tpublic function %s(%s);";
        foreach ($resourceElsData as $resourceElData) {
            $methodHeaders = [];
            $methodMatrixs = [];
            foreach($resourceElData["params"] as $param) {
                if ($param["style"] == "header") {
                    $methodHeaders[] = $param["name"] . " " . $param["fixed"];
                } else if ($param["style"] == "matrix") {
                    $methodMatrixs[] = intval($param["numSlug"]) . ";" . $param["name"] . "=" . str_replace(" ", "+", $param["fixed"]);
                }
            }
            $methodHeadersStr = "";
            if (count($methodHeaders) > 0) {
                $methodHeadersStr = "\"" . implode("\", \"", $methodHeaders) . "\"";
            }
            $methodMatrixsStr = "";
            if (count($methodMatrixs) > 0) {
                $methodMatrixsStr = "\"" . implode("\", \"", $methodMatrixs) . "\"";
            }
            foreach ($resourceElData["methods"] as $method) {
                $methodParams = "";
                if ($method["requestDataType"] != "") {
                    if ($methodParams != "") {
                        $methodParams .= ", ";
                    }
                    $methodParams .= $method["requestDataType"] . " \$input";
                }
                if ($method["responseDataType"] != "") {
                    if ($methodParams != "") {
                        $methodParams .= ", ";
                    }
                    $methodParams .= $method["responseDataType"] . " \$output";
                }
                $interfaceMethods[] = sprintf($methodWrap, $resourceElData["fullPath"], $method["type"], $methodHeadersStr, $methodMatrixsStr,  $method["name"], $methodParams);    
            }            
        }
        $interface = preg_replace("/\\r\\n/", "", sprintf($interfaceWrap, $interfaceName, implode("", $interfaceMethods)));
        $interfaceFullPath = $this->classesRoot . "/icontrollers/" . $interfaceName . ".php";
        file_put_contents($interfaceFullPath, $interface);
    }
    
    private function setFullPathForResources(&$resourceElsData, $path) {
        foreach ($resourceElsData as &$resourceElData) {
            if ($resourceElData["fullPath"] === null) {
                $resourceElData["fullPath"] = "";
            }
            $resourceElData["fullPath"] .= $path . "/" . $resourceElData["path"];
            if (count($resourceElData["resources"]) > 0) {
                $this->setFullPathForResources($resourceElData["resources"], $resourceElData["fullPath"]);
            }
        }
        unset($resourceElData);
    }
    
    private function setNumSlugForParams(&$resourceElsData) {
        foreach ($resourceElsData as &$resourceElData) {
            if ($resourceElData["fullPath"] === null) {
                throw new \Exception("Произошла ошибка парсера.");
            }
            $numSlug = count(explode("/", substr($resourceElData["fullPath"], 1))) - 1;
            foreach ($resourceElData["params"] as &$param) {
                $param["numSlug"] = $numSlug;
            }
            unset($param);
            if (count($resourceElData["resources"]) > 0) {
                $this->setNumSlugForParams($resourceElData["resources"]);
            }
        }
        unset($resourceElData);
    }
    
    private function setParamsFromParentResources(&$resourceElsData, $params = []) {
        foreach ($resourceElsData as &$resourceElData) {
            foreach ($params as $param) {
                $resourceElData["params"][] = $param;
            }
            if (count($resourceElData["resources"]) > 0) {
                $this->setParamsFromParentResources($resourceElData["resources"], $resourceElData["params"]);
            }
        }
        unset($resourceElData);
        
    }
    
    private function resourceElsDataToList(&$resourceElsData, &$resourceElsDataList) {
        foreach ($resourceElsData as &$resourceElData) {
            if (count($resourceElData["methods"]) > 0) {
                $resourceElsDataList[] = &$resourceElData;
            }
            if (count($resourceElData["resources"]) > 0) {
                $this->resourceElsDataToList($resourceElData["resources"], $resourceElsDataList);
            }
            unset($resourceElData["resources"]);
        }
    }
    public function transformWadlToClass($wadlFullPath) {
        $wadl = $this->getWadl($wadlFullPath);
        $absWadlBasePath = $this->getAbsWadlBasePath($wadl);
        $interfaceFileName = $this->getInterfaceNameFromWadlFullPath($wadlFullPath);
        $classFileName = $this->getClassNameFromWadlFullPath($wadlFullPath);
        // Распарсим include элементы
        $includeEls = $wadl->xpath("wadl:grammars/wadl:include");
        if (count($includeEls) > 0) {
            foreach ($includeEls as $includeEl) {
                $hrefAttr = $includeEl["href"];
                if ($hrefAttr === null) {
                    throw new \Exception("Элемент include должен содержать атрибут href.");
                }
                $relSchemaPath = $hrefAttr[0];
                $schemaFullPath = Utils::relativePathToAbsolute($relSchemaPath, $absWadlBasePath);
                $schema = $this->parserXmlSchema->getSchema($schemaFullPath);
                $targetNamespace = $schema["targetNamespace"] ."";
                if (!$this->isFindedNamespaceInWadl($wadl, $targetNamespace)) {
                    throw new \Exception("Не найден namespace " . $targetNamespace . " в документе wadl.");
                }
                $this->schemas[$schemaFullPath] = $schema;
            }   
        }
        $resourcesEl = $wadl->xpath("wadl:resources")[0];
        if ($resourcesEl === null) {
            throw new \Exception("Определите элемент resources в application.");
        }
        $baseAttr = $resourcesEl["base"];
        if ($baseAttr === null) {
            throw new \Exception("Определите атрибут base у элемента resources.");
        }
        $baseAttr = $baseAttr . "";
        // Распарсим resource элементы
        $resourceElsData = [];
        $resourceEls = $wadl->xpath("wadl:resources/wadl:resource");
        foreach ($resourceEls as $resourceEl) {
            $resourceElsData[] = $this->parserResourceElement($resourceEl, $wadl);
        }
        $this->setFullPathForResources($resourceElsData, preg_replace("/\/$/", "", $baseAttr));
        $this->setNumSlugForParams($resourceElsData);
        $this->setParamsFromParentResources($resourceElsData);
        // преобразуем дерево ресурсов в список
        $resourceElsDataList = [];
        $this->resourceElsDataToList($resourceElsData, $resourceElsDataList);
        // Создадим интерфейс
        $this->createInterfaceFromWadl($interfaceFileName, $resourceElsDataList);
        // Создадим класс с константами
        $this->createClassWithConstsFromWadl($classFileName, $resourceElsDataList);
    }
}

?>
