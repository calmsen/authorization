<?php
namespace Calmsen\Component\Parser;
use Calmsen\Component\Utils\Utils;
/**
 * Description of ValidatorByXmlSchema
 *
 * @author Ruslan Rakhmankulov
 */
class ParserXmlSchema {

    private $httpSchemasRoot;
    private $schemasRoot;
    private $classesRoot;
    private $schemas = array(); // массив документов, ключ - это абсолютный путь к документу, значение - это документ. Массив используется для кэша. 
    private $simpleTypeElementsData = array(); // массив данных по элементам simpleType, ключ - это абсолютный путь к элементу, значение - это данные как map. Массив используется для кэша.
    private $complexTypeElementsData = array(); // массив данных по элементам complexType, ключ - это абсолютный путь к элементу, значение - это данные как map. Массив используется для кэша.
    private $elementsData = array(); // массив данных по элементам element, ключ - это абсолютный путь к элементу, значение - это данные как map. Массив используется для кэша.
    private $schemasData = array();// массив данных по элементам schema, ключ - это абсолютный путь к элементу, значение - это данные как map. Массив используется для кэша.
    private $isExistedEntity = false;
    private $entityElementPath;
    private $isExistedRelationship = false;
    private $relationshipElementPath;
    private $entityTable;
    private $parameters;

    function __construct($httpSchemasRoot, $schemasRoot, $classesRoot) {
        $this->httpSchemasRoot = $httpSchemasRoot;
        $this->schemasRoot = $schemasRoot;
        $this->classesRoot = $classesRoot;
        $this->parseXsdTypes();
    }
    
    private function getParameterResult($name) {
        $result = $this->parameters[$name];
        $this->parameters[$name] = null;
        return $result;
    }
    
    private function setParameterResult($name, $value) {
        $this->parameters[$name] = $value;
    }
    
    private function parserSchema($schema, $schemaFullPath) {
        // Расмотрит атрибуты которые не парсятся
        $attributeFormDefault = $schema["attributeFormDefault"];
        if ($attributeFormDefault !== null) {
            throw new \Exception("Данный фреймворк не парсит атрибут attributeFormDefault у элемента schema.");
        }
        $blockDefault = $schema["blockDefault"];
        if ($blockDefault !== null) {
            throw new \Exception("Данный фреймворк не парсит атрибут blockDefault у элемента schema.");
        }
        $finalDefault = $schema["finalDefault"];
        if ($finalDefault !== null) {
            throw new \Exception("Данный фреймворк не парсит атрибут finalDefault у элемента schema.");
        }
        $id = $schema["id"];
        if ($id !== null) {
            throw new \Exception("Данный фреймворк не парсит атрибут id у элемента schema.");
        }
        // Расмотрим элементы которые не парсятся
        $redefine = $schema->xpath("xs:redefine")[0];
        if ($redefine !== null) {
            throw new \Exception("Данный фреймворк не парсит элемент redefine у элемента schema.");
        }
        $group = $schema->xpath("xs:group")[0];
        if ($group !== null) {
            throw new \Exception("Данный фреймворк не парсит элемент group у элемента schema.");
        }
        $attributeGroup = $schema->xpath("xs:attributeGroup")[0];
        if ($attributeGroup !== null) {
            throw new \Exception("Данный фреймворк не парсит элемент attributeGroup у элемента schema.");
        }
        $attribute = $schema->xpath("xs:attribute")[0];
        if ($attribute !== null) {
            throw new \Exception("Данный фреймворк не парсит элемент attribute у элемента schema.");
        }
        $notation = $schema->xpath("xs:notation")[0];
        if ($notation !== null) {
            throw new \Exception("Данный фреймворк не парсит элемент notation у элемента schema.");
        }
        // Расмотрим другие атрибуты 
        $elementFormDefault = $schema["elementFormDefault"];
        if ($elementFormDefault === null) {
            throw new \Exception("Не указан атрибут elementFormDefault у элемента schema " . $schemaFullPath . ".");
        }
        $elementFormDefault .= "";
        if ($elementFormDefault != "qualified") {
            throw new \Exception("Данный фреймворк не позволяет атрибуту elementFormDefault указывать другое значение, кроме как не qualified в элементе schema.");
        }
        $targetNamespace = $schema["targetNamespace"];
        if ($targetNamespace === null) {
            throw new \Exception("Не указан атрибут targetNamespace у элемента schema " . $schemaFullPath . ".");
        }
        $targetNamespace .= "";
        if ($namespaceInp !== null && $namespaceInp != $targetNamespace) {
            throw new \Exception("Пространство " . $namespaceInp . " не совпадает с атрибутом targetNamespace у элемента schema " . $schemaFullPath . ".");
        }
        if (strpos($schemaFullPath, "/types") !== false) {
            $targetPathInfo = str_replace($this->httpSchemasRoot, "", $targetNamespace);
            $pathInfo = str_replace($this->schemasRoot, "", $schemaFullPath);
            $pathInfo = str_replace(".xsd", "", $pathInfo);
            if ($targetPathInfo != $pathInfo) {
                throw new \Exception("Если схема относится к описанию типов, то полный путь к схеме должен соотвествовать полному пути к targetNamespace.Исходная схема " . $schemaFullPath . ".");
            }
        } else {
            $targetPathInfo = str_replace($this->httpSchemasRoot, "", $targetNamespace);
            if ($targetPathInfo != "/types/BaseType") {
                throw new \Exception("Если схема не относится к описанию типов, то targetNamespace должен быть равен " . $this->httpSchemasRoot . "/types/BaseType" .  ". Исходная схема " . $schemaFullPath . ".");
            }
        }
        $this->schemas[$schemaFullPath] = $schema;
        $absSchemaBasePath = $this->getAbsSchemaBasePath($schema);

        $elements = $schema->xpath("xs:*");

        $schemaData = array(
            "schemaFullPath" => $schemaFullPath
            , "targetNamespace" => $targetNamespace
            , "elements" => array()
        );
        
        $this->schemasData[$schemaFullPath] = &$schemaData;
        
        foreach ($elements as $element) {
            // Распарсим простые элементы
            if ($element->getName() == "simpleType") {
                $simpleTypeElementData = $this->parseSimpleType($element, $schema);
                $schemaData["elements"][] = array("nodeName" => "simpleType", "elementPath" => $simpleTypeElementData["elementPath"]);
            }
            // Распарсим сложные элементы
            else if ($element->getName() == "complexType") {
                $complexTypeElementData = $this->parseComplexType($element, $schema);
                $schemaData["elements"][] = array("nodeName" => "complexType", "elementPath" => $complexTypeElementData["elementPath"]);
            }
            // Распарсим элементы
            else if ($element->getName() == "element") {
                $elementData = $this->parseElement($element, $schema);
                $schemaData["elements"][] = array("nodeName" => "element", "elementPath" => $elementData["elementPath"]);
            }
            // Возмем схемы из элементво include
            else if ($element->getName() == "include") {
                // Расмотрит атрибуты которые не парсятся
                $id = $element["id"];
                if ($id !== null) {
                    throw new \Exception("Данный фреймворк не парсит атрибут id у элемента include.");
                }
                $this->getSchema(Utils::relativePathToAbsolute($element["schemaLocation"], $absSchemaBasePath), $schema["targetNamespace"]);
            }
            // Возмем схемы из элементов import
            else if ($element->getName() == "import") {
                // Расмотрит атрибуты которые не парсятся
                $id = $element["id"];
                if ($id !== null) {
                    throw new \Exception("Данный фреймворк не парсит атрибут id у элемента import.");
                }
                $this->getSchema(Utils::relativePathToAbsolute($element["schemaLocation"], $absSchemaBasePath), $element["namespace"]);
            } else {
                throw new \Exception("Данный фреймворк не парсит элемент " + $element->getName() + " у элемента schema.");
            }
        }
    }

    /**
     * Получение схемы
     * @param type $schemaFullPath
     * @param type $namespaceInp
     * @return \SimpleXMLElement
     * @throws \Exception
     */
    public function getSchema($schemaFullPath, $namespaceInp = null) {
        if ($this->schemas[$schemaFullPath] === null) {
            if (!file_exists($schemaFullPath)) {
                throw new \Exception("Нет файла " . $schemaFullPath);
            }
            $schema = new \SimpleXMLElement($schemaFullPath, NULL, TRUE);
            $this->parserSchema($schema, $schemaFullPath);
        }
        return $this->schemas[$schemaFullPath];
    }

    /**
     * Получение схем по namespace
     * @param string $namespace
     * @return \SimpleXMLElement[]
     * @throws \Exception
     */
    private function getSchemasByNamespace($namespace) {
        $schemas = array();
        foreach ($this->schemas as $s) {
            $targetNamespace = $s["targetNamespace"] . "";
            if ($targetNamespace == $namespace) {
                $schemas[] = $s;
            }
        }
        if (count($schemas) == 0) {
            throw new \Exception("Схемы не найдены по namespace равному " . $namespace . ".");
        }
        return $schemas;
    }

    private function getSchemaFullPath($schema) {
        $schemaFullPath = null;
        if (count($this->schemas) == 0) {
            throw new \Exception("Массив \$this->schemas не должен быть пустым.");
        }
        foreach ($this->schemas as $p => $s) {
            if ($s == $schema) {
                $schemaFullPath = $p;
                break;
            }
        }

        if ($schemaFullPath === null) {
            throw new \Exception("Путь к схеме не найден.");
        }
        return $schemaFullPath;
    }

    private function getAbsSchemaBasePath($schema) {
        $schemaFullPath = $this->getSchemaFullPath($schema);

        $parsedUrlData = Utils::parseUri($schemaFullPath);
        $absSchemaBasePath = $parsedUrlData["dirname"];
        return $absSchemaBasePath;
    }

    private function getClassNameFromSchemaFullPath($schemaFullPath) {
        if ($schemaFullPath == "") {
            return "XsdTypeValidator";
        }
        $pathParts = explode("/", $schemaFullPath);

        $schemaFileName = $pathParts[count($pathParts) - 1];
        return str_replace(".xsd", "", $schemaFileName) . "Validator";
    }

    /**
     * Получим импортированную схему по namespace из нашей текущей схемы
     * @param string $namespace 
     * @param \SimpleXMLElement $schema текущая схема
     * @return \SimpleXMLElement
     * @throws \Exception
     */
    private function getImportedSchemaByNamespace($namespace, $schema) {
        $importEl = $schema->xpath("xs:import[@namespace='" . $namespace . "']")[0];
        if ($importEl === null) {
            throw new \Exception("Элемент import c атрибутом namespace равным " . $namespace . " не найден в " . $this->getSchemaFullPath($schema) . ".");
        }
        $importedSchema = $this->getSchema(Utils::relativePathToAbsolute($importEl["schemaLocation"], $this->getAbsSchemaBasePath($schema)), $namespace);
        return $importedSchema;
    }

    /**
     * Получим наследуемые данные простого элемента
     * @param \SimpleXMLElement $schema текущая схема
     * @param string $namespace
     * @param string $type название элемента
     * @return array
     * @throws \Exception
     */
    private function getDeliveredSimpleTypeData($type, $schema, $namespace) {
        $data = null;
        $targetNamespace = $schema["targetNamespace"];
        if ($targetNamespace == $namespace) {
            $data = $this->parseSimpleType($type, $schema);
        } else {
            $importEl = $schema->xpath("xs:import[@namespace='" . $namespace . "']")[0];
            if ($importEl === null) {
                throw new \Exception("Элемент import c атрибутом namespace равным " . $namespace . " не найден.");
            }
            $importedSchema = $this->getImportedSchemaByNamespace($namespace, $schema);
            $data = $this->parseSimpleType($importedSchema, $type);
        }
        if ($data === null) {
            throw new \Exception("Данные по элементу simpleType c атрибутом name равным " . $type . " не найдены.");
        }
        return $data;
    }

    /**
     * Получим пространство по префиксу пространства
     * @param \SimpleXMLElement $schema
     * @param string $ns префикс пространства
     * @return string[]
     * @throws \Exception
     */
    private function getNamespaceByNS($schema, $ns) {
        $namespaces = $schema->getDocNamespaces();
        $namespace = $namespaces[$ns];
        if ($namespace === null) {
            throw new \Exception("Атрибут xmlns:" . $ns . " не определен в элементе schema.");
        }
        return $namespace;
    }
    
    private function getNsByNamespace($schema, $namespace) {
        $namespaces = $schema->getDocNamespaces();
        $ns = null;
        foreach ($namespaces as $curNs => $curNamespace) {
            if ($namespace == $curNamespace) {
                $ns = $curNs;
                break;
            }
        }
        return $ns;
    }
    private function parseBaseAttr($base, $schema) {
        $ns = substr($base, 0, strpos($base, ":"));
        if ($ns == "") {
            throw new \Exception("Не указан ns у текста в атрибуте base: $base.");
        }
        $type = substr($base, strpos($base, ":") + 1);

        $namespace = $this->getNamespaceByNS($schema, $ns);
        
        $anyTypeElementSchemaFullPath = null;
        $anyTypeElementPath = null;
        if ($namespace == "http://www.w3.org/2001/XMLSchema") {
            // Рассмотрим элементы которые не парсятся.
            if ($type == "anyURI" || $type == "base64Binary") {
                throw new \Exception("Данный фреймворк не парсит элементы с типом " . $type . ".");
            }
            //
            $anyTypeElementSchemaFullPath = "";
            $anyTypeElementPath = "/simpleType[@name=" . $type . "]";
        } else {
            if ($type[0] != ucfirst($type[0])) {
                throw new \Exception("Атрибут type должен быть указан с большой буквы: $type.");
            }
            $anyTypeElement = $this->getElementByName($type, $schema, $namespace);
            if ($anyTypeElement->getName() != "complexType" && $anyTypeElement->getName() != "simpleType") {
                throw new \Exception("Элемент должен быть типа complexType или simpleType.");
            }
            // если элемент наследуется от сложного типа, то пространства имен должны быть разными
            if ($anyTypeElement->getName() == "complexType" && $schema["targetNamespace"] == $namespace) {
                throw new \Exception("Данный фреймворк позволяет наследоваться от сложного типа, только если его пространство имен отлично от родителя.");
            }
            // Узнаем схему элемента исходя из буфера
            $anyTypeElementSchema = $this->getParameterResult("schema");
            $anyTypeElementSchemaFullPath = $this->getSchemaFullPath($anyTypeElementSchema);
            $anyTypeElementPath = $anyTypeElementSchemaFullPath . "/" . $anyTypeElement->getName() . "[@name=" . $type . "]";
        }
        return array(
            "type" => $type
            , "namespace" => $namespace
            , "schemaFullPath" => $anyTypeElementSchemaFullPath
            , "elementPath" => $anyTypeElementPath
        );
    }

    public function getElementByName($name, $schema = null, $namespace = null, $currentSchema = null) {
        if ($schema === null && $namespace === null) {
            throw new \Exception("Не правильно переданы параметры в метод getElementByName.");
        }
        // Если указан namespace и он не равен targetNamespace текущей схемы, то возмем схему указанной в элементе import
        if ($namespace !== null && $namespace != $schema["targetNamespace"]) {
            if ($schema !== null) {
                $importedSchema = $this->getImportedSchemaByNamespace($namespace, $schema);
                return $this->getElementByName($name, $importedSchema, $namespace, $schema);
            }
            // Иначе найдем первый элемент из любой схемы! Осторожно использовать этот случай 
            // так как найденный элемент не всегда будет искомым. Положительный эффект - 
            // можно проверить если ли вообще такой элемент по такому namespace
            else {
                $schemas = $this->getSchemasByNamespace($namespace);
                $finded = false;
                foreach ($schemas as $curSchema) {
                    try {
                        $curElement = $this->getElementByName($name, $schemas[$i]);
                        $finded = true;
                        break;
                    } catch (\Exception $ex) {
                    }
                }
                if (!$finded) {
                    if ($currentSchema === null) 
                        $currentSchema = $schema;
                    throw new \Exception("Элемент с атрибутом name равным " . $name . " не найден в " . $this->getSchemaFullPath($schema) . ". Исходная схема: " . $this->getSchemaFullPath($currentSchema) . ".");
                }
            }
        }
        $element = $schema->xpath("xs:*[@name='" . $name . "']")[0];
        if ($element === null) {
            for ($i = 0; $i < 10; $i++) {
                $includeEls = $schema->xpath("xs:include");
                if (count($includeEls) > 0) {
                    foreach ($includeEls as $includeEl) {
                        $schema = $this->getSchema(Utils::relativePathToAbsolute($includeEl["schemaLocation"], $this->getAbsSchemaBasePath($schema)), $schema["targetNamespace"] . "");
                        $element = $schema->xpath("xs:*[@name='" . $name . "']")[0];
                        if ($element !== null) {
                            break 2;
                        }
                    }
                }
            }
                
        }
        if ($element === null) {
                    if ($currentSchema === null) 
                        $currentSchema = $schema;
                    throw new \Exception("Элемент с атрибутом name равным " . $name . " не найден в " . $this->getSchemaFullPath($schema) . ". Исходная схема: " . $this->getSchemaFullPath($currentSchema) . ".");
        }
        $this->setParameterResult("schema", $schema);
        return $element;
    }

    private function parseSimpleList($listElement, $schema) {
        // Расмотрит атрибуты которые не парсятся
        $id = $listElement["id"];
        if ($id !== null) {
            throw new \Exception("Данный фреймворк не парсит атрибут id у элемента list.");
        }
        // Расмотрим элементы которые не парсятся
        $st = $listElement->xpath("xs:simpleType")[0];
        if ($st !== null) {
            throw new \Exception("Данный фреймворк не парсит элемент simpleType у элемента list.");
        }
        // Рассмотрим другие атрибуты и элементы
        $output = array();
        $itemType = $listElement["itemType"];
        if ($itemType === null) {
            throw new \Exception("Ожидался атрибут itemType у элемента list.");
        }
        $ns = substr($itemType, 0, strpos($itemType, ":"));
        if ($ns == "") {
            throw new \Exception("Не указан ns у текста в атрибуте itemType: $itemType.");
        }
        $type = substr($itemType, strpos($itemType, ":") + 1);
        $namespace = $this->getNamespaceByNS($schema, $ns);

        $dataType = null;
        $simpleTypeElementPath = null;
        if ($namespace == "http://www.w3.org/2001/XMLSchema") {
            $this->setParameterResult("dataType", $type);
            $simpleTypeElementPath = "/simpleType[@name=" . $type .  "]";
        } else {
            $data = $this->getDeliveredSimpleTypeData($type, $schema, $namespace);
            $this->setParameterResult("dataType", $data["dataType"]);
            $simpleTypeElementPath = $data["elementPath"];
        }

        $output["itemType"] = array("type" => $type, "namespace" => $namespace, "elementPath" => $simpleTypeElementPath);
        
        return $output;
    }
    
    private function dataTypesIsTime($dataTypes) {
        $isTime = true;
        foreach ($dataTypes as $dataType) {
            if ($dataType == "time") {
                continue;
            }
            $isTime = false;
            break;
        }
        return $isTime;
    }
    
    private function dataTypesIsDate($dataTypes) {
        $isDate = true;
        foreach ($dataTypes as $dataType) {
            if ($dataType == "date") {
                continue;
            }
            $isDate = false;
            break;
        }
        return $isDate;
    }
    
    private function dataTypesIsDateTime($dataTypes) {
        $isDateTime = true;
        foreach ($dataTypes as $dataType) {
            if ($dataType == "datetime" || $dataType == "date" || $dataType == "time") {
                continue;
            }
            $isDateTime = false;
            break;
        }
        return $isDateTime;
    }
    
    private function dataTypesIsString($dataTypes) {
        $isString = false;
        foreach ($dataTypes as $dataType) {
            if ($dataType == "string" || $dataType == "datetime" || $dataType == "date" || $dataType == "time") {
                $isString = true;
                break;
            }
        }
        return $isString;
    }
    
    private function dataTypesIsDecimal($dataTypes) {
        $isDecimal = false;
        foreach ($dataTypes as $dataType) {
            if ($dataType == "decimal") {
                $isDecimal = true;
                break;
            }
        }
        return $isDecimal;
    }
    
    private function dataTypesIsDouble($dataTypes) {
        $isDouble = false;
        foreach ($dataTypes as $dataType) {
            if ($dataType == "double") {
                $isDouble = true;
                break;
            }
        }
        return $isDouble;
    }
    
    private function dataTypesIsFloat($dataTypes) {
        $isFloat = false;
        foreach ($dataTypes as $dataType) {
            if ($dataType == "float") {
                $isFloat = true;
                break;
            }
        }
        return $isFloat;
    }
    
    private function dataTypesIsUnsignedLong($dataTypes) {
        $isUnsignedLong = true;
        foreach ($dataTypes as $dataType) {
            if ($dataType == "unsignedLong") {
                continue;
            }
            $isUnsignedLong = false;
            break;
        }
        return $isUnsignedLong;
    }
    
    private function dataTypesIsLong($dataTypes) {
        $isLong = false;
        foreach ($dataTypes as $dataType) {
            if ($dataType == "long" || $dataType == "unsignedLong") {
                $isLong = true;
                break;
            }
        }
        return $isLong;
    }
    
    private function dataTypesIsUnsignedInt($dataTypes) {
        $isUnsignedInt = true;
        foreach ($dataTypes as $dataType) {
            if ($dataType == "unsignedInt") {
                continue;
            }
            $isUnsignedInt = false;
            break;
        }
        return $isUnsignedInt;
    }
    
    private function dataTypesIsInt($dataTypes) {
        $isInt = false;
        foreach ($dataTypes as $dataType) {
            if ($dataType == "int" || $dataType == "integer" || $dataType == "negativeInteger" || $dataType == "nonNegativeInteger" || $dataType == "positiveInteger" || $dataType == "nonPositiveInteger" || $dataType == "unsignedInt") {
                $isInt = true;
                break;
            }
        }
        return $isInt;
    }
    
    private function dataTypesIsUnsignedShort($dataTypes) {
        $isUnsignedShort = true;
        foreach ($dataTypes as $dataType) {
            if ($dataType == "unsignedShort") {
                continue;
            }
            $isUnsignedShort = false;
            break;
        }
        return $isUnsignedShort;
    }
    
    private function dataTypesIsShort($dataTypes) {
        $isShort = false;
        foreach ($dataTypes as $dataType) {
            if ($dataType == "short" || $dataType == "unsignedShort") {
                $isShort = true;
                break;
            }
        }
        return $isShort;
    }
    
    private function dataTypesIsUnsignedByte($dataTypes) {
        $isUnsignedByte = true;
        foreach ($dataTypes as $dataType) {
            if ($dataType == "unsignedByte") {
                continue;
            }
            $isUnsignedByte = false;
            break;
        }
        return $isUnsignedByte;
    }
    
    private function dataTypesIsByte($dataTypes) {
        $isByte = false;
        foreach ($dataTypes as $dataType) {
            if ($dataType == "byte" || $dataType == "unsignedByte") {
                $isByte = true;
                break;
            }
        }
        return $isByte;
    }
    
    private function dataTypesIsBoolean($dataTypes) {
        $isBoolean = true;
        foreach ($dataTypes as $dataType) {
            if ($dataType == "Boolean") {
                continue;
            }
            $isBoolean = false;
            break;
        }
        return $isBoolean;
    }
    
    private function parseSimpleUnion($unionElement, $schema) {
        // Расмотрит атрибуты которые не парсятся
        $id = $unionElement["id"];
        if ($id !== null) {
            throw new \Exception("Данный фреймворк не парсит атрибут id у элемента list.");
        }
        // Расмотрим элементы которые не парсятся
        $st = $unionElement->xpath("xs:simpleType")[0];
        if ($st !== null) {
            throw new \Exception("Данный фреймворк не парсит элемент simpleType у элемента list.");
        }
        // Рассмотрим другие атрибуты и элементы
        $output = array();
        $memberTypes = $unionElement["memberTypes"];
        if ($memberTypes === null) {
            throw new \Exception("Ожидался атрибут memberTypes у элемента union.");
        }
        $output["memberTypes"] = [];
        $memberTypesArr = explode(" ", $memberTypes);
        if (count($memberTypesArr) < 2) {
            throw new \Exception("Атрибут memberTypes должен содержать не меньше двух типов.");
        }
        $dataTypes = [];
        foreach ($memberTypesArr as $memberType) {
            $ns = substr($memberType, 0, strpos($memberType, ":"));
            if ($ns == "") {
                throw new \Exception("Не указан ns у текста в атрибуте memberType: $memberType.");
            }
            $type = substr($memberType, strpos($memberType, ":") + 1);
            $namespace = $this->getNamespaceByNS($schema, $ns);

            $simpleTypeElementPath = null;
            if ($namespace == "http://www.w3.org/2001/XMLSchema") {
                $dataTypes[] = $type;                
                $simpleTypeElementPath = "/simpleType[@name=" . $type .  "]";
            } else {
                $data = $this->getDeliveredSimpleTypeData($type, $schema, $namespace);
                $dataTypes[] = $data["dataType"];
                $simpleTypeElementPath = $data["elementPath"];
            }

            $output["memberTypes"][] = array("type" => $type, "namespace" => $namespace, "elementPath" => $simpleTypeElementPath);
        }
        $dataType = null;
        if ($this->dataTypesIsTime($dataTypes)) {
            $dataType = "time";
        } else if ($this->dataTypesIsDate($dataTypes)) {
            $dataType = "date";
        } else if ($this->dataTypesIsDateTime($dataTypes)) {
            $dataType = "datetime";
        } else if ($this->dataTypesIsString($dataTypes)) {
            $dataType = "string";
        } else if ($this->dataTypesIsUnsignedLong($dataTypes)) {
            $dataType = "unsignedLong";
        } else if ($this->dataTypesIsLong($dataTypes)) {
            $dataType = "long";
        } else if ($this->dataTypesIsUnsignedInt($dataTypes)) {
            $dataType = "unsignedInt";
        } else if ($this->dataTypesIsInt($dataTypes)) {
            $dataType = "int";
        } else if ($this->dataTypesIsUnsignedShort($dataTypes)) {
            $dataType = "unsignedShort";
        } else if ($this->dataTypesIsShort($dataTypes)) {
            $dataType = "short";
        } else if ($this->dataTypesIsUnsignedByte($dataTypes)) {
            $dataType = "unsignedByte";
        } else if ($this->dataTypesIsByte($dataTypes)) {
            $dataType = "byte";
        } else if ($this->dataTypesIsBoolean($dataTypes)) {
            $dataType = "boolean";
        } else {
            throw new \Exception("Не получилось определить тип.");
        }
        $this->setParameterResult("dataType", $dataType);
        return $output;
    }

    private function parseSimpleRestriction($restrictionElement, $schema, $parentElementPath = null) {
        // Расмотрит атрибуты которые не парсятся
        $id = $restrictionElement["id"];
        if ($id !== null) {
            throw new \Exception("Данный фреймворк не парсит атрибут id у элемента restriction(simpleType).");
        }
        // Расмотрим элементы которые не парсятся
        $attributeGroup = $restrictionElement->xpath("xs:attributeGroup")[0];
        if ($attributeGroup !== null) {
            throw new \Exception("Данный фреймворк не парсит элемент attributeGroup у элемента restriction(simpleType).");
        }
        $attribute = $restrictionElement->xpath("xs:attribute")[0];
        if ($attribute !== null) {
            throw new \Exception("Данный фреймворк не парсит элемент attribute у элемента restriction(simpleType).");
        }
        $anyAttribute = $restrictionElement->xpath("xs:anyAttribute")[0];
        if ($anyAttribute !== null) {
            throw new \Exception("Данный фреймворк не парсит элемент anyAttribute у элемента restriction(simpleType).");
        }
        $whiteSpace = $restrictionElement->xpath("xs:whiteSpace")[0];
        if ($whiteSpace !== null) {
            throw new \Exception("Данный фреймворк не парсит элемент whiteSpace у элемента restriction(simpleType).");
        }
        $simpleType = $restrictionElement->xpath("xs:simpleType")[0];
        if ($simpleType !== null) {
            throw new \Exception("Данный фреймворк не парсит элемент simpleType у элемента restrictionElement.");
        }
        // Рассмотрим другие атрибуты и элементы
        $output = array();
        $restrictionChilds = $restrictionElement->xpath("xs:*");
        $restrictions = array();
        if (count($restrictionChilds) > 0) {
            foreach ($restrictionChilds as $restrictionChild) {
                $restrictionName = $restrictionChild->getName();
                if ($restrictionName == "simpleType") { // распарсим этот элемент позже
                    continue;
                }
                
                if ($restrictionName == "enumeration" && $restrictions[$restrictionName] !== null) {
                    if (strpos($restrictions[$restrictionName], " ") !== false) {
                        throw new \Exception("Данный фреймворк не позволяет создавать enumeration, содержащие пробелы.");
                    }
                    $restrictions[$restrictionName] .= "|";
                }
                $restrictions[$restrictionName] .= $restrictionChild["value"] . "";   
            }
        }
        
        // если данный элемент наследуется от другого элемента,
        // то добавим ограничения последнего элемента к данному.
        $base = $restrictionElement["base"];
        if ($base !== null) {
            $baseData = $this->parseBaseAttr($base, $schema);
            $type = $baseData["type"];
            $namespace = $baseData["namespace"];

            $simpleTypeElementPath = null;
            if ($namespace == "http://www.w3.org/2001/XMLSchema") {
                $this->setParameterResult("dataType", $type);
                $simpleTypeElementPath = "/simpleType[@name=" . $type .  "]";
            } else {
                $data = $this->getDeliveredSimpleTypeData($type, $schema, $namespace);
                $this->setParameterResult("dataType", $data["dataType"]);
                $simpleTypeElementPath = $data["elementPath"];
                if ($data["list"] !== null) {
                    $this->setParameterResult("list", $data["list"]);
                    if ($restrictions["enumeration"] !== null) {
                        throw new \Exception("Данный фреймворк не парсит элемент enumeration у элемента restriction(simpleType) для списков.");
                    }
                    if ($restrictions["pattern"] !== null) {
                        throw new \Exception("Данный фреймворк не парсит элемент pattern у элемента restriction(simpleType) для списков.");
                    }
                    if ($restrictions["whiteSpace"] !== null) {
                        throw new \Exception("Данный фреймворк не парсит элемент whiteSpace у элемента restriction(simpleType) для списков.");
                    }
                }
                if ($data["union"] !== null) {
                    throw new \Exception("Данный фреймворк не позволяет наследоваться от родителя, имеющего элемент union.");
                }
                if ($data["restriction"] !== null) {
                    // нельзя наследовать enumeration, если были определены в родителе
                    $findEnumeration = false;
                    foreach ($restrictions as $restrictionName => $r) {
                        if ($restrictionName == "enumeration") {
                            $findEnumeration = true;
                            break;
                        }
                    }
                    if ($findEnumeration) {
                        foreach ($data["restriction"]["elements"] as $restrictionName => $r) {
                            if ($restrictionName == "enumeration") {
                                throw new \Exception("Данный фреймворк не позволяет наследовать перечисления от родителя, если перечисления определяются в текущем simpleType.");
                            }
                        }
                    }
                    $restrictions = array_merge($data["restriction"]["elements"], $restrictions);
                }  
            }
            $output["base"] = array("type" => $type, "namespace" => $namespace, "elementPath" => $simpleTypeElementPath);
        } else {
            throw new \Exception("Не указан атрибут base у элемента restriction(simpleType).");
        }
        $output["elements"] = $restrictions;
        return $output;
    }

    private function parseSimpleTypeAnnotation($annotationElement, $schema) {
        $output = array();
        $annotationElementChilds = $annotationElement->xpath("xs:*")[0];
        $output["appinfo"] = array(
            "annotations" => array()
        );
        foreach ($annotationElementChilds as $annotationElementChild) {
            if ($annotationElementChild->getName() == "document") {
                $documentEl = $annotationElementChild;
                $output["document"] = $documentEl . "";
            } else if ($annotationElementChild->getName() == "appinfo") {
                $appinfoEl = $annotationElementChild;
                $annotEls = $appinfoEl->xpath("an:*");
                if (count($annotEls) > 0) {
                    foreach ($annotEls as $annotEl) {
                        if ($annotEl->getName() == "error") {
                            $errorMessage = $annotEl["message"] . "";
                            $output["appinfo"]["annotations"]["error"] = array(
                                "message" => $errorMessage
                            );
                        } else {
                            throw new \Exception("Элемент " . $annotEl->getName() . " не обрабатывается!");
                        }
                    }
                }
            } else {
                throw new \Exception("Элемент " . $annotationElementChild->getName() . " не обрабатывается!");
            }
        }

        return $output;
    }

    private function parseSimpleType($simpleTypeElement, $schema) {
        $output = array();

        if (is_string($simpleTypeElement)) {
            $name = $simpleTypeElement;
            $simpleTypeElement = $this->getElementByName($name, $schema);
            // Присвоим новое значение схемы
            $schema = $this->getParameterResult("schema");
            if ($simpleTypeElement->getName() != "simpleType") {
                throw new \Exception("Ожидалось что элемент с атрибутом name равным " . $name . " будет simpleType.");
            }
        }
        // Расмотрит атрибуты которые не парсятся
        $id = $simpleTypeElement["id"];
        if ($id !== null) {
            throw new \Exception("Данный фреймворк не парсит атрибут id у элемента simpleType.");
        }
        $final = $simpleTypeElement["final"];
        if ($final !== null) {
            throw new \Exception("Данный фреймворк не парсит атрибут final у элемента simpleType.");
        }
        // Расмотрим элементы которые не парсятся
        $simpleType = $simpleTypeElement->xpath("xs:simpleType")[0];
        if ($simpleType !== null) {
            throw new \Exception("Данный фреймворк не парсит элемент simpleType у элемента simpleType.");
        }
        $schemaFullPath = $this->getSchemaFullPath($schema);
        $output["schemaFullPath"] = $schemaFullPath;
        $elementPath = $schemaFullPath . "/simpleType";
        // Рассмотрим другие атрибуты и элементы
        $name = $simpleTypeElement["name"];
        if ($name === null) {
            throw new \Exception("Не указан атрибут name у элемента simpleType.");
        }
        $name .= "";
        $output["name"] = $name;
        $elementPath .= "[@name=" . $name . "]";
        if ($this->simpleTypeElementsData[$elementPath] !== null) {
            return $this->simpleTypeElementsData[$elementPath];
        }
        $output["elementPath"] = $elementPath;
        $simpleTypeElementChilds = $simpleTypeElement->xpath("*");
        if (count($simpleTypeElementChilds) == 0) {
            throw new \Exception("Элемент simpleType должен содержать в себе элементы restriction|list|anntation.");
        }
        $output["annotation"] = array();
        foreach ($simpleTypeElementChilds as $simpleTypeElementChild) {
            if ($simpleTypeElementChild->getName() === "annotation") {
                $output["annotation"] = $this->parseSimpleTypeAnnotation($simpleTypeElementChild, $schema);
            } else if ($simpleTypeElementChild->getName() === "restriction") {
                $output["restriction"] = $this->parseSimpleRestriction($simpleTypeElementChild, $schema, $elementPath);
                $output["list"] = $this->getParameterResult("list");
                $output["dataType"] = $this->getParameterResult("dataType");
                break;
            } else if ($simpleTypeElementChild->getName() === "list") {
                $output["list"] = $this->parseSimpleList($simpleTypeElementChild, $schema);
                $output["dataType"] = $this->getParameterResult("dataType");
                break;
            } else if ($simpleTypeElementChild->getName() === "union") {
                $output["union"] = $this->parseSimpleUnion($simpleTypeElementChild, $schema);
                $output["dataType"] = $this->getParameterResult("dataType");
                break;
            } else {
                throw new \Exception("Элемент " . $simpleTypeElement[0]->getName() . " не обрабатывается в текущем контексте");
            }
        }
        // Положим в кэш
        $this->simpleTypeElementsData[$elementPath] = $output;
        return $output;
    }

    private function getBooleanMethodContent($elementData) {
        $type = $elementData["name"];
        $methodContent .= "\n\t\tif (is_bool(\$bool)) {
                \n\t\t\tthrow new \Exception( \"Булево с типом " . $type . " должно содержать булево значение.\" );
                \n\t\t}";
        return $methodContent;
    }

    private function getNumericMethodContent($elementData) {
        $type = $elementData["name"];
        if ($elementData["dataType"] == "decimal") {
            $methodContent .= "\n\t\tif (is_numeric(\$num)) {
                \n\t\t\tthrow new \Exception( \"Число с типом " . $type . " должна содержать число.\" );
                \n\t\t}";
        } else if ($elementData["dataType"] == "float" || $elementData["dataType"] == "double") {
            $methodContent .= "\n\t\tif (is_float(\$num)) {
                \n\t\t\tthrow new \Exception( \"Число с типом " . $type . " должно содержать число.\" );
                \n\t\t}";
        } else {
            $methodContent .= "\n\t\tif (is_int(\$num)) {
                \n\t\t\tthrow new \Exception( \"Число с типом " . $type . " должно содержать целое число.\" );
                \n\t\t}";
        }
        if ($elementData["dataType"] == "unsignedByte" || $elementData["dataType"] == "unsignedShort" || $elementData["dataType"] == "unsignedInt" || $elementData["dataType"] == "unsignedLong") {
            $methodContent .= "\n\t\tif (\$num < 0) {
                \n\t\t\tthrow new \Exception( \"Число c типом " . $type . " не может быть меньше 0.\" ); 
                \n\t\t}";
        }
        if ($elementData["dataType"] == "negativeInteger") {
            $methodContent .= "\n\t\tif (\$num >= 0) {
                \n\t\t\tthrow new \Exception( \"Число c типом " . $type . " не может быть больше либо равно 0.\" ); 
                \n\t\t}";
        }
        if ($elementData["dataType"] == "nonNegativeInteger") {
            $methodContent .= "\n\t\tif (\$num < 0) {
                \n\t\t\tthrow new \Exception( \"Число c типом " . $type . " не может быть меньше 0.\" ); 
                \n\t\t}";
        }
        if ($elementData["dataType"] == "positiveInteger") {
            $methodContent .= "\n\t\tif (\$num <= 0) {
                \n\t\t\tthrow new \Exception( \"Число c типом " . $type . " не может быть меньше либо равно 0.\" ); 
                \n\t\t}";
        }
        if ($elementData["dataType"] == "nonPositiveInteger") {
            $methodContent .= "\n\t\tif (\$num > 0) {
                \n\t\t\tthrow new \Exception( \"Число c типом " . $type . " не может быть больше 0.\" ); 
                \n\t\t}";
        }
        if (count($elementData["restriction"]["elements"]) > 0) {
            foreach ($elementData["restriction"]["elements"] as $restrictionName => $restrictionValue) {
                if ($restrictionName == "totalDigits") {
                    $methodContent .= "\n\t\tif (strlen(\$num) > " . $restrictionValue . ") {
                    \n\t\t\tthrow new \Exception( \"Число c типом " . $type . " не должно состоять больше чем из " . $restrictionValue . " " . Utils::declension($restrictionValue, array("цифры", "цифр")) . ".\" ); 
                    \n\t\t}";
                } else {
                    throw new \Exception("restriction " . $restrictionName . " не обрабатываетя для чисел!");
                }
            }
        }
        return $methodContent;
    }

    private function getStringMethodContent($elementData) {
        $type = $elementData["name"];
        $methodContent = "";
        if (count($elementData["restriction"]["elements"]) > 0) {
            foreach ($elementData["restriction"]["elements"] as $restrictionName => $restrictionValue) {
                if ($restrictionName == "minLength") {
                    $methodContent .= "\n\t\tif (mb_strlen(\$str) < " . $restrictionValue . ") {
                    \n\t\t\tthrow new \Exception( \"Строка с типом " . $type . " не должна состоять меньше чем из " . $restrictionValue . " " . Utils::declension($restrictionValue, array("символа", "символов")) . ".\" ); 
                    \n\t\t}";
                } else if ($restrictionName == "maxLength") {
                    $methodContent .= "\n\t\tif (mb_strlen(\$str) > " . $restrictionValue . ") {
                    \n\t\t\tthrow new \Exception( \"Строка с типом " . $type . " не должна состоять больше чем из " . $restrictionValue . " " . Utils::declension($restrictionValue, array("символа", "символов")) . ".\" ); 
                    \n\t\t}";
                } else if ($restrictionName == "length") {
                    $methodContent .= "\n\t\tif (mb_strlen(\$str) != " . $restrictionValue . ") {
                    \n\t\t\tthrow new \Exception( \"Строка с типом " . $type . " не должна состоять больше или меньше чем из " . $restrictionValue . " " . Utils::declension($restrictionValue, array("символа", "символов")) . ".\" ); 
                    \n\t\t}";
                } else if ($restrictionName == "pattern") {
                    $methodContent .= "\n\t\tif (!preg_match(\"" . $restrictionValue . "\", \$str)) {
                    \n\t\t\tthrow new \Exception( \"Строка с типом " . $type . " должна совпадать по шаблону " . $restrictionValue . ".\" ); 
                    \n\t\t}";
                } else if ($restrictionName == "enumeration") {
                    $methodContent .= "\n\t\tif (!preg_match(\"^(" . $restrictionValue . ")\$\",\$str)) {
                    \n\t\t\tthrow new \Exception( \"Строка с типом " . $type . " должна быть одним из значений: " . str_replace("|", ",", $restrictionValue) . ".\" ); 
                    \n\t\t}";
                } else {
                    throw new \Exception("restriction " . $restrictionName . " не обрабатываетя для строк!");
                }
            }
        }

        return $methodContent;
    }
    
    private function getDateMethodContent($elementData) {
        if ($elementData["type"] === null) {
            // Заменим на свой тип
            $elementData = $this->simpleTypeElementsData[$this->schemasRoot . "/types/BaseType.xsd/simpleType[@name=" . ucfirst($elementData["name"]) . "]"];
        }        
        $type = $elementData["name"];
        $methodContent = "";
        if (count($elementData["restriction"]["elements"]) > 0) {
            foreach ($elementData["restriction"]["elements"] as $restrictionName => $restrictionValue) {
                if ($restrictionName == "pattern") {
                    $format = null;
                    if ($elementData["dataType"] == "date") {
                        $format = "Y-m-d";
                    } else if ($elementData["dataType"] == "time") {
                        $format = "H:i:s";
                    } else {
                        $format = "Y-m-d H:i:s";
                        $restrictionValue = str_replace("T", " ",  $restrictionValue);
                    }
                    if ($format === null) {
                        throw new \Exception("Произошла ошибка парсинга.");
                    }
                    $methodContent .= "\n\t\t\$" . $elementData["dataType"] . "AsString = \$" . $elementData["dataType"] . "->format(\"" . $format . "\");
                    \n\t\tif (!preg_match(\"" . $restrictionValue . "\", \$" . $elementData["dataType"] . "AsString)) {
                    \n\t\t\tthrow new \Exception( \"Date/Time с типом " . $type . " должна совпадать по шаблону " . $restrictionValue . ".\" ); 
                    \n\t\t}";
                } else {
                    throw new \Exception("restriction " . $restrictionName . " не обрабатываетя для Date/Time!");
                }
            }
        }

        return $methodContent;
    }

    private function getItemMethodContent($elementData, &$methodParameter) {
        if ($elementData["dataType"] == "boolean") {
            $methodParameter = "\$bool";
            $methodContent = $this->getBooleanMethodContent($elementData);
        } else if ($elementData["dataType"] == "byte" || $elementData["dataType"] == "short" || $elementData["dataType"] == "int" || $elementData["dataType"] == "integer" || $elementData["dataType"] == "long" || $elementData["dataType"] == "float" || $elementData["dataType"] == "double" || $elementData["dataType"] == "decimal" || $elementData["dataType"] == "unsignedByte" || $elementData["dataType"] == "unsignedShort" || $elementData["dataType"] == "unsignedInt" || $elementData["dataType"] == "unsignedLong" || $elementData["dataType"] == "negativeInteger" || $elementData["dataType"] == "nonNegativeInteger" || $elementData["dataType"] == "positiveInteger" || $elementData["dataType"] == "nonPositiveInteger") {
            $methodParameter = "\$num";
            $methodContent = $this->getNumericMethodContent($elementData);
        } else if ($elementData["dataType"] == "string") {
            $methodParameter = "\$str";
            $methodContent = $this->getStringMethodContent($elementData);
        } else if ($elementData["dataType"] == "date" || $elementData["dataType"] == "time" || $elementData["dataType"] == "dateTime") {
            $methodParameter = "\$" . $elementData["dataType"];
            $methodContent = $this->getDateMethodContent($elementData);
        } else {
            throw new \Exception("Type" . $elementData["dataType"] . " не обрабатываетя!");
        }
        return $methodContent;
    }

    private function getListMethodContent($elementData) {
        $methodContent = "";
        if (count($elementData["restriction"]["elements"]) > 0) {
            foreach ($elementData["restriction"]["elements"] as $restrictionName => $restrictionValue) {
                if ($restrictionName == "minLength") {
                    $methodContent .= "\n\t\tif (count(\$list) < " . $restrictionValue . ") {
                    \n\t\t\tthrow new \Exception( \"Список с типом " . $elementData["name"] . " не должен состоять меньше чем из " . $restrictionValue . " " . Utils::declension($restrictionValue, array("элемента", "элементов")) . ".\" ); 
                    \n\t\t}";
                } else if ($restrictionName == "maxLength") {
                    $methodContent .= "\n\t\tif (count(\$list) > " . $restrictionValue . ") {
                    \n\t\t\tthrow new \Exception( \"Список с типом " . $elementData["name"] . " не должен состоять больше чем из " . $restrictionValue . " " . Utils::declension($restrictionValue, array("элемента", "элементов")) . ".\" ); 
                    \n\t\t}";
                } else if ($restrictionName == "length") {
                    $methodContent .= "\n\t\tif (count(\$list) != " . $restrictionValue . ") {
                    \n\t\t\tthrow new \Exception( \"Список с типом " . $elementData["name"] . " не должен состоять больше или меньше чем из " . $restrictionValue . " " . Utils::declension($restrictionValue, array("элемента", "элементов")) . ".\" ); 
                    \n\t\t}";
                } else {
                    throw new \Exception("restriction " . $restrictionName . " не обрабатываетя для списоков!");
                }
            }
        }
//        $itemMethodParametr = "";
//        $itemMethodContent = $this->getItemMethodContent($this->simpleTypeElementsData[$elementData["list"]["itemType"]["elementPath"]], $itemMethodParametr);
//        $methodContent .= "\n\t\tforeach (\$list as " . $itemMethodParametr . ") {
//                    " . str_replace("\n\t", "\n\t\t", $itemMethodContent) . "
//                    \n\t\t}
//                    ";
        
        $simpleTypeElementData = $this->simpleTypeElementsData[$elementData["list"]["itemType"]["elementPath"]];
        $className = $this->getClassNameFromSchemaFullPath($simpleTypeElementData["schemaFullPath"]);
        $methodContent .= "\n\t\tforeach(\$list as \$item) {
            \n\t\t\t" . $className . "::validate" . ucfirst($simpleTypeElementData["name"]) . "(\$item);
            \n\t\t}";
        return $methodContent;
    }
    private function getUnionMethodContent($elementData) {
        $methodContent = "";
        $types = [];
        foreach ($elementData["union"]["memberTypes"] as $memberType) {
            $simpleTypeElementsData = $this->simpleTypeElementsData[$memberType["elementPath"]];
            $className = $this->getClassNameFromSchemaFullPath($simpleTypeElementsData["schemaFullPath"]);
            $methodContent .= "\n\t\t\$isValidated = true;
                \n\t\ttry {
                \n\t\t\t" . $className . "::validate" . ucfirst($simpleTypeElementsData["name"]) . "(\$item);
                \n\t\t} catch (\Exception \$ex) {
                \n\t\t\t\$isValidated = false;
                \n\t\t}
                \n\t\tif (\$isValidated) {
                \n\t\t\treturn;
                \n\t\t}";
            $types[] = $memberType["type"];
        }
        $methodContent .= "\n\t\tthrow new \Exception(\"Свойство " . $elementData["name"] . " должно соответствовать одному из типу: " . implode("|", $types) . ".\");";
        return $methodContent;
    }
    
    private function parseTypeAttr($type, $schema) {
        $ns = substr($type, 0, strpos($type, ":"));
        if ($ns == "") {
            throw new \Exception("Не указан ns у текста в атрибуте type: $type.");
        }
        $type = substr($type, strpos($type, ":") + 1);
        $namespace = $this->getNamespaceByNS($schema, $ns);
        $anyTypeElementSchemaFullPath = null;
        $anyTypeElementPath = null;
        $isComplexType = false;
        if ($namespace == "http://www.w3.org/2001/XMLSchema") {
            // Рассмотрим элементы которые не парсятся.
            if ($type == "anyURI" || $type == "base64Binary") {
                throw new \Exception("Данный фреймворк не парсит элементы с типом " . $type . ".");
            }
            //
            $anyTypeElementSchemaFullPath = "";
            $anyTypeElementPath = "/simpleType[@name=" . $type . "]";
        } else {
            if ($type[0] != ucfirst($type[0])) {
                throw new \Exception("Атрибут type должен быть указан с большой буквы: $type.");
            }
            // Узнаем схему элемента исходя из буфера
            $anyTypeElement = $this->getElementByName($type, $schema, $namespace);
            if ($anyTypeElement->getName() != "complexType" && $anyTypeElement->getName() != "simpleType") {
                throw new \Exception("Элемент должен быть типа complexType или simpleType.");
            }
            $anyTypeElementSchema = $this->getParameterResult("schema");
            $anyTypeElementSchemaFullPath = $this->getSchemaFullPath($anyTypeElementSchema);
            $anyTypeElementPath = $anyTypeElementSchemaFullPath . "/" . $anyTypeElement->getName() . "[@name=" . $type . "]";
            $isComplexType = ($anyTypeElement->getName() == "complexType" ? true : false );
        }      
        
        return array(
            "type" => $type
            , "namespace" => $namespace
            , "schemaFullPath" => $anyTypeElementSchemaFullPath
            , "elementPath" => $anyTypeElementPath
            , "isComplexType" => $isComplexType
        );
    }
    
    private function parseSubstitutionGroupAttr($substitutionGroup, $schema) {
        if ($substitutionGroup == "") {
            throw new \Exception("Не указан текст в атрибуте substitutionGroup: $substitutionGroup.");
        }
        $ns = substr($substitutionGroup, 0, strpos($substitutionGroup, ":"));
        if ($ns == "") {
            throw new \Exception("Не указан ns у текста в атрибуте substitutionGroup: $substitutionGroup.");
        }
        $substitutionGroup = substr($substitutionGroup, strpos($substitutionGroup, ":") + 1);
        $namespace = $this->getNamespaceByNS($schema, $ns);
        $elementSchemaFullPath = null;
        $elementPath = null;
        if ($namespace == "http://www.w3.org/2001/XMLSchema") {
            throw new \Exception("Атрибут substitutionGroup не может указывать на dataType.");
        } else {
            if ($substitutionGroup[0] != Utils::lowerCaseFirst($substitutionGroup[0])) {
                throw new \Exception("Атрибут substitutionGroup должен быть указан с маленькой буквы: $substitutionGroup.");
            }
            // Узнаем схему элемента исходя из буфера
            $element = $this->getElementByName($substitutionGroup, $schema, $namespace);
            if ($element->getName() != "element") {
                throw new \Exception("Элемент должен быть типа element.");
            }
            $elementSchema = $this->getParameterResult("schema");
            $elementSchemaFullPath = $this->getSchemaFullPath($elementSchema);
            $elementPath = $elementSchemaFullPath . "/" . $element->getName() . "[@name=" . $substitutionGroup . "]";
        }
        return array(
            "substitutionGroup" => $substitutionGroup
            , "namespace" => $namespace
            , "schemaFullPath" => $elementSchemaFullPath
            , "elementPath" => $elementPath
        );
    }
    
    private function parseRefAttr($ref, $schema) {
        $ns = substr($ref, 0, strpos($ref, ":"));
        if ($ns == "") {
            throw new \Exception("Не указан ns у текста в атрибуте ref: $ref.");
        } 
        $ref = substr($ref, strpos($ref, ":") + 1);
        if ($ref[0] != Utils::lowerCaseFirst($ref[0])) {
            throw new \Exception("Атрибут ref должен быть указан с маленькой буквы.");
        }
        $namespace = $this->getNamespaceByNS($schema, $ns);
        if ($namespace == "http://www.w3.org/2001/XMLSchema") {
            throw new \Exception("Атрибут ref не может указывать на dataType.");
        }
        
        return array(
            "ref" => $ref
            , "namespace" => $namespace
        );
    }

    private function parseElementAnnotations($annotationElement, $schema) {
        $output = array();
        $annotationElementChilds = $annotationElement->xpath("xs:*");
        $output["appinfo"] = array(
            "annotations" => array()
        );
        foreach ($annotationElementChilds as $annotationElementChild) {
            if ($annotationElementChild->getName() == "document") {
                $documentEl = $annotationElementChild;
                $output["document"] = $documentEl . "";
            } else if ($annotationElementChild->getName() == "appinfo") {
                $appinfoEl = $annotationElementChild;
                $annotEls = $appinfoEl->xpath("an:*");
                if (count($annotEls) > 0) {
                    foreach ($annotEls as $annotEl) {
                        if ($annotEl->getName() == "error") {
                            $errorMessage = $annotEl["message"] . "";
                            $output["appinfo"]["annotations"]["error"] = array(
                                "message" => $errorMessage
                            );
                        } else if ($annotEl->getName() == "notColumn") {
                            $output["appinfo"]["annotations"]["notColumn"] = true;
                        } else if ($annotEl->getName() == "column") {
                            if ($output["appinfo"]["annotations"]["notColumn"]) {
                                throw new \Exception("Нельзя указывать для одного элемента сразу атрибуты notColumn и column.");
                            }
                            $columnName = $annotEl["name"];
                            if ($columnName !== null) {
                                $columnName .= "";
                            } else {
                                $columnName = $name;
                            }
                            $columnType = $annotEl["type"];
                            if ($columnType !== null) {
                                $columnType .= "";
                            }

                            $output["appinfo"]["annotations"]["column"] = array(
                                "name" => $columnName
                                , "type" => $columnType
                            );
                        } else if ($annotEl->getName() == "notFormField") {
                            $output["appinfo"]["annotations"]["notFormField"] = true;
                        } else if ($annotEl->getName() == "formField") {
                            if ($output["appinfo"]["annotations"]["notFormField"]) {
                                throw new \Exception("Нельзя указывать для одного элемента сразу атрибуты notFormField и formField.");
                            }
                            $formFieldName = $annotEl["name"];
                            if ($formFieldName !== null) {
                                $formFieldName .= "";
                            }
                            $formFieldRequired = $annotEl["required"];
                            if ($formFieldRequired !== null) {
                                $formFieldRequired = boolval($formFieldRequired);
                            }
                            $formFieldMin = $annotEl["min"];
                            if ($formFieldMin !== null) {
                                $formFieldMin = intval($formFieldMin);
                            }
                            $formFieldMax = $annotEl["max"];
                            if ($formFieldMax !== null) {
                                $formFieldMax = intval($formFieldMax);
                            }
                            $formFieldMaxlength = $annotEl["max"];
                            if ($formFieldMaxlength !== null) {
                                $formFieldMaxlength = intval($formFieldMaxlength);
                            }
                            $formFieldPattern = $annotEl["pattern"];
                            if ($formFieldPattern !== null) {
                                $formFieldPattern .= "";
                            }                            
                            $output["appinfo"]["annotations"]["formField"] = array(
                                "name" => $formFieldName
                                , "required" => $formFieldRequired
                                , "min" => $formFieldMin
                                , "max" => $formFieldMax
                                , "maxlength" => $formFieldMaxlength
                                , "pattern" => $formFieldPattern
                            );
                        } else if ($annotEl->getName() == "var") {
                            $varName = $annotEl["name"] . "";
                            $varType = $annotEl["type"] . "";
                            $varDescription = $annotEl["description"] . "";
                            $output["appinfo"]["annotations"]["var"] = array(
                                "name" => $varName
                                , "type" => $varType
                                , "description" => $varDescription
                            );
                        } else {
                            throw new \Exception("Элемент " . $annotEl->getName() . " не обрабатывается!");
                        }
                    }
                }
            } else {
                throw new \Exception("Элемент " . $annotationElementChild->getName() . " не обрабатывается!");
            }
        }

        return $output;
    }
    
    private function parseXpathPart($xpathPart, $schema) {
        $ns = substr($xpathPart, 0, strpos($xpathPart, ":"));
        if ($ns == "") {
            throw new \Exception("Не указан ns у текста в атрибуте xpath: $xpath.");
        }
        $xpathPart = substr($xpathPart, strpos($xpathPart, ":") + 1);

        $namespace = $this->getNamespaceByNS($schema, $ns);
        
        return array(
            "xpathPart" => $xpathPart
            , "namespace" => $namespace
        );
    }
    
    private function parseKeyElement($keyEl, $schema) {
        $key = array(
            "nodeName" => "key"
        );
        $keyNameAttr = $keyEl["name"];
        if ($keyNameAttr === null) {
            throw new \Exception("Элемент selector должен содержать атрибут xpath.");
        }
        $keyNameAttr .= "";
        $key["name"] = $keyNameAttr;

        $selectorEl = $keyEl->xpath("xs:selector")[0];
        if ($selectorEl === null) {
            throw new \Exception("Элемент key должен содержать элемент selector.");
        }
        $selXpathAttr = $selectorEl["xpath"];
        if ($selXpathAttr === null) {
            throw new \Exception("Элемент selector должен содержать атрибут xpath.");
        }
        $selXpathAttr .= "";
        $xpathParts = [];
        foreach (explode("/", $selXpathAttr) as $xpathPart) {
            $xpathParts[] = $this->parseXpathPart($xpathPart, $schema);
        }
        $key["selector"] = array(
            "xpath" => $selXpathAttr
            , "xpathParts" => $xpathParts
        );

        $fieldsEl = $keyEl->xpath("xs:field");
        if (count($fieldsEl) == 0) {
            throw new \Exception("Элемент key должен содержать хотя бы один элемент field.");
        }
        $key["fields"] = [];
        foreach ($fieldsEl as $fieldEl) {
            $fieldXpathAttr = $fieldEl["xpath"];
            if ($fieldXpathAttr === null) {
                throw new \Exception("Элемент field должен содержать атрибут xpath.");
            }
            $fieldXpathAttr .= "";
            $xpathParts = [];
            foreach (explode("/", $fieldXpathAttr) as $xpathPart) {
                $xpathParts[] = $this->parseXpathPart($xpathPart, $schema);
            }
            $key["fields"][] = array(
                "xpath" => $fieldXpathAttr
                , "xpathParts" => $xpathParts
            );
        }
        return $key;
    }
    
    private function parseKeyrefElement($keyrefEl, $schema) {
        $keyref = array(
            "nodeName" => "keyref"
        );
        $keyrefNameAttr = $keyrefEl["name"];
        if ($keyrefNameAttr === null) {
            throw new \Exception("Элемент selector должен содержать атрибут xpath.");
        }
        $keyrefNameAttr .= "";
        $keyref["name"] = $keyrefNameAttr;

        $selectorEl = $keyrefEl->xpath("xs:selector")[0];
        if ($selectorEl === null) {
            throw new \Exception("Элемент keyref должен содержать элемент selector.");
        }
        $selXpathAttr = $selectorEl["xpath"];
        if ($selXpathAttr === null) {
            throw new \Exception("Элемент selector должен содержать атрибут xpath.");
        }
        $selXpathAttr .= "";
        $xpathParts = [];
        foreach (explode("/", $selXpathAttr) as $xpathPart) {
            $xpathParts[] = $this->parseXpathPart($xpathPart, $schema);
        }
        $keyref["selector"] = array(
            "xpath" => $selXpathAttr
            , "xpathParts" => $xpathParts
        );

        $fieldsEl = $keyrefEl->xpath("xs:field");
        if (count($fieldsEl) == 0) {
            throw new \Exception("Элемент keyref должен содержать хотя бы один элемент field.");
        }
        $keyref["fields"] = [];
        foreach ($fieldsEl as $fieldEl) {
            $fieldXpathAttr = $fieldEl["xpath"];
            if ($fieldXpathAttr === null) {
                throw new \Exception("Элемент field должен содержать атрибут xpath.");
            }
            $fieldXpathAttr .= "";
            $xpathParts = [];
            foreach (explode("/", $fieldXpathAttr) as $xpathPart) {
                $xpathParts[] = $this->parseXpathPart($xpathPart, $schema);
            }
            $keyref["fields"][] = array(
                "xpath" => $fieldXpathAttr
                , "xpathParts" => $xpathParts
            );
        }
        return $keyref;
    }
    
    private function parseUniqueElement($uniqueEl, $schema) {
        $unique = array(
            "nodeName" => "unique"
        );
        $uniqueNameAttr = $uniqueEl["name"];
        if ($uniqueNameAttr === null) {
            throw new \Exception("Элемент selector должен содержать атрибут xpath.");
        }
        $uniqueNameAttr .= "";
        $unique["name"] = $uniqueNameAttr;

        $selectorEl = $uniqueEl->xpath("xs:selector")[0];
        if ($selectorEl === null) {
            throw new \Exception("Элемент unique должен содержать элемент selector.");
        }
        $selXpathAttr = $selectorEl["xpath"];
        if ($selXpathAttr === null) {
            throw new \Exception("Элемент selector должен содержать атрибут xpath.");
        }
        $selXpathAttr .= "";
        $xpathParts = [];
        foreach (explode("/", $selXpathAttr) as $xpathPart) {
            $xpathParts[] = $this->parseXpathPart($xpathPart, $schema);
        }
        $unique["selector"] = array(
            "xpath" => $selXpathAttr
            , "xpathParts" => $xpathParts
        );

        $fieldsEl = $uniqueEl->xpath("xs:field");
        if (count($fieldsEl) == 0) {
            throw new \Exception("Элемент unique должен содержать хотя бы один элемент field.");
        }
        $unique["fields"] = [];
        foreach ($fieldsEl as $fieldEl) {
            $fieldXpathAttr = $fieldEl["xpath"];
            if ($fieldXpathAttr === null) {
                throw new \Exception("Элемент field должен содержать атрибут xpath.");
            }
            $fieldXpathAttr .= "";
            $xpathParts = [];
            foreach (explode("/", $fieldXpathAttr) as $xpathPart) {
                $xpathParts[] = $this->parseXpathPart($xpathPart, $schema);
            }
            $unique["fields"][] = array(
                "xpath" => $fieldXpathAttr
                , "xpathParts" => $xpathParts
            );
        }
        return $unique;
    }
    
    private function parseElementAdditionalAttrs($el, $name) {
        $minOccurs = $el["minOccurs"];
        if ($minOccurs === null) {
            $minOccurs = "1";
        }
        $minOccurs .= "";
        if (!is_numeric($minOccurs)) {
            throw new \Exception("Атрибут minOccurs у элемента " . $name . " должен быть числом.");
        }
        $maxOccurs = $el["maxOccurs"];
        if ($maxOccurs === null) {
            $maxOccurs = "1";
        }
        $maxOccurs .= "";
        $default = $el["default"];
        if ($default !== null) {
            $default .= "";
        }
        if ($fixed !== null) {
            $fixed .= "";
        }
        $fixed = $el["fixed"];
        if ($default !== null && $fixed !== null) {
            throw new \Exception("Атрибут default и атрибут fixed не могут быть представлены в одном элементе element.");
        }
        if (!(is_numeric($maxOccurs) || $maxOccurs == "unbounded")) {
            throw new \Exception("Атрибут maxOccurs у элемента " . $name . " должен быть числом или unbounded.");
        }
        return array(
            "minOccurs" => $minOccurs
            , "maxOccurs" => $maxOccurs
            , "default" => $default
            , "fixed" => $fixed
        );
    }

    public function getElementsBySubstitutionGroupInOtherSchems($substitutionGroup, $schema) {
        $elements = [];
        $importsEls = $schema->xpath("xs:import");
        foreach ($importsEls as $importEl) {
            $importSchema = $this->getSchema(Utils::relativePathToAbsolute($importEl["schemaLocation"], $this->getAbsSchemaBasePath($schema)), $importEl["namespace"] . "");
            $ns = $this->getNsByNamespace($importSchema, $schema["targetNamespace"]);
            $element = $importSchema->xpath("xs:element[@substitutionGroup='" . $ns . ":" . $substitutionGroup . "']")[0];
            if ($element !== null) {
                $elements[] = $element;
            }           
        }
        return $elements;
    }
    
    private function parseElementWithRef($el, $schema, $parentElementPath) {
        $refAttr = $el["ref"];
        $refData = $this->parseRefAttr($refAttr, $schema);
        $ref = $refData["ref"];
        $elementPath = $parentElementPath . "/element[@ref=" . $ref . "]"; 
        if ($this->elementsData[$elementPath] !== null) {
            return $this->elementsData[$elementPath];
        }
        $additData = $this->parseElementAdditionalAttrs($el, $ref);
        // найдем элементы с substitutionGroup равным нашему элементу
        $substitutionGroupElements = $this->getElementsBySubstitutionGroupInOtherSchems($ref, $schema);
        if (count($substitutionGroupElements)) {
            $additData["union"] = array(
                "memberTypes" => []
            );
            foreach ($substitutionGroupElements as $substitutionGroupElement) {
                $substitutionGroupElementData = $this->parseElement($substitutionGroupElement, $substitutionGroupElement->xpath("//xs:schema")[0]);
                $additData["union"]["memberTypes"][] = $substitutionGroupElementData["type"];
            }
        }
        // возмем данные от элемента на который ссылаемся и добавим их к своим. При этом можно заметить что elementPath будет не тот который явлется ключом в $this->elementsData.
        $el = $this->getElementByName($ref, $schema, $namespace);
        if ($el->getName() != "element") {
            throw new \Exception("Элемент должен быть типа element.");
        }
        // Узнаем схему элемента исходя из буфера. 
        $schema = $this->getParameterResult("schema");
        //
        $data = $this->parseElement($el, $schema);
        if (!$data["type"]["isComplexType"]) {
            throw new \Exception("Элемент должен быть типа complexType.");
        }
        $output = array_merge($data, $additData);
        // Положим в кэш
        $this->elementsData[$elementPath] = $output;
        return $output;
    }
    
    private function parseElement($el, $schema, $parentElementPath = null) {
        // Посмотрим атрибуты, которые не поддерживаются
        $abstractAttr = $el["abstract"];
        if ($abstractAttr !== null) {
            throw new \Exception("Данный фреймворк не парсит атрибут abstract у элемента element.");
        }
        $blockAttr = $el["block"];
        if ($blockAttr !== null) {
            throw new \Exception("Данный фреймворк не парсит атрибут block у элемента element.");
        }
        $finalAttr = $el["final"];
        if ($finalAttr !== null) {
            throw new \Exception("Данный фреймворк не парсит атрибут final у элемента element.");
        }
        $formAttr = $el["form"];
        if ($formAttr !== null) {
            throw new \Exception("Данный фреймворк не парсит атрибут form у элемента element.");
        }
        $idAttr = $el["id"];
        if ($idAttr !== null) {
            throw new \Exception("Данный фреймворк не парсит атрибут id у элемента element.");
        }
        $nillableAttr = $el["nillable"];
        if ($nillableAttr !== null) {
            throw new \Exception("Данный фреймворк не парсит атрибут nillable у элемента element.");
        }
        // Расмотри другие атрибуты
        $schemaFullPath = $this->getSchemaFullPath($schema);
        $elementPath = $schemaFullPath;
        if ($parentElementPath !== null) {
            $elementPath = $parentElementPath;
        }
        $elementPath .= "/element";
        $name = $el["name"];
        if ($name === null) {
            $refAttr = $el["ref"];
            if ($refAttr !== null) {
                return $this->parseElementWithRef($el, $schema, $parentElementPath);
            }
            throw new \Exception("Не указан атрибут name у элемента element: " . $elementPath . ".");
        }
        $name .= "";
        $elementPath .= "[@name=" . $name . "]";
        if ($this->elementsData[$elementPath] !== null) {
            return $this->elementsData[$elementPath];
        }
        $type = $el["type"];
        if ($type === null) {
            throw new \Exception("Не указан атрибут type у элемента element.");
        }
        $typeData = $this->parseTypeAttr($type, $schema);
        
        $substitutionGroupData = array();
        $substitutionGroupAttr = $el["substitutionGroup"];
        if ($substitutionGroupAttr !== null) {
            $substitutionGroupData = $this->parseSubstitutionGroupAttr($substitutionGroupAttr, $schema);
        } else {
            $additData = $this->parseElementAdditionalAttrs($el, $name);
        }
            
        $keys = [];
        $keyEls = $el->xpath("xs:key");
        foreach ($keyEls as $keyEl) {
            $keys[] = $this->parseKeyElement($keyEl, $schema);
        }
        $keyrefs = [];
        $keyrefEls = $el->xpath("xs:keyref");
        foreach ($keyrefEls as $keyrefEl) {
            $keyrefs[] = $this->parseKeyrefElement($keyrefEl, $schema);
        }
        $uniques = [];
        $uniqueEls = $el->xpath("xs:unique");
        foreach ($uniqueEls as $uniqueEl) {
            $uniques[] = $this->parseUniqueElement($uniqueEl, $schema);
        }
        
        $annotationData = array();
        $annotationEl = $el->xpath("xs:annotation")[0];
        if ($annotationEl !== null) {
            $annotationData = $this->parseElementAnnotations($annotationEl, $schema);
        }
        // сделаем кое какие проверки
        if ($typeData["isComplexType"] && ($additData["default"] !== null || $additData["fixed"] !== null)) {
            throw new \Exception("Сложные типы не могут иметь атрибуты default и fixed.");
        }
        if ($parentElementPath === null) {
            if (!$typeData["isComplexType"]) {
                throw new \Exception("Данный фреймворк позволяет создавать в корне схемы элементы только типа complexType.");
            }
            if ($schemaFullPath != $typeData["schemaFullPath"]) {
                throw new \Exception("Данный фреймворк позволяет создавать в корне схемы элементы только текущего типа.");
            }
        }
        $output = array(
            "name" => $name
            , "type" => $typeData
            , "minOccurs" => $additData["minOccurs"]
            , "maxOccurs" => $additData["maxOccurs"]
            , "default" => $additData["default"]
            , "fixed" => $additData["fixed"]
            , "keys" => $keys
            , "keyrefs" => $keyrefs
            , "uniques" => $uniques
            , "annotation" => $annotationData
            , "substitutionGroup" => $substitutionGroupData
            , "schemaFullPath" => $schemaFullPath
            , "elementPath" => $elementPath
            , "nodeName" => "element"
            , "targetNamespace" => $this->schemasData[$schemaFullPath]["targetNamespace"]
        );
        // Положим в кэш
        $this->elementsData[$elementPath] = $output;
        return $output;
    }

    private function parseChoice($choiceElement, $schema, $parentElementPath, $level = 2) {
        // Расмотрит атрибуты которые не парсятся
        $id = $choiceElement["id"];
        if ($id !== null) {
            throw new \Exception("Данный фреймворк не парсит атрибут id у элемента choice.");
        }
        // Расмотрим элементы которые не парсятся
        $group = $choiceElement->xpath("xs:group")[0];
        if ($group !== null) {
            throw new \Exception("Данный фреймворк не парсит элемент group у элемента choice.");
        }
        $choice = $choiceElement->xpath("xs:choice")[0];
        if ($choice !== null) {
            throw new \Exception("Данный фреймворк не парсит элемент choice у элемента choice.");
        }
        $any = $choiceElement->xpath("xs:any")[0];
        if ($any !== null) {
            throw new \Exception("Данный фреймворк не парсит элемент any у элемента choice.");
        }
        // Расмотрим другие атрибуты и элементы
        $maxOccurs = $choiceElement["maxOccurs"];
        if ($maxOccurs === null) {
            $maxOccurs = "1";
        }
        $maxOccurs .= "";
        if ($maxOccurs == "unbounded" || intval($maxOccurs) > 1) {
            throw new \Exception("Данный фреймворк не позволяет элементу choice иметь maxOccurs больше 1.");
        }
        $minOccurs = $choiceElement["minOccurs"];
        if ($minOccurs === null) {
            $minOccurs = "1";
        }
        $minOccurs .= "";
        
        if ($level > 2) {
            throw new \Exception("Данный фреймворк не позволяет вложенность choice больше 3 уровня..");
        }
        $elementPath = $parentElementPath . "/choice";
        $choice = array(
            "nodeName" => "choice"
            , "level" => $level
            , "elements" => array()
            , "minOccurs" => $minOccurs
            , "maxOccurs" => $maxOccurs
        );
        $level++;
        $choiceElementChilds = $choiceElement->xpath("*");
        if (count($choiceElementChilds) == 0) {
            throw new \Exception("Элемент choice должен содержать элементы.");
        }
        foreach ($choiceElementChilds as $choiceElementChild) {
            if ($choiceElementChild->getName() == "element") {
                $choice["elements"][] = $this->parseElement($choiceElementChild, $schema, $elementPath);
            } else if ($choiceElementChild->getName() == "sequence") {
                $choice["elements"][] = $this->parseSequence($choiceElementChild, $schema, $elementPath, $level);
            } else {
                throw new \Exception("Элемент " . $choiceElementChild->getName() . " не обрабатывается!");
            }
        }
        // Первые элементы должны иметь minOccurs = 1
        foreach ($choice["elements"] as $element) {
            if (($element["nodeName"] == "element" && intval($element["minOccurs"]) == 0) 
                    || ($element["nodeName"] == "sequence" && intval($element["elements"][0]["minOccurs"]) == 0)) {
                throw new \Exception("Элемент choice должен содержать первые элементы с minOccurs равные 1.");
            }
        }
        
        return $choice;
    }

    private function parseSequence($sequenceElement, $schema, $parentElementPath, $level = 1) {
        // Расмотрит атрибуты которые не парсятся
        $id = $sequenceElement["id"];
        if ($id !== null) {
            throw new \Exception("Данный фреймворк не парсит атрибут id у элемента sequence.");
        }
        // Расмотрим элементы которые не парсятся
        $group = $sequenceElement->xpath("xs:group")[0];
        if ($group !== null) {
            throw new \Exception("Данный фреймворк не парсит элемент group у элемента sequence.");
        }
        $sequence = $sequenceElement->xpath("xs:sequence")[0];
        if ($sequence !== null) {
            throw new \Exception("Данный фреймворк не парсит элемент sequence у элемента sequence.");
        }
        $any = $sequenceElement->xpath("xs:any")[0];
        if ($any !== null) {
            throw new \Exception("Данный фреймворк не парсит элемент any у элемента sequence.");
        }
        // Расмотрим другие атрибуты и элементы
        $maxOccurs = $sequenceElement["maxOccurs"];
        if ($maxOccurs === null) {
            $maxOccurs = "1";
        }
        $maxOccurs .= "";
        $minOccurs = $sequenceElement["minOccurs"];
        if ($minOccurs === null) {
            $minOccurs = "1";
        }
        $minOccurs .= "";
        
        if ($level > 3) {
            throw new \Exception("Данный фреймворк не позволяет вложенность sequence больше 3 уровня..");
        }
        $sequence = array(
            "nodeName" => "sequence"
            , "level" => $level
            , "elements" => array()
            , "minOccurs" => $minOccurs
            , "maxOccurs" => $maxOccurs
        );
        $level++;
        $elementPath = $parentElementPath . "/sequence";
        $sequenceElementChilds = $sequenceElement->xpath("*");
        if (count($sequenceElementChilds) == 0) {
            throw new \Exception("Элемент sequence должен содержать элементы.");
        }        
        foreach ($sequenceElementChilds as $sequenceElementChild) {
            if ($sequenceElementChild->getName() == "element") {
                $sequence["elements"][] = $this->parseElement($sequenceElementChild, $schema, $elementPath);
            } else if ($sequenceElementChild->getName() == "choice") {
                $sequence["elements"][] = $this->parseChoice($sequenceElementChild, $schema, $elementPath, $level);
            } else {
                throw new \Exception("Элемент " . $sequenceElementChild->getName() . " не обрабатывается!");
            }
        }
        return $sequence;
    }

    private function getDeliveredComplexTypeData($type, $schema, $namespace) {
        $data = null;
        $targetNamespace = $schema["targetNamespace"];
        if ($targetNamespace == $namespace) {
            $data = $this->parseComplexType($type, $schema);
        } else {
            $importedSchema = $this->getImportedSchemaByNamespace($namespace, $schema);
            $data = $this->parseComplexType($type, $importedSchema);
        }
        if ($data === null) {
            throw new \Exception("Данные по элементу complexType c атрибутом name равным " . $type . " не найдены.");
        }
        return $data;
    }

    private function parseComplexTypeAnnotation($annotationElement, $schema) {
        $output = array();
        $annotationElementChilds = $annotationElement->xpath("xs:*");
        $output["appinfo"] = array(
            "annotations" => array()
        );
        foreach ($annotationElementChilds as $annotationElementChild) {
            if ($annotationElementChild->getName() == "document") {
                $documentEl = $annotationElementChild;
                $output["document"] = $documentEl . "";
            } else if ($annotationElementChild->getName() == "appinfo") {
                $appinfoEl = $annotationElementChild;
                $annotEls = $appinfoEl->xpath("an:*");
                if (count($annotEls) > 0) {
                    foreach ($annotEls as $annotEl) {
                        if ($annotEl->getName() == "array") {
                            $output["appinfo"]["annotations"]["array"] = true;
                        } else if ($annotEl->getName() == "relationship") {
                            $output["appinfo"]["annotations"]["relationship"] = true;
                        } else if ($annotEl->getName() == "table") {
                            $tableName = $annotEl["name"];
                            if ($tableName !== null) {
                                $tableName .= "";
                            } else {
                                $tableName = Utils::lowerCaseFirst($name);
                            }
                            $output["appinfo"]["annotations"]["table"] = array(
                                "name" => $tableName
                            );
                        } else {
                            throw new \Exception("Элемент " . $annotEl->getName() . " не обрабатывается!");
                        }
                    }
                }
            } else {
                throw new \Exception("Элемент " . $annotationElementChild->getName() . " не обрабатывается!");
            }
        }

        return $output;
    }

    private function checkDeliveridProperties($sequence1, $sequence2) {
        $elements1 = array();
        $this->putElementsInArray($sequence1, $elements1);
        $elements2 = array();
        $this->putElementsInArray($sequence2, $elements2);
        foreach ($elements1 as $el1) {
            foreach ($elements2 as $el2) {
                if ($el1["name"] == $el2["name"]) {
                    throw new \Exception("Данный фреймворк не позволяет наследовать одинаковые элементы.");
                }
            }
        }
    }

    private function parseComplexContent($complexContentElement, $schema, $parentElementPath) {
        // Расмотрит атрибуты которые не парсятся
        $id = $complexContentElement["id"];
        if ($id !== null) {
            throw new \Exception("Данный фреймворк не парсит атрибут id у элемента complexContent.");
        }
        $mixed = $complexContentElement["mixed"];
        if ($mixed !== null) {
            throw new \Exception("Данный фреймворк не парсит атрибут mixed у элемента complexContent.");
        }
        // Расмотрим элементы которые не парсятся
        $restriction = $complexContentElement->xpath("xs:restriction")[0];
        if ($restriction !== null) {
            throw new \Exception("Данный фреймворк не парсит элемент restriction у элемента complexContent.");
        }
        $elementPath = $parentElementPath . "/complexContent";
        // Расмотрим другие атрибуты 
        $output = array();
        $complexContentElementChild = $complexContentElement->xpath("xs:*")[0];
        if ($complexContentElementChild->getName() == "extension") {
            $extensionElement = $complexContentElementChild;
            // Расмотрит атрибуты которые не парсятся
            $id = $extensionElement["id"];
            if ($id !== null) {
                throw new \Exception("Данный фреймворк не парсит атрибут id у элемента extension(complexType).");
            }
            // Расмотрим элементы которые не парсятся
            $group = $extensionElement->xpath("xs:group")[0];
            if ($group !== null) {
                throw new \Exception("Данный фреймворк не парсит элемент group у элемента extension(complexType).");
            }
            $all = $extensionElement->xpath("xs:all")[0];
            if ($all !== null) {
                throw new \Exception("Данный фреймворк не парсит элемент all у элемента extension(complexType).");
            }
            $choice = $extensionElement->xpath("xs:choice")[0];
            if ($choice !== null) {
                throw new \Exception("Данный фреймворк не парсит элемент choice у элемента extension(complexType).");
            }
            $attributeGroup = $extensionElement->xpath("xs:attributeGroup")[0];
            if ($attributeGroup !== null) {
                throw new \Exception("Данный фреймворк не парсит элемент attributeGroup у элемента extension(complexType).");
            }
            $attribute = $extensionElement->xpath("xs:attribute")[0];
            if ($attribute !== null) {
                throw new \Exception("Данный фреймворк не парсит элемент attribute у элемента extension(simpleType).");
            }
            $anyAttribute = $extensionElement->xpath("xs:anyAttribute")[0];
            if ($anyAttribute !== null) {
                throw new \Exception("Данный фреймворк не парсит элемент anyAttribute у элемента complexType(simpleType).");
            }
            // Расмотрим другие атрибуты и элементы
            
            $base = $extensionElement["base"];
            if ($base === null) {
                throw new \Exception("Ожидался атрибут base у элемента extension.");
            }
            $baseData = $this->parseBaseAttr($base, $schema);
            $output["extension"] = array(
                "base" => $baseData
            );
            $type = $baseData["type"];
            $namespace = $baseData["namespace"];
            $extensionElementChild = $extensionElement->xpath("xs:*")[0];
            if ($extensionElementChild->getName() == "sequence") {
                $output["extension"]["sequence"] = $this->parseSequence($extensionElementChild, $schema, $elementPath . "/extension/sequence");
                $data = $this->getDeliveredComplexTypeData($type, $schema, $namespace);
                $this->checkDeliveridProperties($output["extension"]["sequence"], $data["sequence"]);
            } else {
                throw new \Exception("Элемент " . $extensionElementChild->getName() . " не обрабатывается!");
            }
        } else {
            throw new \Exception("Элемент " . $complexContentElementChild->getName() . " не обрабатывается!");
        }
        return $output;
    }
    
    private function checkUniqueElements($inputElement) {
        if ($inputElement["elements"] === null || count($inputElement["elements"]) == 0) {
            return array();
        }
        $uniqueElementNames = array();
        
        foreach ($inputElement["elements"] as $element) {
            // 1. положим простые элементы
            if ($element["nodeName"] == "element") {
                if (in_array($element["name"], $uniqueElementNames)) {
                    throw new \Exception("Элементы sequence должны иметь уникальные элементы.");
                }
            } else if ($element["nodeName"] == "choice") {
                // 2 Элементы choice могут иметь элементы только типа element или sequence. При этом sequence должен иметь только элементы типа element
                $choiceDescendantElementNames = [];
                // 2.1 возмем первые вложенные элементы у sequence, если текущий элемент sequence, или сам элемент, если элемент element        
                foreach ($element["elements"] as $choiceChildElement) {
                    if ($choiceChildElement["nodeName"] == "sequence") {
                        if ($choiceChildElement["elements"][0] === null)  {
                            throw new \Exception("Укажите элементы для sequence.");
                        }

                        $choiceChildElementName = $choiceChildElement["elements"][0]['name'];
                    } else {
                        $choiceChildElementName = $choiceChildElement['name'];
                    }
                    if (in_array($choiceChildElementName, $choiceDescendantElementNames)) {
                        throw new \Exception("Элементы choice должны иметь уникальные первые элементы.");
                    }
                    $choiceDescendantElementNames[] = $choiceChildElementName;
                }
                // 2.2 проверим чтоб первые элементы были уникальными среди всех вложенных элементов. Рассмотрим только элементы внутри sequence, так как частичная проверка на уникальность уже была выше.
                foreach ($choiceDescendantElementNames as $choiceDescendantElementName) {
                    if ($choiceChildElement["nodeName"] == "sequence") {       
                        for($i = 1, $l = count($choiceChildElement["elements"]); $i < $l; $i++) {
                            $sequenceChildElement = $choiceChildElement["elements"][$i];
                            // проверим так же чтоб не были одинаковые элементы в одном sequence
                            if ($sequenceChildElement["name"] == $choiceChildElement["elements"][0]["name"]) {
                                throw new \Exception("Элементы sequence должны иметь уникальные элементы.");
                            }
                            if (in_array($sequenceChildElementName, $choiceDescendantElementNames)) {
                                throw new \Exception("Элементы choice должны иметь уникальные первые элементы.");
                            }
                        }
                    }
                }
                // 2.3 возмем остальные элементы по одному разу
                foreach ($element["elements"] as $choiceChildElement) {
                    if ($choiceChildElement["nodeName"] == "sequence") {       
                        foreach ($choiceChildElement["elements"] as $sequenceChildElement) {
                            $sequenceChildElementName = $sequenceChildElement['name'];
                            if (in_array($sequenceChildElementName, $choiceDescendantElementNames)) {
                                continue;
                            }
                            $choiceDescendantElementNames[] = $sequenceChildElementName;
                        }
                    } else {
                        $choiceChildElementName = $choiceChildElement['name'];
                        if (in_array($choiceChildElementName, $choiceDescendantElementNames)) {
                            continue;
                        }
                        $choiceDescendantElementNames[] = $choiceChildElementName;
                    }                    
                }
                // 2.4 положим найденные элементы в $uniqueElements
                foreach ($choiceDescendantElementNames as $choiceDescendantElementName) {
                    if (in_array($choiceDescendantElementName, $uniqueElementNames)) {
                        throw new \Exception("Элементы sequence должны иметь уникальные элементы.");
                    }
                }
            }
        }
        return true;
    }

    private function parseComplexType($complexTypeElement, $schema) {
        if (is_string($complexTypeElement)) {
            $name = $complexTypeElement;
            $complexTypeElement = $this->getElementByName($name, $schema);
            // Присвоим новое значение схемы
            $schema = $this->getParameterResult("schema");
            if ($complexTypeElement->getName() != "complexType") {
                throw new \Exception("Ожидалось что элемент с атрибутом name равным " . $name . " будет complexType.");
            }
        }
        // Расмотрит атрибуты которые не парсятся
        $id = $complexTypeElement["id"];
        if ($id !== null) {
            throw new \Exception("Данный фреймворк не парсит атрибут id у элемента complexType.");
        }
        $abstract = $complexTypeElement["abstract"];
        if ($abstract !== null) {
            throw new \Exception("Данный фреймворк не парсит атрибут abstract у элемента complexType.");
        }
        $block = $complexTypeElement["block"];
        if ($block !== null) {
            throw new \Exception("Данный фреймворк не парсит атрибут block у элемента complexType.");
        }
        $final = $complexTypeElement["final"];
        if ($final !== null) {
            throw new \Exception("Данный фреймворк не парсит атрибут final у элемента complexType.");
        }
        $mixed = $complexTypeElement["mixed"];
        if ($mixed !== null) {
            throw new \Exception("Данный фреймворк не парсит атрибут mixed у элемента complexType.");
        }
        // Расмотрим элементы которые не парсятся
        $simpleContent = $complexTypeElement->xpath("xs:simpleContent")[0];
        if ($simpleContent !== null) {
            throw new \Exception("Данный фреймворк не парсит элемент simpleContent у элемента complexType.");
        }
        $group = $complexTypeElement->xpath("xs:group")[0];
        if ($group !== null) {
            throw new \Exception("Данный фреймворк не парсит элемент group у элемента complexType.");
        }
        $all = $complexTypeElement->xpath("xs:all")[0];
        if ($all !== null) {
            throw new \Exception("Данный фреймворк не парсит элемент all у элемента complexType.");
        }
        $choice = $complexTypeElement->xpath("xs:choice")[0];
        if ($choice !== null) {
            throw new \Exception("Данный фреймворк не парсит элемент choice у элемента complexType.");
        }
        $attributeGroup = $complexTypeElement->xpath("xs:attributeGroup")[0];
        if ($attributeGroup !== null) {
            throw new \Exception("Данный фреймворк не парсит элемент attributeGroup у элемента complexType(simpleType).");
        }
        $attribute = $complexTypeElement->xpath("xs:attribute")[0];
        if ($attribute !== null) {
            throw new \Exception("Данный фреймворк не парсит элемент attribute у элемента complexType(simpleType).");
        }
        $anyAttribute = $complexTypeElement->xpath("xs:anyAttribute")[0];
        if ($anyAttribute !== null) {
            throw new \Exception("Данный фреймворк не парсит элемент anyAttribute у элемента complexType(simpleType).");
        }
        // Расмотрим другие атрибуты и элементы
        $output = array();

        $name = $complexTypeElement["name"];
        if ($name === null) {
            throw new \Exception("Не указан атрибут name у элемента complexType.");
        }
        $name .= "";
        $schemaFullPath = $this->getSchemaFullPath($schema);
        $elementPath = $schemaFullPath . "/complexType[@name=" . $name . "]";
        if ($this->complexTypeElementsData[$elementPath] !== null) {
            return $this->complexTypeElementsData[$elementPath];
        }
        $output["name"] = $name;
        $output["schemaFullPath"] = $schemaFullPath;
        $output["elementPath"] = $elementPath;
        $output["sequence"] = array();
        $output["annotation"] = array();
        $output["elementsMap"] = array();
        foreach ($complexTypeElement->xpath("xs:*") as $complexTypeElementChild) {
            if ($complexTypeElementChild->getName() == "annotation") {
                $output["annotation"] = $this->parseComplexTypeAnnotation($complexTypeElementChild, $schema);
            } else if ($complexTypeElementChild->getName() == "sequence") {
                $output["sequence"] = $this->parseSequence($complexTypeElementChild, $schema, $elementPath);
            } else if ($complexTypeElementChild->getName() == "complexContent") {
                $complexContentData = $this->parseComplexContent($complexTypeElementChild, $schema, $elementPath);
                $output["complexContent"] = $complexContentData;
                $output["sequence"]["nodeName"] = "sequence";
                $output["sequence"]["minOccurs"] = "1";
                $output["sequence"]["maxOccurs"] = "1";
                $output["sequence"]["elements"] = [];
                $this->putElementsInArray($this->complexTypeElementsData[$output["complexContent"]["extension"]["base"]["elementPath"]]["sequence"], $output["sequence"]["elements"]);
                $this->putElementsInArray($output["complexContent"]["extension"]["sequence"], $output["sequence"]["elements"]);
            } else {
                throw new \Exception("Элемент " . $complexTypeElementChild->getName() . " не обрабатывается!");
            }
        }
        $this->checkUniqueElements($output["sequence"]);
        $this->putElementsInArray($output["sequence"], $output["elementsMap"]);
        // Положим в кэш
        $this->complexTypeElementsData[$elementPath] = $output;
        return $output;
    }
    
    private function putElementsInArray(&$inputElements, &$elements) {
        // Положим элементы из данной последовательности
        foreach ($inputElements["elements"] as &$element) {
            if ($element["nodeName"] == "element") {
                $elements[$element["name"]] = &$element;
            }
        }
        unset($element);// нужно удалять обязательно
        // Затем положим элементы из вложенных последовательностей
        foreach ($inputElements["elements"] as &$element) {
            if ($element["nodeName"] != "choice" && $element["nodeName"] != "sequence") {
                continue;
            }
            $this->putElementsInArray($element, $elements);
            
        }
    }

    public function parseXsdTypes() {
        if ($this->schemasData[""] !== null) {
            return;
        }
        $this->schemasData[""] = array(
            "schemaFullPath" => ""
            , "targetNamespace" => "http://www.w3.org/2001/XMLSchema"
            , "elements" => array(
                array("name" => "boolean")
                , array("name" => "byte")
                , array("name" => "short")
                , array("name" => "int")
                , array("name" => "integer")
                , array("name" => "long")
                , array("name" => "float")
                , array("name" => "double")
                , array("name" => "decimal")
                , array("name" => "unsignedByte")
                , array("name" => "unsignedShort")
                , array("name" => "unsignedInt")
                , array("name" => "unsignedLong")
                , array("name" => "negativeInteger")
                , array("name" => "nonNegativeInteger")
                , array("name" => "positiveInteger")
                , array("name" => "nonPositiveInteger")
                , array("name" => "string")
                , array("name" => "date")
                , array("name" => "time")
                , array("name" => "dateTime")
            )
            , "parsed" => true
        );
        foreach ($this->schemasData[""]["elements"] as &$element) {
            $element["nodeName"] = "simpleType";
            $element["elementPath"] = "/simpleType[@name=" . $element["name"] . "]";
            $element["type"] = array(
                "type" => $element["name"]
                , "namespace" => "http://www.w3.org/2001/XMLSchema"
                , "schemaFullPath" => ""
                , "elementPath" => $element["elementPath"]
            );
            $this->simpleTypeElementsData[$element["elementPath"]] = array(
                "name" => $element["name"]
                , "dataType" => $element["name"]
            );
        }
        unset($element);
    }
    
    private function getElementMethodContent($element) {
        $className = $this->getClassNameFromSchemaFullPath($element["type"]["schemaFullPath"]);
        // <!-- сделаем фикс для аннотации varName
        if ($element["annotation"]["appinfo"]["annotations"]["var"] === null) {
            $element["annotation"]["appinfo"]["annotations"]["var"] = array();
        }
        if ($element["annotation"]["appinfo"]["annotations"]["var"]["name"] === null) {
            $element["annotation"]["appinfo"]["annotations"]["var"]["name"] = $element["name"];
        }
        // -->
        $varName = $element["annotation"]["appinfo"]["annotations"]["var"]["name"];
        $validateElement = "\n\t\t" . $className . "::validate" . ucfirst($element["type"]["type"]) . "(\$obj->get" . ucfirst($varName) . "());";
        // если элемент передан по ссылке то для него может быть установлен пользовательский union
        if ($element["union"] !== null && count($element["union"]["memberTypes"]) > 0) {
            $memberTypesValidation = [];
            foreach ($element["union"]["memberTypes"] as $memberType) {
                $curClassName = $this->getClassNameFromSchemaFullPath($memberType["schemaFullPath"]);
                $curValidateElement = "\n\t\t" . $curClassName . "::validate" . ucfirst($memberType["type"]) . "(\$obj->get" . ucfirst($varName) . "());";    
                $memberTypesValidation[] = "if(\$obj->get" . ucfirst($varName) . "() instanceof " . $memberType["type"] . ") {
                    " . str_replace("\n\t", "\n\t\t", $curValidateElement) . "
                    \n\t\t}";
            }
            $validateElement = "\n\t\t" . implode(" else ", $memberTypesValidation) . " else {
                " . str_replace("\n\t", "\n\t\t", $validateElement) . "
                \n\t\t}";
        }
        if ($element["uniques"][0] !== null) {
            if ($element["annotation"]["appinfo"]["annotations"]["array"] === null) {
                throw new \Exception("Данный фреймворк парсит элементы unique только для массивов.");
            }
            $baseChain = implode("", array_map(function($xpathPart) {
                return "->get" . ucfirst($xpathPart["xpathPart"]) . "()";
            }, $element["uniques"][0]["selector"]["xpathParts"]));
            $chains = [];
            foreach ($element["uniques"][0]["fields"] as $field) {
                $chains[] = $baseChain . implode("", array_map(function($xpathPart) {
                    return "->get" . ucfirst($xpathPart["xpathPart"]) . "()";
                }, $field["xpathParts"]));
            }
            $cond = "";
            $and = "";
            foreach ($chains as $key => $chain) {
                $cond .= $and . "\$item1" . $chain . " == \$item2" . $chain;
                $and = " && ";
            }
            $validateElement .= "\n\t\t// проверим на уникальность элементов в массиве
                \n\t\tforeach (\$obj->get" . ucfirst($varName) . "() as \$item1) {
                \n\t\t\t\$finded = 0;
                \n\t\t\tforeach (\$obj->get" . ucfirst($varName) . "() as \$item2) {
                \n\t\t\t\tif (" . $cond . ") {
                \n\t\t\t\t\t\$finded++;
                \n\t\t\t\t}
                \n\t\t\t}
                \n\t\t\tif (\$finded > 1) {
                \n\t\t\t\tthrow new \Exception(\"Массив содержит не уникальные элементы.\");
                \n\t\t\t}
                \n\t\t}
                ";
        }
        
        if (intval($element["minOccurs"]) == 0) {
            $validateElement = "\n\t\tif (\$obj->get" . ucfirst($varName) . "() !== null) {
                " . str_replace("\n\t", "\n\t\t", $validateElement) . "
                \n\t\t}";
        }
        $errorMessage = "\$ex->getMessage()";
        if ($element["annotation"]["appinfo"]["annotations"]["error"] !== null) {
            $errorMessage = "\"" . $element["annotation"]["appinfo"]["annotations"]["error"]["message"] . "\"";
        }
        $validateElement = "\n\t\ttry {
            " . str_replace("\n\t", "\n\t\t", $validateElement) . "
            \n\t\t} catch (\Exception \$ex) {
            \n\t\t\tValidateResult::\$errors[\"" . $varName . "\"] = " . $errorMessage . ";
            \n\t\t}";
        
        return $validateElement;
    }
    
    private function getChoiceMethodContent($elements, $minOccurs = 1) {
        $methodContent = "";
        if (count($elements) == 0) {
            return $methodContent;
        }
        $methodContent .= ($minOccurs == 1 ? "\n\t\t\$finded = false;" : "");
        $firstElementNames = [];
        $else = "";
        foreach ($elements as $element) {
            if ($element["nodeName"] == "element") {
                $varName = $element["annotation"]["appinfo"]["annotations"]["var"]["name"];
                $firstElementNames[] = $varName;
                $methodContent .= "\n\t\t" . $else . "if (\$obj->get" . ucfirst($varName) . "() !== null) {
                    " . ($minOccurs == 1 ? "\n\t\t\t\$finded = true;" : "") . "
                    " . str_replace("\n\t", "\n\t\t", $this->getElementMethodContent($element)) . "
                    \n\t\t}";
            } else if ($element["nodeName"] == "sequence") {
                $varName = $element["elements"][0]["annotation"]["appinfo"]["annotations"]["var"]["name"];
                $firstElementNames[] = $element["elements"][0]["annotation"]["appinfo"]["annotations"]["var"]["name"];
                $methodContent .= "\n\t\t" . $else . "if (\$obj->get" . ucfirst($varName) . "() !== null) {
                    " . ($minOccurs == 1 ? "\n\t\t\t\$finded = true;" : "") . "
                    " . str_replace("\n\t", "\n\t\t", $this->getSequenceMethodContent($element["elements"])) . "
                    \n\t\t}";
            } else {
                throw new \Exception("Элемент " . $element["nodeName"] . " не обрабатывается!");
            }
            $else = "else ";
        }
        if ($minOccurs == 1) {
            $methodContent .= "\n\t\tif (!\$finded) {
                \n\t\t\tthrow new \Exception(\"Укажите что нибудь из следующего: " . implode(" | ", $firstElementNames) . "\");
                \n\t\t}";           
        }
        return $methodContent;
    }

    private function getSequenceMethodContent($elements) {
        $methodContent = "";
        if (count($elements) == 0) {
            return $methodContent;
        }

        foreach ($elements as $element) {
            if ($element["nodeName"] == "element") {
                $methodContent .= $this->getElementMethodContent($element);
            } else if ($element["nodeName"] == "choice") {
                $methodContent .= $this->getChoiceMethodContent($element["elements"], intval($element["minOccurs"]));
            } else {
                throw new \Exception("Элемент " . $element["nodeName"] . " не обрабатывается!");
            }
        }
        return $methodContent;
    }
    
    private function getComplexListMethodContent($elementData) {
        $methodContent = "";
        if (count($elementData["sequence"]["elements"]) == 0) {
            return $methodContent;
        }
        $element = $elementData["sequence"]["elements"][0];
        if (intval($elementData["sequence"]["minOccurs"]) > 0) {
            $methodContent .= "\n\t\tif (count(\$list) < " . $elementData["sequence"]["minOccurs"] . ") {
            \n\t\t\tthrow new \Exception(\"Массив с типом " . $elementData["name"] . " не должен состоять меньше чем из " . $elementData["sequence"]["minOccurs"] . " элементов.\");
            \n\t\t}";
        }
        if (is_numeric($elementData["sequence"]["maxOccurs"])) {
            $methodContent .= "\n\t\tif (count(\$list) > " . $elementData["sequence"]["maxOccurs"] . ") {
            \n\t\t\tthrow new \Exception(\"Массив с типом " . $elementData["name"] . " не должен состоять больше чем из " . $elementData["sequence"]["maxOccurs"] . " элементов.\");
            \n\t\t}";
        }
        $className = $this->getClassNameFromSchemaFullPath($element["schemaFullPath"]);
        $methodContent .= "\n\t\tforeach(\$list as \$item) {
            \n\t\t\t" . $className . "::validate" . ucfirst($element["type"]["type"]) . "(\$item);
            \n\t\t}";
        return $methodContent;
    }
    
    private function getEntryMethodContent($elementData) {
        $methodContent = "";
        if (count($elementData["sequence"]["elements"]) != 2) {
            throw new \Exception("Произошла ошибка парсинга.");
        }

        foreach ($elementData["sequence"]["elements"] as $element) {
            if ($element["nodeName"] == "element") {
                $className = $this->getClassNameFromSchemaFullPath($element["type"]["schemaFullPath"]);
                // <!-- сделаем фикс для аннотации varName
                if ($element["annotation"]["appinfo"]["annotations"]["var"] === null) {
                    $element["annotation"]["appinfo"]["annotations"]["var"] = array();
                }
                if ($element["annotation"]["appinfo"]["annotations"]["var"]["name"] === null) {
                    $element["annotation"]["appinfo"]["annotations"]["var"]["name"] = $element["name"];
                }
                // -->
                $varName = $element["annotation"]["appinfo"]["annotations"]["var"]["name"];
                $validateElement = "\n\t\t" . $className . "::validate" . ucfirst($element["type"]["type"]) . "(\$" . $varName . ");";
                $errorMessage = "\$ex->getMessage()";
                if ($element["annotations"]["error"] !== null) {
                    $errorMessage = "\"" . $element["annotations"]["error"]["message"] . "\"";
                }
                $validateElement = "\n\t\ttry {
                    " . str_replace("\n\t", "\n\t\t", $validateElement) . "
                    \n\t\t} catch (\Exception \$ex) {
                    \n\t\t\tValidateResult::\$errors[\"" . $varName . "\"] = " . $errorMessage . ";
                    \n\t\t}";
                $methodContent .= $validateElement;
            } else {
                throw new \Exception("Элемент " . $element["nodeName"] . " не обрабатывается!");
            }
        }
        return $methodContent;
    }
    
    private function getComplexMapMethodContent($elementData) {
        $methodContent = "";
        if (count($elementData["sequence"]["elements"]) == 0) {
            return $methodContent;
        }
        $element = $elementData["sequence"]["elements"][0];
        if (intval($elementData["sequence"]["minOccurs"]) > 0) {
            $methodContent .= "\n\t\tif (count(\$map) < " . $elementData["sequence"]["minOccurs"] . ") {
            \n\t\t\tthrow new \Exception(\"Карта " . $elementData["name"] . " не должна состоять меньше чем из " . $elementData["sequence"]["minOccurs"] . " элементов.\");
            \n\t\t}";
        }
        if (is_numeric($elementData["sequence"]["maxOccurs"])) {
            $methodContent .= "\n\t\tif (count(\$map) > " . $elementData["sequence"]["maxOccurs"] . ") {
            \n\t\t\tthrow new \Exception(\"Карта " . $elementData["name"] . " не должна состоять больше чем из " . $elementData["sequence"]["maxOccurs"] . " элементов.\");
            \n\t\t}";
        }
        
        $className = $this->getClassNameFromSchemaFullPath($element["schemaFullPath"]);
        
        $methodContent .= "\n\t\tforeach(\$map as \$key => \$value) {
            \n\t\t\t" . $className . "::validate" . ucfirst($element["type"]["type"]) . "(\$key, \$value);
            \n\t\t}";
        return $methodContent;
    }

    private function createClassForValidateCustomType($className, $schemaData) {
        $classWrap = "<?php
                    \n/**
                    \n * Данный класс содержит статические функции для проверки переменных на валидность
                    \n *
                    \n * @author Ruslan Rakhmankulov
                    \n */
                    \nclass " . $className . " extends Validator {
                    %s
                    \n}
                    \n?>";
        
        $classProperties = array();
        $classMethods = array();
        $methodWrap = "\n\n\tstatic function %s(%s) {
                    %s
                    \n\t}";
        foreach ($schemaData["elements"] as $element) {
            if ($element["nodeName"] != "simpleType") {
                continue;
            }
            $simpleTypeElementData = $this->simpleTypeElementsData[$element["elementPath"]];
            $methodName = "validate" . ucfirst($simpleTypeElementData["name"]);
            $methodParameter = "";
            if ($simpleTypeElementData["list"] !== null) {
                $methodParameter = "\$list";
                $methodContent = $this->getListMethodContent($simpleTypeElementData);
            } else if ($simpleTypeElementData["union"] !== null) {
                $methodParameter = "\$item";
                $methodContent = $this->getUnionMethodContent($simpleTypeElementData);
            } else {
                $methodContent = $this->getItemMethodContent($simpleTypeElementData, $methodParameter);
            }
            $classMethods[] = sprintf($methodWrap, $methodName, $methodParameter, $methodContent);
        }

        foreach ($schemaData["elements"] as $element) {
            if ($element["nodeName"] != "complexType") {
                continue;
            }
            $complexTypeElementData = $this->complexTypeElementsData[$element["elementPath"]];
            $methodName = "validate" . $complexTypeElementData["name"];
            $methodParameter = "";
            if ($complexTypeElementData["annotation"]["appinfo"]["annotations"]["array"] !== null) {
                $methodParameter = "\$list";
                $methodContent = $this->getComplexListMethodContent($complexTypeElementData);
            } else if ($complexTypeElementData["annotation"]["appinfo"]["annotations"]["map"] !== null) {
                $methodParameter = "\$map";
                $methodContent = $this->getComplexMapMethodContent($complexTypeElementData);
            } else if ($complexTypeElementData["annotation"]["appinfo"]["annotations"]["select"] !== null 
                    || $complexTypeElementData["annotation"]["appinfo"]["annotations"]["entity"] !== null){
                $cacheName = Utils::lowerCaseFirst($complexTypeElementData["name"] . "Cache");
                
                $methodParameter = "\$obj";
                $methodContent = "\n\t\t// Проверим в кэше
                \n\t\tif (UserTypeValidator::\$caches[\"" . $cacheName . "\"] === null) {
                \n\t\t\tUserTypeValidator::\$caches[\"" . $cacheName . "\"] = [];
                \n\t\t}
                \n\t\tif (in_array(" . $methodParameter . "->getId(), UserTypeValidator::\$caches[\"" . $cacheName . "\"])) {
                \n\t\t\treturn;
                \n\t\t}
                \n\t\tUserTypeValidator::\$caches[\"" . $cacheName . "\"][] = " . $methodParameter . "->getId();
                \n\t\t//";
                $elements = null;
                if ($complexTypeElementData["complexContent"] !== null) {
                    $extClassName = $this->getClassNameFromSchemaFullPath($element["type"]["schemaFullPath"]);
                    $methodContent .= "\n\t\t" . $extClassName . "::validate" . ucfirst($complexTypeElementData["complexContent"]["extension"]["base"]["type"]) . "(" . $methodParameter . ");";                    
                    $elements = $complexTypeElementData["complexContent"]["extension"]["sequence"]["elements"];
                } else {
                    $elements = $complexTypeElementData["sequence"]["elements"];
                }
                
                $methodContent .= $this->getSequenceMethodContent($elements);
            } else if ($complexTypeElementData["annotation"]["appinfo"]["annotations"]["entry"] !== null){
                $methodParameter = "\$key, \$value";
                $methodContent = $this->getEntryMethodContent($complexTypeElementData);
            } else {
                continue;
            }
            $classMethods[] = sprintf($methodWrap, $methodName, $methodParameter, $methodContent);
        }
        $class = preg_replace("/\\r\\n/", "", sprintf($classWrap, implode("", $classMethods)));
        $classFullPath = $this->classesRoot . "/validators/" . $className . ".php";
        file_put_contents($classFullPath, $class);
    }

    private function createEnumsFromSimpleTypes($schemaData) {
        foreach ($schemaData["elements"] as $element) {
            if ($element["nodeName"] != "simpleType") {
                continue;
            }
            $simpleTypeElementData = $this->simpleTypeElementsData[$element["elementPath"]];
            if ($simpleTypeElementData["list"] !== null) {
                continue;
            }
            if ($simpleTypeElementData["union"] !== null) {
                continue;
            }
            foreach ($simpleTypeElementData["restriction"]["elements"] as $restrictionName => $restrictionValue) {
                if ($restrictionName == "enumeration") {
                    $className = $simpleTypeElementData["name"] . "Enum";
                    $enums = explode("|", $restrictionValue);
                    $classWrap = "<?php
                                \n/**
                                \n * Данный класс описывает перечисление
                                \n *
                                \n * @author Ruslan Rakhmankulov
                                \n */
                                \nclass %s {
                                %s
                                \n}
                                \n?>";
                    $classProperties = "";
                    $property = "\n\tconst %s = \"%s\";";
                    $classProperties = implode("", array_map(function($enum)use($property) {
                                        return sprintf($property, strtoupper($enum), $enum);
                                    }, $enums));
                    $class = preg_replace("/\\r\\n/", "", sprintf($classWrap, $className, $classProperties));
                    $classFullPath = $this->classesRoot . "/enums/" . $className . ".php";
                    file_put_contents($classFullPath, $class);
                }
            }
        }
    }

    private function createClassesWithConst($schemaData) {
        foreach ($schemaData["elements"] as $element) {
            if ($element["nodeName"] != "complexType") {
                continue;
            }
            $complexTypeElementData = $this->complexTypeElementsData[$element["elementPath"]];
            if ($complexTypeElementData["annotation"]["appinfo"]["annotations"]["table"] === null) {
                continue;
            }
            $className = $complexTypeElementData["name"] . "Property";
            $extends = "";
            if ($complexTypeElementData["complexContent"] !== null) {
                $extends = " extends " . $complexTypeElementData["complexContent"]["extension"]["base"]["type"] . "Property";
            }
            $classWrap = "<?php
                            \n/**
                            \n * Данный класс описывает модель
                            \n *
                            \n * @author Ruslan Rakhmankulov
                            \n */
                            \nclass " . $className . $extends . " {
                            %s
                            \n}
                            \n?>";
            $classConsts = array();
            foreach ($complexTypeElementData["elementsMap"] as $element2) {
                if ($complexTypeElementData["complexContent"] !== null) {
                    $finded = false;
                    foreach ($complexTypeElementData["complexContent"]["extension"]["sequence"]["elements"] as $element3) {
                        if ($element2["name"] == $element3["name"]) {
                            $finded = true;
                            break;
                        }
                    }
                    if (!$finded) {
                        continue;
                    }
                }
                $varName = $element2["annotation"]["appinfo"]["annotations"]["var"]["name"];
                $classConsts[] = "\n\tconst " . Utils::upperUnderScore($varName) . " = \"" . Utils::lowerCaseFirst($varName) . "\";";
            }
            $class = preg_replace("/\\r\\n/", "", sprintf($classWrap, implode("", $classConsts)));
            $classFullPath = $this->classesRoot . "/properties/" . $className . ".php";
            file_put_contents($classFullPath, $class);
        }
    }

    private function createClassesFromComplexTypes($schemaData) {
        foreach ($schemaData["elements"] as $element) {
            if ($element["nodeName"] != "complexType") {
                continue;
            }
            $complexTypeElementData = $this->complexTypeElementsData[$element["elementPath"]];
            if ($complexTypeElementData["annotation"]["appinfo"]["annotations"]["entity"] === null && $complexTypeElementData["annotation"]["appinfo"]["annotations"]["select"] === null) {
                continue;
            }
            
            $className = $complexTypeElementData["name"];
            $classSubFolder = "/types";
            if (strpos($className, "Response") !== false) {
                $classSubFolder = "/responses";
            } else if (strpos($className, "Request") !== false) {
                $classSubFolder = "/requests";
            }
            $extends = "";
            if ($complexTypeElementData["complexContent"] !== null) {
                $extends = " extends " . $complexTypeElementData["complexContent"]["extension"]["base"]["type"];
            }
            $classWrap = "<?php
                        \n/**
                        \n * Данный класс описывает модель
                        \n *
                        \n * @author Ruslan Rakhmankulov
                        \n */
                        \nclass " . $className . $extends . " {
                        %s
                        \n}
                        \n?>";
            $classProperties = array();
            $propertyWrap = "\n\n\t/**
                \n\t * @var %s
                \n\t */
                \n\tprotected \$%s;";
            $classMethods = array();
            $methodWrapForGetter = "\n\n\t/**
                \n\t * @return %s
                \n\t */
                \n\tpublic function %s() {
                %s
                \n\t}";
            $methodWrapForSetter = "\n\n\t/**
                \n\t * @param %s
                \n\t * @return " . $className . "
                \n\t */
                \n\tpublic function %s(%s) {
                %s
                \n\t\treturn\$this;
                \n\t}";
            $constructorWrap = "\n\n\tfunction __constructor(){
                %s
                \n\t}";
            $constructorContent = "";
            foreach ($complexTypeElementData["elementsMap"] as $element2) {
                if ($complexTypeElementData["complexContent"] !== null) {
                    $finded = false;
                    foreach ($complexTypeElementData["complexContent"]["extension"]["sequence"]["elements"] as $element3) {
                        if ($element2["name"] == $element3["name"]) {
                            $finded = true;
                            break;
                        }
                    }
                    if (!$finded) {
                        continue;
                    }
                }
                $varType = $element2["annotation"]["appinfo"]["annotations"]["var"]["type"];
                // <!-- сделаем фикс для аннотации varName
                if ($element2["annotation"]["appinfo"]["annotations"]["var"] === null) {
                    $element2["annotation"]["appinfo"]["annotations"]["var"] = array();
                }
                if ($element2["annotation"]["appinfo"]["annotations"]["var"]["name"] === null) {
                    $element2["annotation"]["appinfo"]["annotations"]["var"]["name"] = $element2["name"];
                }
                // -->
                $varName = $element2["annotation"]["appinfo"]["annotations"]["var"]["name"];
                if ($element2["annotation"]["appinfo"]["annotations"]["array"] === true) {
                    $constructorContent .= "\n\t\t\$this->" . $varName . " = array();";
                } else if ($element2["default"] !== null){
                    if ($varType == "boolean" || $varType == "int") {
                        $constructorContent .= "\n\t\t\$this->" . $varName . " = " . $element2["default"] . ";";
                    } else if ($varType == "string") {
                            $constructorContent .= "\n\t\t\$this->" . $varName . " = \"" . $element2["default"] . "\";";
                    } else {
                        throw new \Exception("Произошла ошибка парсера.");
                    }
                }
                // Свойства
                $classProperties[] = sprintf($propertyWrap, $varType, Utils::lowerCaseFirst($varName));
                // Геттер
                $methodAnnReturn = $varType;
                $methodName = "get" . ucfirst($varName);
                $methodContent = "\n\t\treturn\$this->" . Utils::lowerCaseFirst($varName) . ";";
                $classMethods[] = sprintf($methodWrapForGetter, $methodAnnReturn, $methodName, $methodContent);
                // Сеттер
                $methodAnnParam = $varType . " " . "\$" . Utils::lowerCaseFirst($varName);
                $methodName = "set";
                if ($varType == "boolean") {
                    $methodName = "is";
                }
                $methodName .= ucfirst($varName);
                $methodParameter = "\$" . Utils::lowerCaseFirst($varName);
                $methodContent = "\n\t\t\$this->" . Utils::lowerCaseFirst($varName) . " = " . $methodParameter . ";";
                $classMethods[] = sprintf($methodWrapForSetter, $methodAnnParam, $methodName, $methodParameter, $methodContent);
            }
            $constructor = "";
            if ($constructorContent) {
                $superConstructor = "";
                if ($complexTypeElementData["complexContent"] !== null) {
                    $superConstructor = "\n\t\tparent::" . $complexTypeElementData["complexContent"]["extension"]["base"]["type"] . "();";
                }
                $constructor = sprintf($constructorWrap, $superConstructor . $constructorContent);
            }

            $class = preg_replace("/\\r\\n/", "", sprintf($classWrap, implode("", $classProperties) . "\n" . implode("", $classMethods) . $constructor));
            $classFullPath = $this->classesRoot . $classSubFolder . "/" . $className . ".php";
            file_put_contents($classFullPath, $class);
        }
    } 

    private function createFormsFromComplexTypes($schemaData) {
        foreach ($schemaData["elements"] as $element) {
            if ($element["nodeName"] != "complexType") {
                continue;
            }
            $complexTypeElementData = $this->complexTypeElementsData[$element["elementPath"]];
            if ($complexTypeElementData["annotation"]["appinfo"]["annotations"]["entity"] === null && $complexTypeElementData["annotation"]["appinfo"]["annotations"]["select"] === null) {
                continue;
            }
            
            $className = $complexTypeElementData["name"] . "Form";
            $classSubFolder = "/forms";
            if (strpos($className, "Response") !== false) {
                continue;
            } else if (strpos($className, "Request") !== false) {
                continue;
            }
            $extends = "";
            $propeties = "";
            if ($complexTypeElementData["complexContent"] !== null) {
                $extends = " extends " . $complexTypeElementData["complexContent"]["extension"]["base"]["type"] . "Form";
            } else {
                $propeties = "\n\n\t/**
                        \n\t * @var FormField[] as map
                        \n\t */
                        \n\tprotected \$fields;
                        \n\n\t/**
                        \n\t * @return FormField[] as map
                        \n\t */
                        \n\tpublic function getFields() {
                        \n\t\treturn \$this->fields;
                        \n\t}
                        \n\n\t/**
                        \n\t * @param FormField[] as map \$fields
                        \n\t * @return " . $className . "
                        \n\t */
                        \n\tpublic function setFields(\$fields) {
                        \n\t\t\$this->fields = \$fields;
                        \n\t\treturn \$this;
                        \n\t}";
            }
            $classWrap = "<?php
                        \n/**
                        \n * Данный класс описывает форму
                        \n *
                        \n * @author Ruslan Rakhmankulov
                        \n */
                        \nclass " . $className . $extends . " {
                        " . $propeties . "
                        %s
                        \n}
                        \n?>";
            $initProperties = "";
            $superConstructor = "";
            if ($complexTypeElementData["complexContent"] !== null) {
                $superConstructor = "\n\t\tparent::" . $complexTypeElementData["complexContent"]["extension"]["base"]["type"] . "Form" . "();";;
            } else {
                $initProperties = "\n\t\t\$this->fields = array();";
            }
            $constructorWrap = "\n\n\tfunction __constructor(){
                " . ($initProperties != "" ? $initProperties : $superConstructor) . "
                %s
                \n\t}";
            $constructorContent = "";
            foreach ($complexTypeElementData["elementsMap"] as $element2) {
                if ($element2["annotation"]["appinfo"]["annotations"]["formField"] !== null) {
                    if ($complexTypeElementData["complexContent"] !== null) {
                        $finded = false;
                        foreach ($complexTypeElementData["complexContent"]["extension"]["sequence"]["elements"] as $element3) {
                            if ($element2["name"] == $element3["name"]) {
                                $finded = true;
                                break;
                            }
                        }
                        if (!$finded) {
                            continue;
                        }
                    }
                    $constructorContentPart = "\n\t\t\$this->fields[\"" . $element2["annotation"]["appinfo"]["annotations"]["formField"]["name"] . "\"] = (new FormField())
                        \n\t\t\t->setName(\"" . $element2["annotation"]["appinfo"]["annotations"]["formField"]["name"] . "\")
                        \n\t\t\t->setType(\"" . $element2["annotation"]["appinfo"]["annotations"]["formField"]["type"] . "\")
                        " . ($element2["annotation"]["appinfo"]["annotations"]["formField"]["required"]?"\n\t\t\t->setRequired(\"" . $element2["annotation"]["appinfo"]["annotations"]["formField"]["required"] . "\")":"") . "
                        " . ($element2["annotation"]["appinfo"]["annotations"]["formField"]["min"]?"\n\t\t\t->setMin(\"" . $element2["annotation"]["appinfo"]["annotations"]["formField"]["min"] . "\")":"") . "
                        " . ($element2["annotation"]["appinfo"]["annotations"]["formField"]["max"]?"\n\t\t\t->setMax(\"" . $element2["annotation"]["appinfo"]["annotations"]["formField"]["max"] . "\")":"") . "
                        " . ($element2["annotation"]["appinfo"]["annotations"]["formField"]["maxlength"]?"\n\t\t\t->setMaxlength(\"" . $element2["annotation"]["appinfo"]["annotations"]["formField"]["maxlength"] . "\")":"") . "
                        " . ($element2["annotation"]["appinfo"]["annotations"]["formField"]["pattern"]?"\n\t\t\t->setPattern(\"" . $element2["annotation"]["appinfo"]["annotations"]["formField"]["pattern"] . "\")":"") . "
                        " . ($element2["annotation"]["appinfo"]["annotations"]["formField"]["defaultValue"]?"\n\t\t\t->setDefaultValue(\"" . $element2["annotation"]["appinfo"]["annotations"]["formField"]["defaultValue"] . "\")":"") . ";";
                    $constructorContent .= "\n" . preg_replace('/\s+(;?)$/m', '$1', $constructorContentPart);
                }
            }
            $constructor = "";
            if ($constructorContent) {
                $constructor = sprintf($constructorWrap, $constructorContent);
            }

            $class = preg_replace("/\\r\\n/", "", sprintf($classWrap, $constructor));
            $classFullPath = $this->classesRoot . $classSubFolder . "/" . $className . ".php";
            file_put_contents($classFullPath, $class);
        }
    }

    private function isJoinTable($complexTypeElementData, $schemaData) {
        // Элементы должны состоять из двух параметров с nodeName равным element, minOccurs = 1, maxOccurs = 1 и тип должен быть сложным.
        if (count($complexTypeElementData["sequence"]["elements"]) != 2) {
            return false;
        }
        
        $isJoinTable = false;
        foreach ($complexTypeElementData["sequence"]["elements"] as $element3) {
            if ($element3["nodeName"] != "element") {
                $isJoinTable = false;
                break;
            }
            if ($element3["nodeName"] == "element" && $element3["minOccurs"] == "1" && $element3["maxOccurs"] == "1" && strpos($element3["type"]["elementPath"], "/simpleType") === false) {
                $isJoinTable = true;
                continue;
            }
            $isJoinTable = false;
            break;
        }
        if (!$isJoinTable) {
            return false;
        }
        foreach ($schemaData["elements"] as $element) {
            if ($element["nodeName"] != "element") {
                continue;
            }
            $elementData = $this->elementsData[$element["elementPath"]];
            $complexTypeElementData2 = $this->complexTypeElementsData[$elementData["type"]["elementPath"]];
            if ($complexTypeElementData2 === null) {
                throw new \Exception("Произошла ошибка парсинга. Элемент " . $elementData["type"]["elementPath"] . "должен быть в кеше.");
            }
            foreach ($complexTypeElementData2["sequence"]["elements"] as $element2) {
                if ($element2["nodeName"] == "element" && $element2["minOccurs"] == "0" && $element2["maxOccurs"] == "unbounded") {
                    if ($element2["type"]["elementPath"] == $complexTypeElementData["elementPath"]) {
                        $isJoinTable = true;
                    }
                    continue;
                }
                $isJoinTable = false;
                break;
            }
        }
        return $isJoinTable;
    }

    private function isSelect($complexTypeElementData, $schemaData) {
        // Должно быть хотя бы два элемента - id(или другой ключ) и еще какой-нибудь
        if (count($complexTypeElementData["elementsMap"]) < 2) {
            return false;
        }
        
        // Найдем хотя бы один простой тип(предположительно что это основной ключ)
        $finded = false;
        foreach ($complexTypeElementData["elementsMap"] as $element) {        
            if (strpos($element["type"]["elementPath"], "/simpleType") !== false) {
                $finded = true;
                break;
            }
        }
        if (!$finded) {
            return false;
        }
        // Элементы должны иметь параметр maxOccurs равный 1
        $isSelect = false;
        foreach ($complexTypeElementData["elementsMap"] as $element) {
            if ($element["maxOccurs"] == "1") {
                $isSelect = true;
                continue;
            }
            $isSelect = false;
            break;
        }
        if (!$isSelect) {
            return false;
        }
        // Элементы должны содержаться в entity. Если это сложный тип, то у элементво должны быть одинаковые пути к схемам. Если это простой тип, то должны быть равны абсолютные пути
        $entityComplexTypeElementData = $this->complexTypeElementsData[$this->entityElementPath];
        foreach ($complexTypeElementData["elementsMap"] as $element) {
            $found = false;
            foreach ($entityComplexTypeElementData["elementsMap"] as $entityElement) {
                if ($element["name"] == $entityElement["name"] && 
                        ((strpos($element["type"]["elementPath"], "/simpleType") === false && $element["type"]["schemaFullPath"] == $entityElement["type"]["schemaFullPath"]) 
                        || (strpos($element["type"]["elementPath"], "/simpleType") !== false && $element["type"]["elementPath"] == $entityElement["type"]["elementPath"]))) {
                    $found = true;
                    break;
                }
            }
            if (!$found) {
                throw new \Exception("Элемент с именем " . $element["name"] . " должен существовать в " . $this->entityElementPath .  ".");
            }
        }
        return $isSelect;
    }

    private function isEntity($complexTypeElementData, $schemaData) {
        // Найдем элемент complexType из элементов типа element, находящиеся в корне схемы, который содержит элементы с nodeName = element, с minOccurs = 0, с maxOccurs = 1, и который содержит элемент являющиюся экземпляром нашего типа.
        if (count($complexTypeElementData["elementsMap"]) < 2) {
            return false;
        }
        $isValidated = true;
        foreach ($complexTypeElementData["elementsMap"] as $element3) {
            if ($element3["minOccurs"] != "0" || $element3["maxOccurs"] != "1") {
                $isValidated = false;
                break;
            }
        }
        if (!$isValidated) {
            return false;
        }
        $isEntity = false;
        foreach ($schemaData["elements"] as $element) {
            if ($element["nodeName"] != "element") {
                continue;
            }
            $elementData = $this->elementsData[$element["elementPath"]];
            $complexTypeElementData2 = $this->complexTypeElementsData[$elementData["type"]["elementPath"]];
            if ($complexTypeElementData2 === null) {
                throw new \Exception("Произошла ошибка парсинга. Элемент " . $elementData["type"]["elementPath"] . "должен быть в кеше.");
            }
            $isValidated = true;
            foreach ($complexTypeElementData2["sequence"]["elements"] as $element2) {
                if ($element2["nodeName"] != "element" || $element2["minOccurs"] != "0" || $element2["maxOccurs"] != "unbounded") {
                    $isValidated = false;
                    break;
                }
            }
            if (!$isValidated) {
                continue;
            }
            foreach ($complexTypeElementData2["sequence"]["elements"] as $element2) {
                if ($element2["type"]["elementPath"] == $complexTypeElementData["elementPath"]) {
                    $isEntity = true;
                }
            }
        }
        if ($isEntity) {
            $this->isExistedEntity = $isEntity;
            $this->entityElementPath = $complexTypeElementData["elementPath"];
        }
            
        return $isEntity;
    }

    private function isRelationship($complexTypeElementData) {
        // Элементы должны быть с nodeName равным element, minOccurs = 0, maxOccurs = unbounded. 
        // И среди элементов должен быть элемент указывающий на сущность (entity)
        // Элемент relationship может быть определен один раз. 
        $isRelationship = false;
        foreach ($complexTypeElementData["sequence"]["elements"] as $element) {
            if ($element["nodeName"] == "element" && $element["minOccurs"] == "0" && $element["maxOccurs"] == "unbounded") {
                if ($element["type"]["elementPath"] == $this->entityElementPath) {
                    $isRelationship = true;
                    $this->entityTable = $element["name"];
                }
                continue;
            }
            $isRelationship = false;
            break;
        }
        if ($isRelationship) {
            $this->isExistedRelationship = $isRelationship;
            $this->relationshipElementPath = $complexTypeElementData["elementPath"];
        }            
        return $isRelationship;
    }

    private function isArray($complexTypeElementData, $schemaData) {
        // Элементы, которые в своей последовательности имеют только один элемент с атрибутом type, указывающим на данную схему. Элементы должны иметь атрибуты minOccurs и maxOccurs равные 1.
        $isArray = false;
        if (count($complexTypeElementData["sequence"]["elements"]) == 1 && $complexTypeElementData["sequence"]["elements"][0]["type"]["schemaFullPath"] == $schemaData["schemaFullPath"] && intval($complexTypeElementData["sequence"]["elements"][0]["minOccurs"]) == 1 && $complexTypeElementData["sequence"]["elements"][0]["maxOccurs"] == "1") {
            $isArray = true;
        }
        return $isArray;
    }
    
    private function setFormAnnotationsForEntitySimpleElement(&$element, $varArray, $varType, $dataType, $simpleTypeElementData) {
        if ($element["annotation"]["appinfo"]["annotations"]["notFormField"] === null && $element["annotation"]["appinfo"]["annotations"]["formField"] === null) {
            $element["annotation"]["appinfo"]["annotations"]["formField"] = array();
        }
        if ($element["annotation"]["appinfo"]["annotations"]["formField"] !== null) {
            if ($element["annotation"]["appinfo"]["annotations"]["formField"]["name"] === null) {
                $element["annotation"]["appinfo"]["annotations"]["formField"]["name"] = $element["annotation"]["appinfo"]["annotations"]["var"]["name"];
            }
            if ($element["annotation"]["appinfo"]["annotations"]["formField"]["type"] === null) {
                if (!$varArray) {
                    if ($varType == "DateTime") {
                        $element["annotation"]["appinfo"]["annotations"]["formField"]["type"] = strtolower($dataType);
                    } else if ($element["type"]["type"] == "Color") {
                        $element["annotation"]["appinfo"]["annotations"]["formField"]["type"] = "color";
                    } else if ($element["type"]["type"] == "Email") {
                        $element["annotation"]["appinfo"]["annotations"]["formField"]["type"] = "email";
                    } else if ($element["type"]["type"] == "Phone") {
                        $element["annotation"]["appinfo"]["annotations"]["formField"]["type"] = "tel";
                    } else if ($varType == "int" || $varType == "boolean") {
                        $element["annotation"]["appinfo"]["annotations"]["formField"]["type"] = "number";
                    }else {
                        $element["annotation"]["appinfo"]["annotations"]["formField"]["type"] = "text";
                    }
                } else {
                    $element["annotation"]["appinfo"]["annotations"]["formField"]["type"] = "text";
                }

            }
            if ($element["annotation"]["appinfo"]["annotations"]["formField"]["required"] === null) {
                $element["annotation"]["appinfo"]["annotations"]["formField"]["required"] = (bool)$element["minOccurs"];
            }
            if (!$varArray && $element["annotation"]["appinfo"]["annotations"]["formField"]["min"] === null) {
                $min = null;
                if ($varType == "int") {
                    if ($dataType == "unsignedByte" || $dataType == "unsignedShort" || $dataType == "unsignedInt" || $dataType == "unsignedLong" || $dataType == "nonNegativeInteger") {
                        $min = 0;
                    } else if ($dataType == "positiveInteger") {
                        $min = 1;
                    }
                    if ($simpleTypeElementData["restriction"] !== null && count($simpleTypeElementData["restriction"]["elements"]) > 0) {
                        if ($simpleTypeElementData["restriction"]["elements"]["minInclusive"] !== null) {
                            if ($min !== null && $simpleTypeElementData["restriction"]["elements"]["minInclusive"] > $min) {
                                $min = $simpleTypeElementData["restriction"]["elements"]["minInclusive"];
                            }
                        } else if ($simpleTypeElementData["restriction"]["elements"]["minExclusive"] !== null) {
                            if ($min !== null && ($simpleTypeElementData["restriction"]["elements"]["minExclusive"] + 1) > $min) {
                                $min = $simpleTypeElementData["restriction"]["elements"]["minExclusive"] + 1;
                            }
                        }
                    }

                } else if ($varType == "boolean") {
                    $min = 0;
                }
                $element["annotation"]["appinfo"]["annotations"]["formField"]["min"] = ($min === null ? 0 : $min);
            }
            if (!$varArray && $element["annotation"]["appinfo"]["annotations"]["formField"]["max"] === null) {
                $max = null;
                if ($varType == "int") {
                    if ($dataType == "negativeInteger") {
                        $max = -1;
                    } else if ($dataType == "nonPositiveInteger") {
                        $max = 0;
                    }
                    if ($simpleTypeElementData["restriction"] !== null && count($simpleTypeElementData["restriction"]["elements"]) > 0) {
                        if ($simpleTypeElementData["restriction"]["elements"]["maxInclusive"] !== null) {
                            if ($max !== null && $simpleTypeElementData["restriction"]["elements"]["maxInclusive"] < $max) {
                                $max = $simpleTypeElementData["restriction"]["elements"]["maxInclusive"];
                            }
                        } else if ($simpleTypeElementData["restriction"]["elements"]["maxExclusive"] !== null) {
                            if ($max !== null && ($simpleTypeElementData["restriction"]["elements"]["maxExclusive"] - 1) < $max) {
                                $max = $simpleTypeElementData["restriction"]["elements"]["maxExclusive"] - 1;
                            }
                        }
                    }
                } else if ($varType == "boolean") {
                    $max = 1;
                }
                $element["annotation"]["appinfo"]["annotations"]["formField"]["max"] = ($max === null ? 0 : $max);
            }
            if (!$varArray && $element["annotation"]["appinfo"]["annotations"]["formField"]["maxlength"] === null) {

                if ($dataType == "string") {
                    $maxlength = null;
                    if ($simpleTypeElementData["restriction"] !== null && count($simpleTypeElementData["restriction"]["elements"]) > 0) {
                        if ($simpleTypeElementData["restriction"]["elements"]["length"] !== null) {
                            $maxlength = $simpleTypeElementData["restriction"]["elements"]["length"];
                        } else if ($simpleTypeElementData["restriction"]["elements"]["maxLength"] !== null) {
                            $maxlength = $simpleTypeElementData["restriction"]["elements"]["maxLength"];
                        }
                    }
                    $element["annotation"]["appinfo"]["annotations"]["formField"]["maxlength"] = ($maxlength === null ? 0 : $maxlength);
                }
            }
            if (!$varArray && $element["annotation"]["appinfo"]["annotations"]["formField"]["pattern"] === null) {
                $pattern = null;
                if ($simpleTypeElementData["restriction"] !== null && count($simpleTypeElementData["restriction"]["elements"]) > 0) {
                    if ($simpleTypeElementData["restriction"]["elements"]["pattern"] !== null) {
                        $pattern = $simpleTypeElementData["restriction"]["elements"]["pattern"];
                    }
                }
                $element["annotation"]["appinfo"]["annotations"]["formField"]["pattern"] = $pattern;
            }
            if (!$varArray && $element["annotation"]["appinfo"]["annotations"]["formField"]["defaultValue"] === null) {
                $element["annotation"]["appinfo"]["annotations"]["formField"]["defaultValue"] = $element["default"];
            }
        }
    }
    
    private function setFormAnnotationsForEntityComplexElement(&$element, $varArray, $varType) {
        if ($element["annotation"]["appinfo"]["annotations"]["notFormField"] === null && $element["annotation"]["appinfo"]["annotations"]["formField"] === null) {
            $element["annotation"]["appinfo"]["annotations"]["formField"] = array();
        }
        if ($element["annotation"]["appinfo"]["annotations"]["formField"] !== null) {
            if ($element["annotation"]["appinfo"]["annotations"]["formField"]["name"] === null) {
                $element["annotation"]["appinfo"]["annotations"]["formField"]["name"] = $element["annotation"]["appinfo"]["annotations"]["var"]["name"];
            }
            if ($element["annotation"]["appinfo"]["annotations"]["formField"]["type"] === null) {
                if (!$varArray && $varType != "map") {
                    $element["annotation"]["appinfo"]["annotations"]["formField"]["type"] = "number";
                } else {
                    $element["annotation"]["appinfo"]["annotations"]["formField"]["type"] = "text";
                }
            }
            if ($element["annotation"]["appinfo"]["annotations"]["formField"]["required"] === null) {
                $element["annotation"]["appinfo"]["annotations"]["formField"]["required"] = (bool)$element["minOccurs"];
            }
            $element["annotation"]["appinfo"]["annotations"]["formField"]["min"] = 0;
            $element["annotation"]["appinfo"]["annotations"]["formField"]["max"] = 0;
            $element["annotation"]["appinfo"]["annotations"]["formField"]["maxlength"] = 0;
        }
    }
    
    private function setColumnAnnotationsForEntitySimpleElement(&$element, $varArray, $varType, $dataType, $simpleTypeElementData) {
        if ($element["annotation"]["appinfo"]["annotations"]["notColumn"] === null && $element["annotation"]["appinfo"]["annotations"]["column"] === null) {
            $element["annotation"]["appinfo"]["annotations"]["column"] = array();
        }
        if ($element["annotation"]["appinfo"]["annotations"]["column"] !== null) {
            if ($element["annotation"]["appinfo"]["annotations"]["column"]["name"] === null) {
                $element["annotation"]["appinfo"]["annotations"]["column"]["name"] = $element["annotation"]["appinfo"]["annotations"]["var"]["name"];
            }
            if ($element["annotation"]["appinfo"]["annotations"]["column"]["type"] === null) {
                $columnType = null;
                if ($varType == "boolean") {
                    $columnType = "boolean";
                } else if ($varType == "int") {
                    if ($dataType == "byte" || $dataType == "unsignedByte") {
                        $columnType = "tinyint";
                    } else if ($dataType == "short" || $dataType == "unsignedShort") {
                        $columnType = "smallint";
                    } else if ($dataType == "int" || $dataType == "unsignedInt" || $dataType == "negativeInteger" || $dataType == "nonNegativeInteger" || $dataType == "positiveInteger" || $dataType == "nonPositiveInteger") {
                        $columnType = "int";
                    } else if ($dataType == "long" || $dataType == "unsignedLong") {
                        $columnType = "bigint";
                    } else if ($dataType == "float") {
                        $columnType = "float";
                    } else if ($dataType == "double") {
                        $columnType = "double";
                    } else if ($dataType == "decimal") {
                        $columnType = "decimal";
                    } else {
                        throw new \Exception("Произошла ошибка парсера.");
                    }
                } else if ($varType == "DateTime") {
                    $columnType = strtolower($dataType);
                } else if ($simpleTypeElementData["restriction"] !== null && count($simpleTypeElementData["restriction"]["elements"]) > 0 && $simpleTypeElementData["restriction"]["elements"]["enumeration"] !== null) {
                    $columnType = "enum";
                } else if ($varType == "string") {
                    $columnType = "varchar";
                } else if ($varArray) {
                    $columnType = "set";
                }
                $element["annotation"]["appinfo"]["annotations"]["column"]["type"] = $columnType;
            }
            if ($element["annotation"]["appinfo"]["annotations"]["column"]["length"] === null) {
                if ($element["annotation"]["appinfo"]["annotations"]["column"]["type"] == "string") {
                    $element["annotation"]["appinfo"]["annotations"]["column"]["length"] = 256;
                }
            }
            if ($element["annotation"]["appinfo"]["annotations"]["column"]["unsigned"] === null) {
                if (strpos($dataType, "unsigned") !== false) {
                    $element["annotation"]["appinfo"]["annotations"]["column"]["unsigned"] = true;
                }
            }
        }
    }
    
    private function setColumnAnnotationsForEntityComplexElement(&$element, $varArray, $varEntity, $varSelect, $varMap) {
        if (!$varArray && $element["annotation"]["appinfo"]["annotations"]["notColumn"] === null && $element["annotation"]["appinfo"]["annotations"]["column"] === null) {
            $element["annotation"]["appinfo"]["annotations"]["column"] = array();
        }
        if ($element["annotation"]["appinfo"]["annotations"]["column"] === null) {
            $element["annotation"]["appinfo"]["annotations"]["notColumn"] = true;
        }
        if ($element["annotation"]["appinfo"]["annotations"]["column"] !== null) {
            if ($element["annotation"]["appinfo"]["annotations"]["column"]["name"] === null) {
                if ($varEntity || $varSelect) {
                    $element["annotation"]["appinfo"]["annotations"]["column"]["name"] = $element["annotation"]["appinfo"]["annotations"]["var"]["name"] . "Id";
                } else if ($varMap) {
                    $element["annotation"]["appinfo"]["annotations"]["column"]["name"] = $element["annotation"]["appinfo"]["annotations"]["var"]["name"];
                }                        
            }
            if ($element["annotation"]["appinfo"]["annotations"]["column"]["type"] === null) {
                if ($varEntity || $varSelect) {
                    $element["annotation"]["appinfo"]["annotations"]["column"]["type"] = "bigint";
                } else if ($varMap) {
                    $element["annotation"]["appinfo"]["annotations"]["column"]["type"] = "text";
                }
            }
            if ($element["annotation"]["appinfo"]["annotations"]["column"]["unsigned"] === null) {
                if ($varEntity || $varSelect) {
                    $element["annotation"]["appinfo"]["annotations"]["column"]["unsigned"] = true;
                }
            }
        }
    }
    
    private function setOrmAnnotationsForEntityComplexElement(&$element, $varArray, $varEntity, $complexTypeElementData, $relationshipElementData) {
        if ($varEntity) {
            $element["annotation"]["appinfo"]["annotations"]["OneToOne"] = array(
                "targetEntity" => $element["type"]["type"]
            );
            if ($element["elementPath"] != $complexTypeElementData["elementPath"]) {
                $mappedBy = null;
                $inversedBy = null;
                // если связи описаны в этой схеме $mappedBy не равен нулю, а $inversedBy равен нулю

                $mappedBy = $element["name"];
                
            }
                
        }
     }
    
    private function setAnnotationsForEntityElement(&$element) {
        $simpleTypeElementData = $this->simpleTypeElementsData[$element["type"]["elementPath"]];
        if ($simpleTypeElementData !== null) {
            if ($element["annotation"]["appinfo"]["annotations"]["var"] === null) {
                $element["annotation"]["appinfo"]["annotations"]["var"] = array();
            }
            if ($element["annotation"]["appinfo"]["annotations"]["var"]["name"] === null) {
                $element["annotation"]["appinfo"]["annotations"]["var"]["name"] = $element["name"];
            }
            $varType = null;
            if ($element["annotation"]["appinfo"]["annotations"]["var"]["type"] === null) {
                $dataType = $simpleTypeElementData["dataType"];
                if ($dataType == "byte" || $dataType == "short" || $dataType == "int" || $dataType == "integer" || $dataType == "long" || $dataType == "float" || $dataType == "double" || $dataType == "decimal" || $dataType == "unsignedByte" || $dataType == "unsignedShort" || $dataType == "unsignedInt" || $dataType == "unsignedLong" || $dataType == "negativeInteger" || $dataType == "nonNegativeInteger" || $dataType == "positiveInteger" || $dataType == "nonPositiveInteger") {
                    $varType = "int";
                } else if ($dataType == "boolean") {
                    $varType = "boolean";
                } else if ($dataType == "dateTime" || $dataType == "date" || $dataType == "time") {
                    $varType = "DateTime";
                } else if ($dataType == "string"){
                    $varType = "string";
                }
                if ($varType === null) {
                    throw new \Exception("Произошла ошибка парсера.");
                }
                if ($simpleTypeElementData["list"] !== null) {
                    $varType .= "[]";
                }
                $element["annotation"]["appinfo"]["annotations"]["var"]["type"] = $varType;
            }                
            $varArray = false;
            if ($simpleTypeElementData["list"] !== null) {
                $varArray = true;
            }
            $element["annotation"]["appinfo"]["annotations"]["array"] = $varArray;

            if (!$varArray && $element["fixed"] === null && $element["default"] === null) {
                $default = null;
                $varType = $element["annotation"]["appinfo"]["annotations"]["var"]["type"];
                if ($simpleTypeElementData["restriction"] !== null && count($simpleTypeElementData["restriction"]["elements"]) > 0 && $simpleTypeElementData["restriction"]["elements"]["enumeration"] !== null) {
                    $default = explode("|", $simpleTypeElementData["restriction"]["elements"]["enumeration"])[0];
                } else if ($varType == "boolean") {
                    $default = "false";
                } else if ($varType == "int") {
                    $default = "0";
                }
                $element["default"] = $default;
            }
            $this->setColumnAnnotationsForEntitySimpleElement($element, $varArray, $varType, $dataType, $simpleTypeElementData);
            $this->setFormAnnotationsForEntitySimpleElement($element, $varArray, $varType, $dataType, $simpleTypeElementData);
            if ($element["annotation"]["appinfo"]["annotations"]["formField"] !== null) {
                if ($element["annotation"]["appinfo"]["annotations"]["formField"]["maxlength"] !== null) {
                    $element["annotation"]["appinfo"]["annotations"]["column"]["length"] = $element["annotation"]["appinfo"]["annotations"]["formField"]["maxlength"];
                }
            }
            return;
        }
        $complexTypeElementData = $this->complexTypeElementsData[$element["type"]["elementPath"]];
        if ($element["annotation"]["appinfo"]["annotations"]["var"] === null) {
            $element["annotation"]["appinfo"]["annotations"]["var"] = array();
        }
        if ($element["annotation"]["appinfo"]["annotations"]["var"]["name"] === null) {
            $element["annotation"]["appinfo"]["annotations"]["var"]["name"] = $element["name"];
        }
        $varType = null;
        if ($element["annotation"]["appinfo"]["annotations"]["var"]["type"] === null) {
            if ($complexTypeElementData["annotation"]["appinfo"]["annotations"]["entity"] !== null 
                    || $complexTypeElementData["annotation"]["appinfo"]["annotations"]["select"] !== null){
                $varType = $complexTypeElementData["name"];
            } else if ($complexTypeElementData["annotation"]["appinfo"]["annotations"]["array"] !== null) {
                $varType = $complexTypeElementData["sequence"]["elements"][0]["type"]["type"] . "[]";
            } else if ($complexTypeElementData["annotation"]["appinfo"]["annotations"]["map"] !== null) {
                $varType = $complexTypeElementData["sequence"]["elements"][0]["type"]["type"] . "[] as map";
            }
            if ($varType === null) {
                throw new \Exception("Произошла ошибка парсера.");
            }
            $element["annotation"]["appinfo"]["annotations"]["var"]["type"] = $varType;
        }
        $varArray = false;
        if ($complexTypeElementData["annotation"]["appinfo"]["annotations"]["array"] !== null) {
            $varArray = true;
            $element["annotation"]["appinfo"]["annotations"]["array"] = $varArray;
        }
        $varMap = false;
        if ($complexTypeElementData["annotation"]["appinfo"]["annotations"]["map"] !== null) {
            $varMap = true;
            $element["annotation"]["appinfo"]["annotations"]["map"] = $varMap;
        }
        $varEntity = false;
        if ($complexTypeElementData["annotation"]["appinfo"]["annotations"]["entity"] !== null) {
            $varEntity = true;
            $element["annotation"]["appinfo"]["annotations"]["entity"] = $varEntity;
        }
        $varSelect = false;
        if ($complexTypeElementData["annotation"]["appinfo"]["annotations"]["select"] !== null) {
            $varSelect = true;
            $element["annotation"]["appinfo"]["annotations"]["select"] = $varSelect;
        }
        $this->setColumnAnnotationsForEntityComplexElement($element, $varArray, $varEntity, $varSelect, $varMap);
        $this->setFormAnnotationsForEntityComplexElement($element, $varArray, $varType);
    }
    
    private function setOrmAnnotationsForEntityElement(&$element) {
        $complexTypeElementData = $this->complexTypeElementsData[$element["type"]["elementPath"]];
        if ($complexTypeElementData === null) {
            return;
        }
        $relationshipElementData = $this->elementsData[$complexTypeElementData["relationshipElementPath"]];
        $this->setOrmAnnotationsForEntityComplexElement($element
                , $element["annotation"]["appinfo"]["annotations"]["array"]
                , $element["annotation"]["appinfo"]["annotations"]["entity"]
                , $element["annotation"]["appinfo"]["annotations"]["select"]
                , $element["annotation"]["appinfo"]["annotations"]["map"]
                , $complexTypeElementData, $relationshipElementData);
    }
    
    private function setAnnotationsForSelectElement(&$element, $entityElement) {
        $simpleTypeElementData = $this->simpleTypeElementsData[$element["type"]["elementPath"]];
        if ($simpleTypeElementData !== null) {
            $varType = $entityElement["annotation"]["appinfo"]["annotations"]["var"];
            $element["annotation"]["appinfo"]["annotations"]["var"] = $varType;
            $varArray = false;
            if ($simpleTypeElementData["list"] !== null) {
                $varArray = true;
            }
            $element["annotation"]["appinfo"]["annotations"]["array"] = $varArray;

            if (!$varArray && $element["fixed"] === null && $element["default"] === null) {
                $element["default"] = $entityElement["default"];
            }
            // аннотоции таблиц
            if ($entityElement["annotation"]["appinfo"]["annotations"]["notColumn"] !== null) {
                $element["annotation"]["appinfo"]["annotations"]["notColumn"] = $entityElement["annotation"]["appinfo"]["annotations"]["notColumn"];
            }
            if ($entityElement["annotation"]["appinfo"]["annotations"]["column"] !== null) {
                $element["annotation"]["appinfo"]["annotations"]["column"] = $entityElement["annotation"]["appinfo"]["annotations"]["column"];
            }
            // аннотации форм
            if ($entityElement["annotation"]["appinfo"]["annotations"]["notFormField"] !== null) {
                $element["annotation"]["appinfo"]["annotations"]["notFormField"] = $entityElement["annotation"]["appinfo"]["annotations"]["notFormField"];
            }
            if ($entityElement["annotation"]["appinfo"]["annotations"]["formField"] !== null) {
                $element["annotation"]["appinfo"]["annotations"]["formField"] = $entityElement["annotation"]["appinfo"]["annotations"]["formField"];
            }
            // аннотации таблиц, зависящие от формы
            if ($element["annotation"]["appinfo"]["annotations"]["formField"] !== null) {
                if ($element["annotation"]["appinfo"]["annotations"]["formField"]["maxlength"] !== null) {
                    $element["annotation"]["appinfo"]["annotations"]["column"]["length"] = $element["annotation"]["appinfo"]["annotations"]["formField"]["maxlength"];
                }
            }
            // аннотации валидатора
            if ($element["annotation"]["appinfo"]["annotations"]["error"] === null && $entityElement["annotation"]["appinfo"]["annotations"]["error"] !== null) {
                $element["annotation"]["appinfo"]["annotations"]["error"] = $entityElement["annotation"]["appinfo"]["annotations"]["error"];
            }
            return;
        }
        $complexTypeElementData2 = $this->complexTypeElementsData[$element["type"]["elementPath"]];
        if ($element["annotation"]["appinfo"]["annotations"]["var"] === null) {
            $varType = null;
            if ($complexTypeElementData2["annotation"]["appinfo"]["annotations"]["entity"] !== null 
                    || $complexTypeElementData2["annotation"]["appinfo"]["annotations"]["select"] !== null){
                $varType = $complexTypeElementData2["name"];
            } else if ($complexTypeElementData2["annotation"]["appinfo"]["annotations"]["array"] !== null) {
                $varType = $complexTypeElementData2["sequence"]["elements"][0]["type"]["type"] . "[]";
            } else if ($complexTypeElementData2["annotation"]["appinfo"]["annotations"]["map"] !== null) {
                $varType = "map";
            }
            if ($varType === null) {
                throw new \Exception("Произошла ошибка парсера.");
            }
            $element["annotation"]["appinfo"]["annotations"]["var"] = array(
                "type" => $varType
            );
            $varArray = false;
            if ($complexTypeElementData2["annotation"]["appinfo"]["annotations"]["array"] !== null) {
                $varArray = true;
                $element["annotation"]["appinfo"]["annotations"]["array"] = $varArray;
                if ($varArray !== $entityElement["annotation"]["appinfo"]["annotations"]["array"]) {
                    throw new \Exception("varArray должен быть таким же как у entity.");
                }
            }
            $varMap = false;
            if ($complexTypeElementData2["annotation"]["appinfo"]["annotations"]["map"] !== null) {
                $varMap = true;
                $element["annotation"]["appinfo"]["annotations"]["map"] = $varMap;
                if ($varMap !== $entityElement["annotation"]["appinfo"]["annotations"]["map"]) {
                    throw new \Exception("varMap должен быть таким же как у entity.");
                }
            }
                
            $varEntity = false;
            if ($complexTypeElementData2["annotation"]["appinfo"]["annotations"]["entity"] !== null) {
                $varEntity = true;
                $element["annotation"]["appinfo"]["annotations"]["entity"] = $varEntity;
            }
            $varSelect = false;
            if ($complexTypeElementData2["annotation"]["appinfo"]["annotations"]["select"] !== null) {
                $varSelect = true;
                $element["annotation"]["appinfo"]["annotations"]["select"] = $varSelect;
            }
            // аннотации таблиц
            if ($entityElement["annotation"]["appinfo"]["annotations"]["notColumn"] !== null) {
                $element["annotation"]["appinfo"]["annotations"]["notColumn"] = $entityElement["annotation"]["appinfo"]["annotations"]["notColumn"];
            }
            if ($entityElement["annotation"]["appinfo"]["annotations"]["column"] !== null) {
                $element["annotation"]["appinfo"]["annotations"]["column"] = $entityElement["annotation"]["appinfo"]["annotations"]["column"];
            }
            // аннотации форм
            if ($entityElement["annotation"]["appinfo"]["annotations"]["notFormField"] !== null) {
                $element["annotation"]["appinfo"]["annotations"]["notFormField"] = $entityElement["annotation"]["appinfo"]["annotations"]["notFormField"];
            }
            if ($entityElement["annotation"]["appinfo"]["annotations"]["formField"] !== null) {
                $element["annotation"]["appinfo"]["annotations"]["formField"] = $entityElement["annotation"]["appinfo"]["annotations"]["formField"];
            }
            // аннотации валидатора
            if ($element["annotation"]["appinfo"]["annotations"]["error"] === null && $entityElement["annotation"]["appinfo"]["annotations"]["error"] !== null) {
                $element["annotation"]["appinfo"]["annotations"]["error"] = $entityElement["annotation"]["appinfo"]["annotations"]["error"];
            }
        }
    }
    
    private function setAnnotationsForEntityComplexTypeElements ($elementPath) {
        $complexTypeElementData = $this->complexTypeElementsData[$elementPath];
        if ($complexTypeElementData["parsed"] !== null) {
            return;
        }
        if ($complexTypeElementData === null) {
            throw new \Exception("Произошла ошибка парсера.");
        }
        if ($complexTypeElementData["annotation"]["appinfo"]["annotations"]["entity"] === null) {
            return;
        }
        foreach ($complexTypeElementData["elementsMap"] as $key => $element) {
            $this->setAnnotationsForEntityElement($complexTypeElementData["elementsMap"][$key]); 
        }
        $complexTypeElementData["parsed"] = true;
        $this->complexTypeElementsData[$elementPath] = $complexTypeElementData;
    }
    
    private function setOrmAnnotationsForEntityComplexTypeElements ($elementPath) {
        $complexTypeElementData = $this->complexTypeElementsData[$elementPath];
        if ($complexTypeElementData["parsedOrm"] !== null) {
            return;
        }
        if ($complexTypeElementData === null) {
            throw new \Exception("Произошла ошибка парсера.");
        }
        if ($complexTypeElementData["annotation"]["appinfo"]["annotations"]["entity"] === null) {
            return;
        }
        foreach ($complexTypeElementData["elementsMap"] as $key => $element) {
            $this->setOrmAnnotationsForEntityElement($complexTypeElementData["elementsMap"][$key]); 
        }
        $complexTypeElementData["parsedOrm"] = true;
        $this->complexTypeElementsData[$elementPath] = $complexTypeElementData;
    }
    
    private function setAnnotationsForSelectComplexTypeElements($elementPath, $entityElementPath) {
        $complexTypeElementData = $this->complexTypeElementsData[$elementPath];
        if ($complexTypeElementData["parsed"] !== null) {
            return;
        }
        if ($complexTypeElementData === null) {
            throw new \Exception("Произошла ошибка парсера.");
        }
        if ($complexTypeElementData["annotation"]["appinfo"]["annotations"]["select"] === null) {
            return;
        }
        $entityComplexTypeElementData = $this->complexTypeElementsData[$entityElementPath];
        if ($entityComplexTypeElementData === null) {
            throw new \Exception("Произошла ошибка парсера.");
        }
        if ($entityComplexTypeElementData["annotation"]["appinfo"]["annotations"]["entity"] === null) {
            throw new \Exception("complexType должжен быть типа entity.");
        }
        foreach ($complexTypeElementData["elementsMap"] as $key => $element) {
            $entityElement = null;
            foreach ($entityComplexTypeElementData["elementsMap"] as $entityKey => $entityElement) {
                if ($key == $entityKey) {
                    break;
                }
            }
            $this->setAnnotationsForSelectElement($complexTypeElementData["elementsMap"][$key], $entityElement); 
        }
        $complexTypeElementData["parsed"] = true;
        $this->complexTypeElementsData[$elementPath] = $complexTypeElementData;
        
    }
    
    private function setAnnotationsForSchemaElements($schemaFullPath) {
        $schemaData = $this->schemasData[$schemaFullPath];
        if ($schemaData["parsed"] !== null) {
            return;
        }
        // Определим пользоватлеьские аннотации для элементов в схемах с именами, оканчивающимся на Type.xsd
        if (strpos($schemaFullPath, "BaseType.xsd") !== false) {
            for ($i = 0, $l = count($schemaData["elements"]); $i < $l; $i++) {
                if ($schemaData["elements"][$i]["nodeName"] != "complexType") {
                    continue;
                }
                $complexTypeElementData = $this->complexTypeElementsData[$schemaData["elements"][$i]["elementPath"]];
                if (count($complexTypeElementData["sequence"]["elements"]) == 1 && $complexTypeElementData["sequence"]["elements"][0]["name"] == "entry") {
                    $this->complexTypeElementsData[$complexTypeElementData["elementPath"]]["annotation"]["appinfo"]["annotations"]["map"] = true;
                }
                // элементы, которые в своей последовательности имеют только 2 элемента с атрибутами name равным key и name равным value соотвественно, является вхождениями в карты
                else if (count($complexTypeElementData["sequence"]["elements"]) == 2 && $complexTypeElementData["sequence"]["elements"][0]["name"] == "key" && $complexTypeElementData["sequence"]["elements"][1]["name"] == "value") {
                    $this->complexTypeElementsData[$complexTypeElementData["elementPath"]]["annotation"]["appinfo"]["annotations"]["entry"] = true;
                }
                // иначе элементы описывают сущность
                else {
                    throw new \Exception("Элемент не был определен: " . $complexTypeElementData["elementPath"] . ".");
                }
            }
        } else if (strpos($schemaFullPath, "Type.xsd") !== false) {
            // проанализируем сначала первый элемент
            if ($schemaData["elements"][0] === null) {
                throw new \Exception("Ожидалось что будут описаны элементы в схеме " . $schemaFullPath . ".");
            }
            if ($schemaData["elements"][0]["nodeName"] != "complexType") {
                throw new \Exception("Ожидалось что первый элемент в схеме " . $schemaFullPath . " будет complexType.");
            }
            $complexTypeElementData = $this->complexTypeElementsData[$schemaData["elements"][0]["elementPath"]];
            $this->isExistedEntity = false;
            $this->isExistedRelationship = false;
            $this->entityElementPath = null;
            $this->relationshipElementPath = null;
            $this->entityTable = null;
            if ($complexTypeElementData["annotation"]["appinfo"]["annotations"]["entity"] === null) {
                if (!$this->isEntity($complexTypeElementData, $schemaData)) {
                    throw new \Exception("Ожидалось что первый элемент будет описывать сущность(entity): " . $complexTypeElementData["elementPath"] . ".");
                }
                $this->complexTypeElementsData[$complexTypeElementData["elementPath"]]["annotation"]["appinfo"]["annotations"]["entity"] = true;
            }
            for ($i = 1, $l = count($schemaData["elements"]); $i < $l; $i++) {
                if ($schemaData["elements"][$i]["nodeName"] != "complexType") {
                    continue;
                }
                $complexTypeElementData = $this->complexTypeElementsData[$schemaData["elements"][$i]["elementPath"]];
                if ($complexTypeElementData["annotation"]["appinfo"]["annotations"]["select"] !== null || $complexTypeElementData["annotation"]["appinfo"]["annotations"]["array"] !== null || $complexTypeElementData["annotation"]["appinfo"]["annotations"]["joinTable"] !== null || $complexTypeElementData["annotation"]["appinfo"]["annotations"]["map"] !== null || $complexTypeElementData["annotation"]["appinfo"]["annotations"]["entry"] !== null) {
                    continue;
                } else if ($complexTypeElementData["annotation"]["appinfo"]["annotations"]["relationship"] !== null) {
                    if ($this->isExistedRelationship) {
                        throw new \Exception("Элемент relationship уже был описан " . $this->relationshipElementPath . ".");
                    }
                    // проверим на валидность 
                    if (!$this->isRelationship($complexTypeElementData)) {
                        throw new \Exception("Элемент relationship описан неправильно " . $complexTypeElementData["elementPath"] . ".");
                    }
                    $this->complexTypeElementsData[$this->entityElementPath]["annotation"]["appinfo"]["annotations"]["table"] = array("name" => $this->entityTable);
                    $this->complexTypeElementsData[$this->entityElementPath]["relationshipElementPath"] = $this->relationshipElementPath;
                    continue;
                } else if ($this->isExistedEntity && !$this->isExistedRelationship && $this->isRelationship($complexTypeElementData)) {
                    $this->complexTypeElementsData[$complexTypeElementData["elementPath"]]["annotation"]["appinfo"]["annotations"]["relationship"] = true;
                    $this->complexTypeElementsData[$this->entityElementPath]["annotation"]["appinfo"]["annotations"]["table"] = array("name" => $this->entityTable);
                    $this->complexTypeElementsData[$this->entityElementPath]["relationshipElementPath"] = $this->relationshipElementPath;
                } else if ($this->isExistedEntity && $this->isJoinTable($complexTypeElementData, $schemaData)) {
                    $this->complexTypeElementsData[$complexTypeElementData["elementPath"]]["annotation"]["appinfo"]["annotations"]["joinTable"] = true;
                } else if ($this->isArray($complexTypeElementData, $schemaData)) {
                    $this->complexTypeElementsData[$complexTypeElementData["elementPath"]]["annotation"]["appinfo"]["annotations"]["array"] = true;
                } else if ($this->isExistedEntity && $this->isSelect($complexTypeElementData, $schemaData)) {
                    $this->complexTypeElementsData[$complexTypeElementData["elementPath"]]["annotation"]["appinfo"]["annotations"]["select"] = true;
                }
                // иначе элементы описывают сущность
                else {
                    throw new \Exception("Элемент не получилось распознать: " . $complexTypeElementData["elementPath"] . ".");
                }
            }
        } else if (strpos($schemaFullPath, "Request.xsd") !== false) {
            for ($i = 0, $l = count($schemaData["elements"]); $i < $l; $i++) {
                if ($schemaData["elements"][$i]["nodeName"] != "complexType") {
                    continue;
                }
                $complexTypeElementData = $this->complexTypeElementsData[$schemaData["elements"][$i]["elementPath"]];
                $complexTypeElementData["annotation"]["appinfo"]["annotations"]["request"] = true;
            }
        } else if (strpos($schemaFullPath, "Response.xsd") !== false) {
            for ($i = 0, $l = count($schemaData["elements"]); $i < $l; $i++) {
                if ($schemaData["elements"][$i]["nodeName"] != "complexType") {
                    continue;
                }
                $complexTypeElementData = $this->complexTypeElementsData[$schemaData["elements"][$i]["elementPath"]];
                $complexTypeElementData["annotation"]["appinfo"]["annotations"]["response"] = true;
            }
        } else {
            throw new \Exception("Схемы с такими именами не парсятся " . $schemaData["schemaFullPath"] . ".");
        }
        $schemaData["parsed"] = true;
        $this->schemasData[$schemaFullPath] = $schemaData;
    }
    
    private function checkXpathPart($xpathPart, &$element) {
        $complexTypeElementData = $this->complexTypeElementsData[$element["type"]["elementPath"]];
        $finded = false;
        foreach ($complexTypeElementData["sequence"]["elements"] as $element) {
            if ($element["name"] == $xpathPart["xpathPart"] && $element["targetNamespace"] == $xpathPart["namespace"]) {
                $finded = true;
                break;
            }
        }
        if (!$finded) {
            throw new \Exception("Не верно указан xpath.");
        }
    }
    
    private function checkXpath($element) {
        foreach (array_merge($element["uniques"], $element["keys"], $element["keyrefs"]) as $u) {
            // проверим селекторы
            $selector = $u["selector"];
            $xpath = null;
            try {
                $xpath = $selector["xpath"];
                $fromElementForSelector = $element; // элемент от которого строится путь
                foreach ($selector["xpathParts"] as $xpathPart) {
                    $this->checkXpathPart ($xpathPart, $fromElementForSelector);
                }
            } catch (\Exception $ex){
                throw new \Exception("Не верно указан xpath: " . $xpath . " у элемента " . $element["elementPath"] . "/" . $u["nodeName"] . "[name=\"" . $u["name"] . "\"]/selector" . ".");
            }
            // проверим fields
            foreach ($u["fields"] as $key => $field) {
                $xpath = null;
                try {
                    $xpath = $field["field"];
                    $fromElementForField = $fromElementForSelector;
                    foreach ($field["xpathParts"] as $xpathPart) {
                        $this->checkXpathPart($xpathPart, $fromElementForField);
                    }
                } catch (\Exception $ex){
                    throw new \Exception("Не верно указан xpath: " . $xpath . " у элемента " . $element["type"]["elementPath"] . "/[name=\"" . $u["name"] . "\"]/selector/field[" . $key . "]" . ".");
                }
            }
        }
    }
    
    public function transformXsdToClass($schemaFullPath) {
        $this->getSchema($schemaFullPath);
        // установим аннотации, определяющие тип
        foreach ($this->schemasData as $schemaData) {
            $this->setAnnotationsForSchemaElements($schemaData["schemaFullPath"]);
        }
        // установим аннотации для элементов, которые составляют сложные типы
        foreach ($this->schemasData as $schemaData) {
            if (strpos($schemaData["schemaFullPath"], "BaseType.xsd") !== false) {
                continue;
            }
            if (strpos($schemaData["schemaFullPath"], "Type.xsd") === false) {
                continue;
            }
            $this->setAnnotationsForEntityComplexTypeElements($schemaData["elements"][0]["elementPath"]);
            $entityElementPath = $schemaData["elements"][0]["elementPath"];
            for ($i = 1, $l = count($schemaData["elements"]); $i < $l; $i++) {
                if ($schemaData["elements"][$i]["nodeName"] != "complexType") {
                    continue;
                }
                $this->setAnnotationsForSelectComplexTypeElements($schemaData["elements"][$i]["elementPath"], $entityElementPath);
            }
        }
        // проверим xpath пути
        foreach ($this->elementsData as $element) {
            if ($element["type"]["schemaFullPath"] == "" || strpos($element["type"]["schemaFullPath"], "BaseType") !== false) {
                continue;
            }
            $this->checkXpath($element);
        }
        // установим аннотации для описания связей
        foreach ($this->schemasData as $schemaData) {
            if (strpos($schemaData["schemaFullPath"], "Type.xsd") === false) {
                continue;
            }
            //$this->setOrmAnnotationsForEntityComplexTypeElements($schemaData["elements"][0]["elementPath"]);
        }
        if ($schemaFullPath !== null && $this->schemasRoot !== null && $this->classesRoot !== null) {   
//            echo "<pre>";
//            print_r($this->complexTypeElementsData[$schemaFullPath . "/complexType[@name=Director]"]);
            $className = $this->getClassNameFromSchemaFullPath($schemaFullPath);
            $schemaData = $this->schemasData[$schemaFullPath];
            // Создадим классы для валидации
            $this->createClassForValidateCustomType($className, $schemaData);
           // Создадим POJO классы и классы с константами
            $this->createClassesFromComplexTypes($schemaData);
            // Создадим константы
            $this->createClassesWithConst($schemaData);
            // Создадим перечисления        
            $this->createEnumsFromSimpleTypes($schemaData);
            // Создадим формы
            $this->createFormsFromComplexTypes($schemaData);
        } else {
            // Создадим валидацию xsd типов
            $this->createClassForValidateCustomType("XsdTypeValidator", $this->schemasData[""]);
        }
    }

}

?>
