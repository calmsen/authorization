<?php
namespace Calmsen\Component\Utils;

class ObjectToXml {
     
    private $xml;
    private $version = "1.0";
    private $encoding = "UTF-8";
    private $rootName = "root";
    private $obj;
 
    function __construct($obj = null, $root = "root") {
        $this->objectToXml($obj, $root);
    }

    public function getXml() {
        return $this->xml;
    }
    
    public function setXml($xml) {
        $this->xml = $xml;
    }
    
    public function getVersion() {
        return $this->version;
    }
    public function setVersion($version) {
        $this->version = $version;
    }
    
    public function getEncoding() {
        return $this->encoding;
    }
    
    public function setEncoding($encoding) {
        $this->encoding = $encoding;
    }
    
    public function getRootName() {
        return $this->rootName;
    }
    
    public function setRootName($rootName) {
        $this->rootName = $rootName;
    }
    public function getObj() {
        return $this->obj;
    }

    public function setObj($obj) {
        $this->obj = $obj;
    }

    private function addChildToXml($obj, $parent) {        
        $methods = get_class_methods($obj);
        if (count($methods) == 0) {
            return;
        }
        foreach($methods as $method) {
            if (strpos($method, "get") !== 0) {
                continue;
            }
            $key = strtolower(substr($method, 3, 1)) . substr($method, 4);
            $val = $obj->$method();

            if (is_array($val) || is_object($val)) {
                if (is_array($val) && $val[0] !== null) {
                    foreach ($val as $v) {
                        if (is_array($v) || is_object($v)) {
                            $child = $parent->addChild($key);
                            $this->addChildToXml($v, $child);
                        } else {
                            $parent->addChild($key, $v);
                        }

                    }

                } else {
                    $child = $parent->addChild($key);
                    $this->addChildToXml($val, $child);
                }
            } else {
                $parent->addChild($key, $val);
            }
        }
    }
    
    public function objectToXml($obj, $rootName) {
        if (!is_object($obj)) {
            throw new Exception("Ожидался что будет объект.");
        }
        $this->obj = $obj;
        $this->rootName = $rootName;
        $this->xml = new SimpleXMLElement("<?xml version=\"".$this->version."\" encoding=\"".$this->encoding."\"?><".$this->rootName."></".$this->rootName.">");
		
        $this->addChildToXml($this->obj, $this->xml);
        return $this->xml;
    }
    
    private function addValToObj($xml, $obj) {
        $class = null;// класс рефлексия
        $classNameForArrays = array();// кеш, где ключ это название массива, а значение - это название класса
        $children = $xml->children();
        foreach($children as $child) {
            $key = $child->getName();
            $getter = "get" . ucfirst($key);
            $setter = "set" . ucfirst($key);
            if (is_array($obj->$getter())) {
                if ($child->count() !== 0) {
                    // Используем рефлексию чтобы определить класс
                    if ($classNameForArrays[$key] === null) {
                        if ($class === null) {
                            $class = new ReflectionClass($obj);
                        }
                        $prop = $class->getProperty($key);
                        $doc = $prop->getDocComment();
                        preg_match("/@var ([a-zA-Z]*)(\[\])/", $doc, $match);
                        $classNameForArrays[$key] = $match[1];
                    }
                    $className = $classNameForArrays[$key];
                    $obj2 = new $className();
                    $this->addValToObj($child, $obj2);
                    $arr = $obj->$getter();
                    $arr[] = $obj2;
                    $obj->$setter($arr);
                } else {
                    $arr = $obj->$getter();
                    $arr[] = $child . "";
                    $obj->$setter($arr);
                }
            } else {
                if ($child->count() !== 0) {
                    $className = ucfirst($key);
                    $obj2 = new $className();
                    $this->addValToObj($child, $obj2);
                    $obj->$setter($obj2);
                } else {
                    $obj->$setter($child . "");
                }
            }
        }
    }
    
    public function xmlToObject($xml) {
        if (!($xml instanceof SimpleXMLElement)) {
            throw new Exception("Ожидался что будет SimpleXMLElement.");
        }
        $this->xml = $xml;
        $className = ucfirst($xml->getName());
        $this->obj = new $className();
        $this->addValToObj($this->xml, $this->obj);
        return $this->obj;
    }
}
//class Data {
//    /**
//     *
//     * @var string 
//     */
//	private $header;
//    /**
//     *
//     * @var User[] 
//     */
//	private $users;
//	
//	public function getHeader() {
//		return $this->header;
//	}
//	public function setHeader($header) {
//		return $this->header = $header;
//	}
//	public function getUsers() {
//		return $this->users;
//	}
//	public function setUsers($users) {
//		return $this->users = $users;
//	}
//    function __construct() {
//        $this->users = array();
//    }
//
//}
//class User {
//    /**
//     *
//     * @var int 
//     */
//	private $id;
//    /**
//     *
//     * @var string 
//     */
//	private $firstName;
//    /**
//     *
//     * @var int 
//     */
//	private $lastName;
//    /**
//     *
//     * @var Image[] 
//     */
//	private $images;
//    /**
//     *
//     * @var int 
//     */
//    private $phones;
//	
//	public function getId() {
//		return $this->id;
//	}
//	public function setId($id) {
//		return $this->id = $id;
//	}
//	public function getFirstName() {
//		return $this->firstName;
//	}
//	public function setFirstName($firstName) {
//		return $this->firstName = $firstName;
//	}
//	public function getLastName() {
//		return $this->lastName;
//	}
//	public function setLastName($lastName) {
//		return $this->lastName = $lastName;
//	}
//    public function getImages() {
//        return $this->images;
//    }
//
//    public function setImages($images) {
//        $this->images = $images;
//    }
//    public function getPhones() {
//        return $this->phones;
//    }
//
//    public function setPhones($phones) {
//        $this->phones = $phones;
//    }
//
//    function __construct() {
//        $this->images = array();
//        $this->phones = array();
//    }
//}
//class Image {
//    /**
//     *
//     * @var int 
//     */
//    private $id;
//    /**
//     *
//     * @var string 
//     */
//    private $title;
//    
//    public function getId() {
//        return $this->id;
//    }
//
//    public function setId($id) {
//        $this->id = $id;
//    }
//
//    public function getTitle() {
//        return $this->title;
//    }
//
//    public function setTitle($title) {
//        $this->title = $title;
//    }
//
//
//}
//$data = new Data();
//$data->setHeader("Список пользователей");
//$users = array();
//
//$user = new User();
//$user->setId(1);
//$user->setFirstName("Ivan");
//$user->setLastName("Ivanov");
//$images = array();
//$image = new Image();
//$image->setId(123);
//$image->setTitle("asdfsa");
//$images[] = $image;
//$image = new Image();
//$image->setId(25);
//$image->setTitle("fgjfg");
//$images[] = $image;
//$user->setImages($images);
//$user->setPhones(array(346346, 343465));
//$users[] = $user;
//
//$user = new User();
//$user->setId(2);
//$user->setFirstName("Alex");
//$user->setLastName("Alexov");
//$users[] = $user;
//
//$data->setUsers($users);
//$obj2xml = new ObjectToXml($data, "Data");
//if (1) {
//    header ('Content-type: text/xml; charset=utf-8');
//    echo $obj2xml->getXml()->asXML();
//} else {
//    header ('Content-type: text/html; charset=utf-8');
//    echo "<pre>";
//    print_r($obj2xml->xmlToObject($obj2xml->getXml()));
//}
?>
