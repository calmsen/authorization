<?php
namespace Calmsen\Component\Utils;

class DataToObject {
     
    private $data;
    private $obj;
    private $className;
    
    function __construct($data = array(), $className = null) {
        $this->xmlToObject($data, $className);
    }

    public function getData() {
        return $this->data;
    }
    
    public function setData($data) {
        $this->data = $data;
    }
    public function getObj() {
        return $this->obj;
    }

    public function setObj($obj) {
        $this->obj = $obj;
    }

    public function getClassName() {
        return $this->className;
    }

    public function setClassName($className) {
        $this->className = $className;
    }

    private function addValToData($obj, &$data) {        
        $methods = get_class_methods($obj);
        if (count($methods) == 0) {
            return;
        }
        foreach($methods as $method) {
            if (strpos($method, "get") !== 0) {
                continue;
            }
            $key = strtolower(substr($method, 3, 1)) . substr($method, 4);
            $val = $obj->$method();

            if (is_array($val) || is_object($val)) {
                if (is_array($val) && $val[0] !== null) {
                    foreach ($val as $v) {
                        if (is_array($v) || is_object($v)) {
                            $data2 = array();
                            $this->addValToData($v, $data2);
                            $data[$key] = $data2;
                        } else {
                            $data[$key] = $v;
                        }

                    }

                } else {
                    $data2 = array();
                    $this->addValToData($val, $data2);
                    $data[$key] = $data2;
                }
            } else {
                $data[$key] = $val;
            }
        }
    }
    
    public function objectToData($obj) {
        if (!is_object($obj)) {
            throw new Exception("Ожидался что будет объект.");
        }
        $this->obj = $obj;
        $this->data = array();
		
        $this->addValToData($this->obj, $this->data);
        return $this->data;
    }
    
    private function addValToObj($data, $obj) {
        $class = null;// класс рефлексия
        $classNameForArrays = array();// кеш, где ключ это название массива, а значение - это название класса
        foreach($data as $key => $item) {
            $getter = "get" . ucfirst($key);
            $setter = "set" . ucfirst($key);
            if (is_array($obj->$getter())) {
                // если массив не индексный
                if (is_array($item) && $item[0] === null) {
                    // Используем рефлексию чтобы определить класс
                    if ($classNameForArrays[$key] === null) {
                        if ($class === null) {
                            $class = new ReflectionClass($obj);
                        }
                        $prop = $class->getProperty($key);
                        $doc = $prop->getDocComment();
                        preg_match("/@var ([a-zA-Z]*)(\[\])/", $doc, $match);
                        $classNameForArrays[$key] = $match[1];
                    }
                    $className = $classNameForArrays[$key];
                    $obj2 = new $className();
                    $this->addValToObj($item, $obj2);
                    $arr = $obj->$getter();
                    $arr[] = $obj2;
                    $obj->$setter($arr);
                } else {
                    $arr = $obj->$getter();
                    $arr[] = $item;
                    $obj->$setter($arr);
                }
                    
            } else {
                // если массив не индексный
                if (is_array($item) && $item[0] === null) {
                    $className = ucfirst($key);
                    $obj2 = new $className();
                    $this->addValToObj($item, $obj2);
                    $obj->$setter($obj2);
                } else {
                    $obj->$setter($item);
                }
            }
        }
    }
    
    public function xmlToObject($data, $className) {
        if (!is_array($data)) {
            throw new Exception("Ожидался что будет массив.");
        }
        if ($className === null) {
            throw new Exception("Укажите класс для преобразования массива.");
        }
        $this->data = $data;
        $this->className = $className;
        $this->obj = new $className();
        $this->addValToObj($this->data, $this->obj);
        return $this->obj;
    }
}
//$data = array(
//				"header" => "Список пользователей"
//				, "users" => array(
//					array(
//                        "id" => 1
//                        , "firstName" => "Ivan"
//                        , "lastName" => "Ivanov"
//                        , "images" => array(
//                            array("id" => 1234, "title" => "картинка 1")
//                            , array("id" => 234, "title" => "картинка 2")
//                            )
//                        , "phones" => array(457457, 457457)
//                        )
//					, array(
//                        "id" => 2
//                        , "firstName" => "Alex"
//                        , "lastName" => "Alexov"
//                        )
//				)
//			);
//class Data {
//    /**
//     *
//     * @var string 
//     */
//	private $header;
//    /**
//     *
//     * @var User[] 
//     */
//	private $users;
//	
//	public function getHeader() {
//		return $this->header;
//	}
//	public function setHeader($header) {
//		return $this->header = $header;
//	}
//	public function getUsers() {
//		return $this->users;
//	}
//	public function setUsers($users) {
//		return $this->users = $users;
//	}
//    function __construct() {
//        $this->users = array();
//    }
//
//}
//class User {
//    /**
//     *
//     * @var int 
//     */
//	private $id;
//    /**
//     *
//     * @var string 
//     */
//	private $firstName;
//    /**
//     *
//     * @var int 
//     */
//	private $lastName;
//    /**
//     *
//     * @var Image[] 
//     */
//	private $images;
//    /**
//     *
//     * @var int 
//     */
//    private $phones;
//	
//	public function getId() {
//		return $this->id;
//	}
//	public function setId($id) {
//		return $this->id = $id;
//	}
//	public function getFirstName() {
//		return $this->firstName;
//	}
//	public function setFirstName($firstName) {
//		return $this->firstName = $firstName;
//	}
//	public function getLastName() {
//		return $this->lastName;
//	}
//	public function setLastName($lastName) {
//		return $this->lastName = $lastName;
//	}
//    public function getImages() {
//        return $this->images;
//    }
//
//    public function setImages($images) {
//        $this->images = $images;
//    }
//    public function getPhones() {
//        return $this->phones;
//    }
//
//    public function setPhones($phones) {
//        $this->phones = $phones;
//    }
//
//    function __construct() {
//        $this->images = array();
//        $this->phones = array();
//    }
//
//
//}
//class Image {
//    /**
//     *
//     * @var int 
//     */
//    private $id;
//    /**
//     *
//     * @var string 
//     */
//    private $title;
//    
//    public function getId() {
//        return $this->id;
//    }
//
//    public function setId($id) {
//        $this->id = $id;
//    }
//
//    public function getTitle() {
//        return $this->title;
//    }
//
//    public function setTitle($title) {
//        $this->title = $title;
//    }
//}
//$data2obj = new DataToObject($data, "Data");
//header ('Content-type: text/html; charset=utf-8');
//echo "<pre>";
//if (1) {    
//    print_r($data2obj->getObj());
//} else {    
//    print_r($data2obj->objectToData($data2obj->getData()));
//}
?>
