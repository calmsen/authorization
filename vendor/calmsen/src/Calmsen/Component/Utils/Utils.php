<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Calmsen\Component\Utils;
/**
 * Description of Utils
 *
 * @author rl
 */
class Utils {    
    static function transformObjectToArray($obj, $keys = null) {
        $arr = array();
        $methods = get_class_methods($className);
        foreach ($methods as $method) {
            if (strpos($method, "get") === 0) {
                $key = strtolower(substr($method, 3, 1)) . substr($method, 4);
                // Если массив допустимых значений указан, то делаем проверку
                if($keys !== null){
                    if(!in_array($keys, $key)) {
                        continue;
                    }
                }
                $arr[$key] = $obj->$method();
                break;
            }
        }
        return $arr;
    }

    static function transformArrayToObject($arr, $className) {
        $obj = new $className();
        foreach ($arr as $key => $value) {
            if (method_exists($obj, "set" . ucfirst($key))) {
                $obj->{"set" . ucfirst($key)}($value);
                break;
            }
        }
        return $obj;
    }
    
    static function transformObjectToObject($obj, $className) {
        $arr = self::transformObjectToArray($obj);
        return self::transformArrayToObject($arr, $className);
    }
    static function getPrivateAndProtectedPropertiesNames($className) {
        $reflect = new ReflectionClass($className);
        $props = $reflect->getProperties(ReflectionProperty::IS_PRIVATE | ReflectionProperty::IS_PROTECTED);
        $propsNames = array();
        foreach ($props as $prop) {
            $propsNames[] = $prop->getName();
        }
        return $propsNames;
    }
    static function declension($int, $expressions) { 
	if (count($expressions) < 3) $expressions[2] = $expressions[1];
            settype($int, "integer"); 
            $count = $int % 100; 
            if ($count >= 5 && $count <= 20) { 
                    $result = $expressions['2']; 
            } else { 
                    $count = $count % 10; 
                    if ($count == 1) { 
                            $result = $expressions['0']; 
                    } elseif ($count >= 2 && $count <= 4) { 
                            $result = $expressions['1']; 
                    } else { 
                            $result = $expressions['2']; 
                    }
            } 
            return $result; 
    }
    static function lowerCaseFirst($str) {
        return strtolower(substr($str, 0, 1)) . substr($str, 1);
    }
    static function underScore($str) {
        return strtolower(preg_replace("/([A-Z])/", "_$1", $str));
    }
    static function upperUnderScore($str) {
        return strtoupper(preg_replace("/([A-Z])/", "_$1", $str));
    }

    static function getRequestUrl() {
        $result = '';
        $default_port = 80;
        if (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on')) {
            $result .= 'https://';
            $default_port = 443;
        } else {
            $result .= 'http://';
        }
        $result .= $_SERVER['SERVER_NAME'];
        if ($_SERVER['SERVER_PORT'] != $default_port) {
            $result .= ':' . $_SERVER['SERVER_PORT'];
        }
        $result .= $_SERVER['REQUEST_URI'];
        return $result;
    }

    static function parseUri($url = '') {
        if (!$url) {
            $url = self::getRequestUrl();
        }
        //возмем части url
        $url_parts = parse_url($url);
        if ($url_parts['scheme'] == "http" || $url_parts['scheme'] == "https") {
            $scheme = $url_parts['scheme'];
            $host = $url_parts['host'];
            $path = $url_parts['path'];
            $query = $url_parts['query'];
            if (preg_match("/\:(\\d+)/", $url, $match)) {
                $port = $match[1];
            } else {
                $port = 80;
                if ($scheme == "https") {
                    $port = 443;
                }
            }
        } else {
            $path = $url;
            $url = null;
            $scheme = null;
            $host = null;
            $query = null;
        }
            
        //если путь заканчивается на слеш, то уберем этот слеш
        if (strrpos($path, '/') == (strlen($path) - 1)) {
            $path = substr($path, 0, strlen($path) - 1);
            $url_parts['path'] = $path;
        }
        //добавим слеш + пробел для того чтобы правильно определить директория если файл не указан
        if (strpos($path, '.') === false) {
            $path .= '/ ';
        }
        //возмем части пути
        $path_parts = pathinfo($path);
        //удалим выше поставленный слеш + пробел
        $path = str_replace('/ ', '', $path);

        $dirname = $path_parts['dirname'];
        $basename = trim($path_parts['basename']); //удалим выше поставленный пробел
        $filename = $path_parts['filename'];
        $extension = trim($path_parts['extension']); //удалим выше поставленный пробел

        $slugs = explode('/', preg_replace("/^\//", "", $dirname));

        return array(
            'uri' => $url
            , 'scheme' => $scheme
            , 'host' => $host
            , 'port' => $port
            , 'path' => $path
            , 'query' => $query
            , 'dirname' => $dirname
            , 'slugs' => $slugs
            , 'basename' => $basename
            , 'filename' => $filename
            , 'extension' => $extension
        );
    }
    static function relativePathToAbsolute($relFilePath, $absBasePath) {
        // уберем первый и последний слешы
        $pattern = "/^\/|\/$/";
        $relFilePath = preg_replace($pattern, "", $relFilePath);
        $absBasePath = preg_replace($pattern, "", $absBasePath);
        $relFilePathParts = explode("/", $relFilePath);
        $absBasePathParts = explode("/", $absBasePath);
        for ($i = 0; $i < count($relFilePathParts) - 1; $i++) {
            if ($relFilePathParts[$i] != "..") {
                break;
            }
            unset($absBasePathParts[count($absBasePathParts) - 1]);
            unset($relFilePathParts[$i]);
            $i--;
        }
        $absFullFilePath = implode("/" ,$absBasePathParts) . "/" . implode("/" ,$relFilePathParts);
        return $absFullFilePath;
    }
}
?>
