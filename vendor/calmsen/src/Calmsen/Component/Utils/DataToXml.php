<?php
namespace Calmsen\Component\Utils;

class DataToXml {
     
    private $xml;
    private $version = "1.0";
    private $encoding = "UTF-8";
    private $rootName = "root";
    private $data = array();
    
    function __construct($data = array(), $rootName = "root") {
        $this->dataToXml($data, $rootName);
    }

    public function getXml() {
        return $this->xml;
    }
    
    public function setXml($xml) {
        $this->xml = $xml;
    }
    
    public function getVersion() {
        return $this->version;
    }
    public function setVersion($version) {
        $this->version = $version;
    }
    
    public function getEncoding() {
        return $this->encoding;
    }
    
    public function setEncoding($encoding) {
        $this->encoding = $encoding;
    }
    
    public function getRootName() {
        return $this->rootName;
    }
    
    public function setRootName($rootName) {
        $this->rootName = $rootName;
    }
    public function getData() {
        return $this->data;
    }

    public function setData($data) {
        $this->data = $data;
    }

    private function addChildToXml($data, $parent) {
        foreach ($data as $key => $val) {
            if (is_array($val)) {
                // Если массив индексный, то делаем обход всего массива
                if ($val[0] !== null) {
                    foreach ($val as $v) {
                        if (is_array($v)) {
                            $child = $parent->addChild($key);
                            $this->addChildToXml($v, $child);
                        } else {
                            $parent->addChild($key, $v);
                        }

                    }

                } else {
                    $child = $parent->addChild($key);
                    $this->addChildToXml($val, $child);
                }
            }
            else {
                $parent->addChild($key, $val);
            }
        }
    }
    
    public function dataToXml($data, $rootName = "root") {
        if (!is_array($data)) {
            throw new Exception("Ожидался что будет массив.");
        }
        $this->data = $data;
        $this->rootName = $rootName;        
        $this->xml = new SimpleXMLElement("<?xml version=\"".$this->version."\" encoding=\"".$this->encoding."\"?><".$this->rootName."></".$this->rootName.">");
        
        $this->addChildToXml($this->data, $this->xml);
        return $this->xml;
    }
    
    private function addValToData($xml, &$data) {
        $children = $xml->children();
        foreach($children as $child) {
            $key = $child->getName();
            if ($data[$key] !== null) {
                if (!(is_array($data[$key]) && $data[$key][0] !== null)) {
                    $data[$key] = array($data[$key]);
                }
                if ($child->count() !== 0) {
                    $data2 = array();
                    $this->addValToData($child, $data2);
                    $data[$key][] = $data2;
                } else {
                    $data[$key][] = $child . "";
                }
            } else {
                if ($child->count() !== 0) {
                    $data2 = array();
                    $this->addValToData($child, $data2);
                    $data[$key] = $data2;
                } else {
                    $data[$key] = $child . "";
                }
            }
        }
    }
    
    public function xmlToData($xml) {
        if (!($xml instanceof SimpleXMLElement)) {
            throw new Exception("Ожидался что будет SimpleXMLElement.");
        }
        $this->xml = $xml;
        $this->data = array();
        $this->addValToData($this->xml, $this->data);
        return $this->data;
    }
}
//$data = array(
//				"header" => "Список пользователей"
//				, "users" => array(
//					array(
//                        "id" => 1
//                        , "firstName" => "Ivan"
//                        , "lastName" => "Ivanov"
//                        , "images" => array(
//                            array("id" => 1234, "title" => "картинка 1")
//                            , array("id" => 234, "title" => "картинка 2")
//                            )
//                        , "phones" => array(457457, 457457)
//                        )
//					, array(
//                        "id" => 2
//                        , "firstName" => "Alex"
//                        , "lastName" => "Alexov"
//                        )
//				)
//			);
//
//$data2xml = new DataToXml($data, "root");
//
//if(0) {
//    header ('Content-type: text/xml; charset=utf-8');
//    echo $data2xml->getXml()->asXML();
//} else {
//    header ('Content-type: text/html; charset=utf-8');
//    echo "<pre>";
//    print_r($data2xml->xmlToData($data2xml->getXml()));
//}
?>
