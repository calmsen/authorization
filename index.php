<?php
mb_internal_encoding("UTF-8");
date_default_timezone_set("UTC");
session_start();

require_once "src/Env.php";

/** Подключим нужные настройки */
if ($_SERVER["SERVER_ADDR"] == "127.0.0.1")
    include_once "app/development.php";
else 
    include_once "app/production.php";

/** Создадим среду */
try {
    Env::create();
} catch (Exception $ex) {
    $response = new Symfony\Component\HttpFoundation\Response('<h1>Произошла ошибка на сервере</h1><pre>' . $ex, 500, array('Content-Type' => 'text/html; charset=utf-8'));
    $response->send();
}
?>