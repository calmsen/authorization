<?php

require_once Env::$DOCUMENT_APPL_ROOT."/vendor/symfony/src/Symfony/Component/ClassLoader/UniversalClassLoader.php";

use Symfony\Component\ClassLoader\UniversalClassLoader;

$loader = new UniversalClassLoader();
$loader->register();

$loader->registerNamespaces(array(
    "Authorization" => Env::$CLASSES_ROOT
    , "Calmsen" => Env::$DOCUMENT_APPL_ROOT."/vendor/calmsen/src"
    , "Symfony" => Env::$DOCUMENT_APPL_ROOT."/vendor/symfony/src"
));
?>
