<?php
header ('Content-type: text/html; charset=utf-8');

$xml = new DOMDocument();
$xml->loadXML(file_get_contents('xmls/usersHolder.xml'));

$xsl = new DOMDocument();
$xsl->load('xsls/base.xsl');

$proc = new XSLTProcessor();
$proc->importStyleSheet($xsl);

echo $proc->transformToXML($xml);

?>
